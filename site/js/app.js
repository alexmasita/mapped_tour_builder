define(function (require) {
    'use strict';

    var Backbone = require('backbone'),
        marionette = require('marionette'),
        TourManager = new marionette.Application(),
        commands = require('commands'),
        reqres = require('reqres'),
        $ = require('jquery'),
        oldSync;
    TourManager.addRegions({
        header: '#header',
        application: '#application'
    });
    reqres.setHandler('getCurrentRoute', function () {
        return Backbone.history.fragment;
    });
    commands.setHandler("navigate", function (route, options) {
        options || (options = {});
        Backbone.history.navigate(route, options);
    });
    oldSync = Backbone.sync;
    Backbone.sync = function (method, model, options) {
        options.beforeSend = function (xhr) {
            var token = $('meta[name="csrf-token"]').attr('content');
            if (token) {
                xhr.setRequestHeader('X-CSRF-Token', token);
            }
        };
        return oldSync(method, model, options);
    };
    TourManager.addInitializer(function () {
        require('apps/entities/mini_tours');
        require('apps/entities/tour_groups');
        require('apps/entities/tours');
        require('apps/entities/scenes');
        require('apps/entities/menu');
        require('apps/entities/menu_scenes');
        require('apps/entities/auto_view');
        require('apps/entities/auto_stops');
        require('apps/entities/scene_settings');
        require('apps/entities/hotspots');
        require('apps/entities/images');
        require('apps/entities/navigation');
        require('apps/entities/flyer_images');
        require('apps/entities/authenticate');
    });
    TourManager.addInitializer(function () {
        /*var SessionModel = reqres.request('session:entity:model');
         app.session = new SessionModel({});
         app.session.checkAuth();*/
    });
    TourManager.addInitializer(function () {
        reqres.setHandler('app', function () {
            return TourManager;
        });
        var SessionModel = reqres.request('session:entity:model');
        TourManager.session = new SessionModel({});
        /*require('apps/upload_tour/upload_tour_app');
         require('apps/edit_tour/edit_tour_app');
         require('apps/authenticate/authenticate_app');*/
    });
    TourManager.on("initialize:after", function () {
        TourManager.session.checkAuth({
            // Start the backbone routing once we have captured a user's auth status
            complete: function () {
                if (Backbone.history) {
                    require(['apps/upload_tour/upload_tour_app', 'apps/edit_tour/edit_tour_app', 'apps/authenticate/authenticate_app'], function () {
                        Backbone.history.start();
                        //set a custom application state
                        if (reqres.request('getCurrentRoute') === "") {
                            var vent = require('vent');
                            vent.trigger("showTourLibrary");
                        }
                    });
                }
            }
        }, undefined);
    });
    return TourManager;
});

