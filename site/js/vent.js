/**
 * Created by alexmasita on 10/1/13.
 */

define(function (require) {
    'use strict';

    var wreqr = require('backbone.wreqr');
    //return new Backbone.Wreqr.EventAggregator();
    return new wreqr.EventAggregator();
    /*
     var Backbone = require('backbone');
     return new Backbone.Wreqr.EventAggregator();
     */
});