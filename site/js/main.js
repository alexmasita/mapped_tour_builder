
require.config({
    /*    urlArgs: "bust=" +  (new Date()).getTime(),*/
    paths: {
        'jquery': 'lib/jquery-1.10.2',
        'autocomplete': 'lib/jquery.autocomplete',
        'underscore': 'lib/underscore',
        'text': 'lib/require/text',
        'jqueryui': 'lib/jquery-ui-1.10.0.custom.min',//lib/jquery-ui-1.10.0.custom.min
        'jquery.ui.widget': 'lib/upload/vendor/jquery.ui.widget',
        'jquery.iframe-transport': 'lib/upload/jquery.iframe-transport',
        'jquery.fileupload': 'lib/upload/jquery.fileupload',
        'jquery.scrollintoview': 'lib/jquery.scrollintoview',
        'mutate.events': 'lib/mutate.events',
        'mutate.min': 'lib/mutate.min',
        'handleBars': 'lib/handlebars',
        'bootstrap': 'lib/bootstrap.min',
        'bootstrap.slider': 'lib/bootstrap-slider',
        'backbone': 'lib/backbone',
        'marionette.original': 'lib/backbone.marionette',
        'marionette': 'lib/backbone.marionette.extended',
        'tourjs': 'lib/tour',
        'addressjs': 'lib/swfaddress/swfaddress',
        'domReady': 'lib/domReady',
        'affix': 'lib/affix',
        'swfkrpano': 'lib/swfkrpano',
        'krpanoiphone': 'lib/krpanoiphone',
        'krpanoiphone.license': 'lib/krpanoiphone.license',
        'backbone.babysitter': 'lib/backbone.babysitter',
        'backbone.wreqr': 'lib/backbone.wreqr',
        'backbone.picky': 'lib/backbone.picky'/*,
        'mapbox': 'https://api.tiles.mapbox.com/mapbox.js/v1.6.1/mapbox.js'*/
    },
    /*enforceDefine:true,https://api.tiles.mapbox.com/mapbox.js/v1.6.1/mapbox.js*/
    shim: {
        "bootstrap": ["jqueryui", "jquery"],
        "bootstrap.slider": ["jquery"],
        "affix": ["jquery"],
        "underscore": {
            deps: [],
            exports: "_"
        },
        "backbone": {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },
        "https://api.tiles.mapbox.com/mapbox.js/v1.6.2/mapbox.js": {
            exports: 'L'
        },
        "//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.24.0/L.Control.Locate.js": {
            deps: ["https://api.tiles.mapbox.com/mapbox.js/v1.6.2/mapbox.js"]
        },
        "handleBars": {
            deps: [],
            exports: "Handlebars"
        },
        "jqueryui": {
            deps: ["jquery"]/*,
             exports:"$"*/
        },
        "jquery.scrollintoview": {
            deps: ["jquery"]
        },
        "mutate.min": {
            deps: ["mutate.events", "jquery"]
        },
        "mutate.events": {
            deps: ["jquery"]
        },
        "jquery.ui.widget": ["jquery"],
        "jquery.iframe-transport": ["jquery"],
        "jquery.fileupload": ["jquery.ui.widget", "jquery.iframe-transport", ],
        "Class.SubObjectMapping": ["mootools-core", "swfkrpano"],
        "pano": ["Class.SubObjectMapping", "swfkrpano"]
    },
    deps: ['jquery', 'underscore']

});
requirejs.onError = function (err) {
    'use strict';
    console.log(err.requireType);

    if (err.requireType === 'timeout') {
        console.log('modules: ' + err.requireModules);
    }

    throw err;
};
/*function onLoadHandler(){
 alert('Yay hay');
 }*/
var onLoadHandler;
var getHotspotCount;
var onHotspotClick;

define(function (require) {
    'use strict';
    require(['domReady!'], function (doc) {
        require(['app', 'apps/main/main_view_menu_app'], function (app) {
            app.start();
        });
    });
});