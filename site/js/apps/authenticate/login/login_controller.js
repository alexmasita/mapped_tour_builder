/**
 * Created by alexmasita on 10/22/13.
 */
define(function (require) {
    'use strict';
    var TourManager = require('app'),
        LoginView = require('apps/authenticate/login/login_view'),
        marionette = require('marionette'),
        commands = require('commands'),
        reqres = require('reqres'),
        vent = require('vent'),
        $ = require('jquery');
    TourManager.module('authenticateApp.login', function (login, TourManager, Backbone, Marionette, $, _) {
        login.controller = {
            showLogin: function (mainRegion, isLogin) {
                var MainViewLayout = require('apps/upload_tour/views/main_view_layout'),
                    MainViewMenu = require('apps/main/views/main_view_menu'),
                    UserModel = reqres.request('user:entity:model'),
                    mainViewLayout = new MainViewLayout(),
                    mainViewMenu = new MainViewMenu(),
                    loginView = new LoginView.login({model: new UserModel({isLogin: isLogin}), isLogin: isLogin});
                mainViewLayout.on('show', function () {
                    mainViewLayout.appHeader.show(mainViewMenu);
                    mainViewLayout.appContainer.show(loginView);
                });
                mainViewMenu.on('renderTourLibrary', function () {
                    vent.trigger("showTourLibrary");
                });
                mainRegion.show(mainViewLayout);
            }
        };
    });
    return TourManager.authenticateApp.login.controller;
        /*login_controller = marionette.Controller.extend({
            initialize: function (options) {
                //this.mainRegion = options.mainRegion;
            },
            showLogin: function (mainRegion, isLogin) {
                var MainViewLayout = require('apps/upload_tour/views/main_view_layout'),
                    MainViewMenu = require('apps/main/views/main_view_menu'),
                    LoginView = require('apps/authenticate/login/login_view'),
                    UserModel = reqres.request('user:entity:model'),
                    mainViewLayout = new MainViewLayout(),
                    mainViewMenu = new MainViewMenu(),
                    loginView = new LoginView({model: new UserModel({isLogin: isLogin}), isLogin: isLogin});
                mainViewLayout.on('show', function () {
                    mainViewLayout.appHeader.show(mainViewMenu);
                    mainViewLayout.appContainer.show(loginView);
                });
                mainViewMenu.on('renderTourLibrary', function () {
                    vent.trigger("showTourLibrary");
                });
                mainRegion.show(mainViewLayout);
            }
        });
    return login_controller;*/
});