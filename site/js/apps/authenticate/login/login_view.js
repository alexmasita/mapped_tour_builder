define(function (require) {
    'use strict';
    var TourManager = require('app'),
        $ = require('jquery'),
        marionette = require('marionette'),
        templates = require('templates'),
        reqres = require('reqres'),
        commands = require('commands'),
        vent = require('vent');
    TourManager.module('authenticateApp.login.view', function (view, TourManager, Backbone, Marionette, $, _) {
        view.login = marionette.ItemView.extend({
            template: templates.loginViewTemplate,
            initialize: function (options) {
                this.isLogin = options.isLogin;
            },
            onRender: function () {
                if (this.isLogin) {
                    this.signInCommand();
                } else {
                    this.signUpCommand();
                }
            },
            events: {
                'click #btn-login': 'login',
                'click #btn-signup-login': 'showSignUp',
                'click @ui.btnSignInLink': 'showSignIn',
                'click #btn-signup': 'signUp'
            },
            ui: {
                firstname: '#firstname',
                lastname: '#lastname',
                username: '#username',
                password: '#password',
                email: '#email',
                loginBox: '#loginbox',
                signUpBox: '#signupbox',
                btnSignInLink: '#signinlink',
                loginUserName: '#login-username',
                loginPassword: '#login-password'
            },
            /*login: function (e) {
             e.preventDefault();
             this.model.save({username: this.ui.loginUserName.val(), password: this.ui.loginPassword.val()},
             {success: function (model, response, options) {
             if (options.xhr.status === 200) {
             vent.trigger("showTourLibrary");
             commands.execute('set:login:name', model.get('firstname') + ' ' + model.get('lastname'));
             }
             }, error: function (model, response, options) {
             console.log('failure ' + response);
             }});
             },*/
            login: function (e) {
                if (e) {
                    e.preventDefault();
                }
                var that = this;
                TourManager.session.login({
                    username: that.ui.loginUserName.val(),
                    password: that.ui.loginPassword.val()
                });
            /*
                this.model.save({username: this.ui.loginUserName.val(), password: this.ui.loginPassword.val()},
                    {success: function (model, response, options) {
                        if (options.xhr.status === 200) {
                            vent.trigger("showTourLibrary");
                            commands.execute('set:login:name', model.get('firstname') + ' ' + model.get('lastname'));
                        }
                    }, error: function (model, response, options) {
                        console.log('failure ' + response);
                    }});
*/
            },
            showSignUp: function (e) {
                e.preventDefault();
                vent.trigger('show:sign:up');
            },
            showSignIn: function (e) {
                e.preventDefault();
                vent.trigger('show:sign:in');
            },
            signInCommand: function () {
                this.ui.signUpBox.hide();
                this.ui.loginBox.show();
            },
            signUpCommand: function () {
                this.ui.loginBox.hide();
                this.ui.signUpBox.show();
            },
            /*signUp: function (e) {
                e.preventDefault();
                var that = this;
                this.model.save({firstname: that.ui.firstname.val(),
                    lastname: that.ui.lastname.val(),
                    username: that.ui.username.val() || that.ui.email.val(),
                    password: that.ui.password.val(),
                    email: that.ui.email.val()});
            },*/
            signUp: function (e) {
                if (e) {
                    e.preventDefault();
                }
                var that = this;
                TourManager.session.signup({firstname: that.ui.firstname.val(),
                    lastname: that.ui.lastname.val(),
                    username: that.ui.username.val() || that.ui.email.val(),
                    password: that.ui.password.val(),
                    email: that.ui.email.val()});
            }
        });
    });
    return TourManager.authenticateApp.login.view;
});