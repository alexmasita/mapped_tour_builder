define(function (require) {
    'use strict';
    var TourManager = require('app'),
        templates = require('templates'),
        ENTER_KEY = 13;
    TourManager.module('authenticateApp.list.view', function (view, TourManager, Backbone, Marionette, $, _) {
        view.user = Marionette.ItemView.extend({
            tagName: 'a',
            className: 'list-group-item',
            template: templates.authenticateListViewTemplate,
            modelEvents: {
                'change': 'render'
            },
            events: {
                'dblclick .user-email .show-user': 'editUserEmail',
                'dblclick .user-password .show-user': 'editUserPassword',
                'dblclick .user-role .show-user': 'editUserRole',
                'blur .user-email .edit-user': 'closeUserEmail',
                'blur .user-password .edit-user': 'closeUserPassword',
                'blur .user-role .edit-user': 'closeUserRole',
                'keypress .user-email input': 'onUpdateUserEmail',
                'keypress .user-password input': 'onUpdateUserPassword',
                'keypress .user-role input': 'onUpdateUserRole',
                'click .close': 'onDelete'
            },
            ui: {
                useremail: '.user-email',
                userpassword: '.user-password',
                userrole: '.user-role',
                userEmailInput: '.user-email input',
                userPasswordInput: '.user-password input',
                userRoleInput: '.user-role input',
                userClose: '.close'
            },
            editUserEmail: function (e) {
                e.preventDefault();
                e.stopPropagation();
                this.ui.useremail.addClass('edit');
                this.ui.userEmailInput.focus();
                this.ui.userEmailInput.select();
            },
            editUserPassword: function (e) {
                e.preventDefault();
                e.stopPropagation();
                this.ui.userpassword.addClass('edit');
                this.ui.userPasswordInput.focus();
                this.ui.userPasswordInput.select();
            },
            editUserRole: function (e) {
                e.preventDefault();
                e.stopPropagation();
                this.ui.userrole.addClass('edit');
                this.ui.userRoleInput.focus();
                this.ui.userRoleInput.select();
            },
            onDelete: function (e) {
                if (e) {e.preventDefault(); }
                var result = confirm("Want to delete?");
                if (result === true) {
                    this.model.destroy();
                }
            },
            closeUserEmail: function () {
                this.model.save({email: this.ui.userEmailInput.val()}, {success: function (model, response, options) {
                    //alert('UserName Save Succeeded');
                }});
                this.ui.useremail.removeClass('edit');
            },
            closeUserPassword: function () {
                this.model.save({password: this.ui.userPasswordInput.val()});
                this.ui.userpassword.removeClass('edit');
            },
            closeUserRole: function () {
                this.model.save({role: this.ui.userRoleInput.val()}, {success: function (model, response, options) {
                    //alert('Role Save Succeeded');
                }});
                this.ui.userrole.removeClass('edit');
            },
            onUpdateUserEmail: function (e) {
                if (e.which === ENTER_KEY) {
                    e.preventDefault();
                    this.closeUserEmail();
                }
            },
            onUpdateUserPassword: function (e) {
                if (e.which === ENTER_KEY) {
                    e.preventDefault();
                    this.closeUserPassword();
                }
            },
            onUpdateUserRole: function (e) {
                if (e.which === ENTER_KEY) {
                    e.preventDefault();
                    this.closeUserRole();
                }
            }
        });
        var noUsersView = Marionette.ItemView.extend({
            tagName: 'a',
            className: 'list-group-item list-group-item-danger',
            template: templates.NoneUserTemplate
        });
        view.users = Marionette.CompositeView.extend({
            template: templates.authenticateListViewContainerTemplate,
            itemViewContainer: '.list-group',
            itemView: view.user,
            emptyView: noUsersView
        });
    });
    return TourManager.authenticateApp.list.view;
});