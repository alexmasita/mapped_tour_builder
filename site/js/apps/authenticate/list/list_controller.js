define(function (require) {
    'use strict';
    var TourManager = require('app'),
        ListView = require('apps/authenticate/list/list_view'),
        reqres = require('reqres'),
        vent = require('vent');
    TourManager.module('authenticateApp.list', function (List, TourManager, Backbone, Marionette, $, _) {
        List.controller = {
            listUsers: function () {
                var MainViewLayout = require('apps/upload_tour/views/main_view_layout'),
                    //MainViewMenu = require('apps/main/views/main_view_menu'),
                    mainViewLayout = new MainViewLayout(),
                    //mainViewMenu = new MainViewMenu(),
                    fetchingUsers = reqres.request('user:entities');

                $.when(fetchingUsers).done(function (users, response, options) {
                    if (response === 401 || response === 403) {
                        return vent.trigger('show:sign:in');
                    }
                    var UsersListView = new ListView.users({collection: users});
                    mainViewLayout.on('show', function () {
                        //mainViewLayout.appHeader.show(mainViewMenu);
                        mainViewLayout.appContainer.show(UsersListView);
                    });
                    /*mainViewMenu.on('renderTourLibrary', function () {
                        vent.trigger("showTourLibrary");
                    });*/
                    TourManager.application.show(mainViewLayout);
                });
            }
        };
    });
    return TourManager.authenticateApp.list.controller;
});