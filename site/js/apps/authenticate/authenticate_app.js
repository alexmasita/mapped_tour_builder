define(function (require) {
    'use strict';

    var TourManager = require('app'),
        reqres = require('reqres'),
        vent = require('vent'),
        commands = require('commands');
    TourManager.module('routers.authenticateApp', function (AuthenticateAppRouter, TourManager, Backbone, Marionette, $, _) {

        AuthenticateAppRouter.router = Marionette.AppRouter.extend({
            appRoutes: {
                //"users": "listUsers",
                //"users/:id": "showUser",
                //"users/:id/edit": "editUser",
                "login": "showLogin",
                "register": "showRegister",
                "list": "listUsers"
            }
        });
        var api = {
            showLogin: function () {
                //var app = reqres.request('app');
                require(['apps/authenticate/login/login_controller'], function (loginController) {
                    loginController.showLogin(TourManager.application, true);
                });
                //****** Manage selection state here ******
            },
            showRegister: function () {
                //var app = reqres.request('app');
                require(['apps/authenticate/login/login_controller'], function (loginController) {
                    loginController.showLogin(TourManager.application, false);
                });
            },
            listUsers: function () {
                require(['apps/authenticate/list/list_controller'], function (ListController) {
                    ListController.listUsers();
                });
            }
        };
        vent.on('show:sign:up', function () {
            api.showRegister();
            commands.execute('navigate', 'register');
        });
        vent.on('show:sign:in', function () {
            api.showLogin();
            commands.execute('navigate', 'login');
        });
        TourManager.addInitializer(function () {
            new AuthenticateAppRouter.router({
                controller: api
            });
        });

    });

    return TourManager.routers.authenticateApp;
});