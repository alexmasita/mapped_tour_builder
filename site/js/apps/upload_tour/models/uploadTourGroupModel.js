define(function (require) {
    'use strict';
    var Backbone = require('backbone'),
        picky = require('picky'),
        _ = require('underscore'),
        uploadTourGroupModel = Backbone.Model.extend({
            idAttribute: '_id',
            validate: function (attrs, options) {
                if (!attrs.groupName) {
                    return "You must set the Group Name!";
                }
            },
            initialize: function () {
                var selectable = new picky.Selectable(this);
                _.extend(this, selectable);
                //Initialize shared object and functions
            }
        });
    return uploadTourGroupModel;
});
