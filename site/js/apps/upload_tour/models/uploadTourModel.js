/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 8/17/13
 * Time: 8:29 PM
 * To change this template use File | Settings | File Templates.
 */


define(function (require) {
    'use strict';
    var _ = require('underscore'),
        Backbone = require('backbone'),
        picky = require('picky'),
        tourModel = Backbone.Model.extend({
            defaults: {
                coverImage: 'img/placeholder.png',
                propertyName: "",
                filename: "",
                description: "",
                scenes: [],
                config: {
                    menus: [],//name="item0",type="category",text="entrance",load="entranceGroup"
                    autoViews: []
                }
            },
            validate: function (attrs, options) {
                if (!attrs.filename) {
                    return "You must select a file to upload!";
                }
            },
            parse: function (response) {
                response.id = response._id;
                return response;
            },
            initialize: function () {
                var selectable = new picky.Selectable(this);
                _.extend(this, selectable);
            }

        });
    return tourModel;

});



