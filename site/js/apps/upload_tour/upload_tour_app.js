define(function (require) {
    'use strict';

    var Router = require('apps/upload_tour/Router/Router'),
        reqres = require('reqres'),
        vent = require('vent'),
        commands = require('commands'),
        uploadTourController,
        UploadTourControl = require('apps/upload_tour/controller/upload_tour_controller'),
        api = {
            show: function () {
                var app = reqres.request('app');
                uploadTourController = new UploadTourControl({mainRegion: app.application});
                uploadTourController.show();
                uploadTourController._tourGroupLibraryContent();
                //****** Manage selection state here ******
            },
            showTours: function (id, tourId) {
                var app = reqres.request('app');
                if (!uploadTourController) {
                    uploadTourController = new UploadTourControl({mainRegion: app.application});
                }
                uploadTourController.show();
                uploadTourController._tourLibraryContent(id, tourId);
            }
        };
    vent.on("showTourLibrary", function () {
        api.show();
        commands.execute('navigate', '');
    });
    vent.on("show-tours:trigger", function (model) {
        var _id = model.get('_id');
        api.showTours(_id);
        commands.execute('navigate', 'tours/' + _id);
    });
    vent.on("show-tours:trigger:all", function () {
        api.showTours('');
        commands.execute('navigate', 'tours');
    });
    return new Router({controller: api});
});