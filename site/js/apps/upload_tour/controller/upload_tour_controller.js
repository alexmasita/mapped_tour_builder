/**
 * Created by alexmasita on 10/22/13.
 */
define(function (require) {
    'use strict';
    var marionette = require('marionette'),
        commands = require('commands'),
        reqres = require('reqres'),
        vent = require('vent'),
        $ = require('jquery'),
        upload_tour_controller = marionette.Controller.extend({
            initialize: function (options) {
                this.mainRegion = options.mainRegion;
            },
            show: function () {
                var layout = this._getMainViewLayout();
                this.mainRegion.show(layout);
            },
            _getMainViewLayout: function () {
                require('bootstrap');
                require('affix');

                var MainViewLayout = require('apps/upload_tour/views/main_view_layout'),
                    that = this;
                this.mainViewlayout = new MainViewLayout();
                this.listenTo(this.mainViewlayout, 'show', function () {
                    that._showMenuAndContent(this.mainViewlayout);
                });
                return this.mainViewlayout;
            },
            _showMenuAndContent: function (layout) {
                this.appHeader = layout.appHeader;
                this.appContainer = layout.appContainer;
                var that = this;/*menu = this._addMenu(this.appHeader),
                 */
                /*menu.on("renderTourLibrary", function () {
                    vent.trigger("showTourLibrary");
                });*/
            },
            _addMenu: function (region) {
                var MainViewMenu = require('apps/main/views/main_view_menu'),
                    menu = new MainViewMenu();
                region.show(menu);
                return menu;
            },
            _tourLibraryContent: function (_id, tourId) {
                reqres.setHandler('selectedTourID', function () {
                    return _id;
                });
                var promise = reqres.request('tours:entities', _id),
                    that = this;
                $.when(promise).done(function (collection) {
                    var TourLibraryView, tourLibrary;
                    if (collection !== undefined) {
                        TourLibraryView = require('apps/upload_tour/views/uploadTourLibraryView');
                        tourLibrary = new TourLibraryView({collection: collection});
                        tourLibrary.on('itemview:upload:model:save', function (childView, model, options) {
                            model.save(options);
                        });
                        tourLibrary.on('trigger:viewAll', function () {
                            vent.trigger('show-tours:trigger:all');
                        });
                        tourLibrary.on('itemview:tour:item:selected', function (childView, model, options) {
                            tourLibrary.collection.trigger('reset');
                        });
                        tourLibrary.on('render', function () {
                            //alert('On render called');
                            if (tourId) {
                                //alert(tourId);
                                tourLibrary.tourItemSelectedById(tourId);
                            }
                        });
                        //tour:item:selected
                    }
                    that.appContainer.show(tourLibrary);
                });
            },
            _tourGroupLibraryContent: function () {
                var TourGroupLibraryView = require('apps/upload_tour/views/uploadTourGroupLibraryView'),
                    TourGroupLibrary = new TourGroupLibraryView(),
                    that = this;
                TourGroupLibrary.on('itemview:group:model:save', function (childView, model, options) {
                    model.save(options);
                });
                TourGroupLibrary.on('itemview:show-tours:trigger', function (childView, model, options) {
                    that._tourLibraryContent();
                });
                this.appContainer.show(TourGroupLibrary);
            }
        });
    return upload_tour_controller;
});