define(function (require) {
    "use strict";
    var $ = require('jquery'),
        reqres = require('reqres'),
        _ = require('underscore'),
        autoComplete = function (element) {

            var data,
                convertedData,
                promise = reqres.request('tour_groups:entities');//defer.promise();

            $.when(promise).done(function (collection) {
                data = collection.toJSON();
                convertedData = _.map(data, function (sug) { return {value: sug.groupName, data: sug._id, description: sug.groupDescription}; });
                require(['autocomplete'], function () {
                    //alert(JSON.stringify(data));
                    element.autocomplete({
                        lookup: convertedData,
                        onSelect: function (suggestion) {
                            //alert(JSON.stringify(suggestion));
                            element.data('groupData', suggestion);
                            //element.val(suggestion.value);
                        }
                    });
                });
            });
            return promise;
        };
    return autoComplete;
});
