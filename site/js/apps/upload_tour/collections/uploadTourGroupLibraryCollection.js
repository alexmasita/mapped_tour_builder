define(function (require) {
    'use strict';
    var _ = require('underscore'),
        Backbone = require('backbone'),
        tourGroupModel = require('apps/upload_tour/models/uploadTourGroupModel'),
        tourGroupLibrary = Backbone.Collection.extend({
            model: tourGroupModel,
            url: '/api/tourGroups'
        });
    return tourGroupLibrary;
});
