/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 8/29/13
 * Time: 8:38 PM
 * To change this template use File | Settings | File Templates.
 */
//var app = app || {};

define(function (require) {
    'use strict';
    var _ = require('underscore'),
        Backbone = require('backbone'),
        tourModel = require('apps/upload_tour/models/uploadTourModel'),
        picky = require('picky'),
        tourLibrary = Backbone.Collection.extend({
            model: tourModel,
            url: function () {
                return '/api/tours/';// + this._id;
            },
            nextOrder: function () {
                if (!this.length) {
                    return 1;
                }
                return this.first().get('order') + 1;
            },
            comparator: function (tour) {
                return -tour.get('order');
            },
            initialize: function (options) {
                var singleSelect = new picky.SingleSelect(this);
                _.extend(this, singleSelect);
                //this._id = options._id;
                //this.isGroupFilter = options.isGroupFilter;
            }
        });
    return tourLibrary;
});