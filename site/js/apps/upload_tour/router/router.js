/**
 * Created by alexmasita on 10/6/13.
 */

define(function (require) {
    'use strict';
    var marionette = require('marionette'),
        router = marionette.AppRouter.extend({
            initialize: function () {
                //alert('selection router initialized');
            },
            appRoutes: {
                //"edit/:folderName(/:filename)": "showSelection"
                "": "show",
                "tours(/:groupName)(/:tourName)": "showTours"

            }
        });
    return router;
});