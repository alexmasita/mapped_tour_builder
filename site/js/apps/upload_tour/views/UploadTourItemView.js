/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 8/29/13
 * Time: 8:22 PM
 * To change this template use File | Settings | File Templates.
 */


define(function (require) {
    'use strict';
    var ENTER_KEY = 13,
        templates = require('templates'),
        vent = require('vent'),
        marionette = require('marionette'),
        commands = require('commands'),
        reqres = require('reqres'),
        autoCompleteModule = require('apps/upload_tour/plugins/autoComplete'),
        $ = require('jquery'),
        uploadTourView = marionette.ItemView.extend({
            tagName: 'a',//changed from div
            className: 'tourContainer row list-group-item',
            template: templates.UploadTourItemTemplate,
            initialize: function (options) {
                this.canEditTours = false;
                if (options.role === 'SuperUser') {
                    this.canEditTours = true;
                }
                this.model.set({canEditTours: this.canEditTours});
            },
            events: {
                'click': 'selectTourItem',
                'click .btn-delete': 'deleteTour',
                'click .btn-edit': 'editTour',
                'click .btn-save': 'saveTour',
                'dblclick .upload-header': 'editHeader',
                'dblclick .upload-detail': 'editDetail',
                'dblclick .tour-detail': 'tourDetail',
                'dblclick .group-detail': 'groupDetail',
                'blur .upload-header .detail': 'closeHeader',
                'blur .upload-detail .detail': 'closeDetail',
                'blur .tour-detail .detail': 'closeTourDetail',
                /*'blur #autocomplete': 'closeGroupDetail',*/
                'keypress .upload-property-name': 'updatePropetyNameOnEnter',
                'keypress .upload-property-description': 'updatePropertyDetailOnEnter',
                'keypress #autocomplete': 'updateGroupOnEnter',
                'focus #autocomplete': 'getAutoComplete',
                'click .btn-data': 'getValue'
            },
            selectTourItem: function (e) {
                e.preventDefault();
                if (!this.model.selected) {
                    this.model.select();
                    this.trigger('tour:item:selected', this.model);
                }
            },
            getValue: function (e) {
                //alert(this.ui.autoComplete.val());
                e.preventDefault();
                //alert(JSON.stringify(this.ui.autoComplete.data('groupData')));
                //e.preventDefault();
            },
            getAutoComplete: function (e) {
                //alert(this.ui.autoComplete.data('groupData'));
                //if (!this.ui.autoComplete.data('groupData')) {
                autoCompleteModule(this.ui.autoComplete);
                //}
            },
            ui: {
                autoComplete: '#autocomplete'
            },
            modelEvents: {
                "change": "render"
            },
            updatePropetyNameOnEnter: function (e) {
                if (e.which === ENTER_KEY) {
                    e.preventDefault();
                    this.closeHeader();
                    //this.render();
                }
            },
            updatePropertyDetailOnEnter: function (e) {
                if (e.which === ENTER_KEY) {
                    e.preventDefault();
                    this.closeDetail();
                    //this.render();
                }
            },
            updateGroupOnEnter: function (e) {
                if (e.which === ENTER_KEY) {
                    e.preventDefault();
                    this.closeGroupDetail();
                }
            },
            editHeader: function (e) {
                e.preventDefault();
                if (this.canEditTours) {
                    this.$('.upload-header').addClass('edit');
                    this.$inputHeader.focus();
                    this.$inputHeader.select();
                }

            },
            editDetail: function (e) {
                e.preventDefault();
                if (this.canEditTours) {
                    this.$('.upload-detail').addClass('edit');
                    this.$inputDetail.focus();
                    this.$inputDetail.select();
                }
            },
            tourDetail: function (e) {
                e.preventDefault();
                if (this.canEditTours) {
                    this.$('.tour-detail').addClass('edit');
                    this.$tourLink.focus();
                    this.$tourLink.select();
                }
            },
            groupDetail: function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (this.canEditTours) {
                    this.$('.group-detail').addClass('edit');
                    this.$groupDetail.focus();
                    this.$groupDetail.select();
                }
            },
            closeTourDetail: function () {
                var valueBody = this.$tourLink.val().trim();
                if (valueBody) {
                    this.trigger('upload:model:save', this.model, {tourLink: valueBody});
                }
                this.$('.tour-detail').removeClass('edit');
            },
            closeHeader: function () {
                var valueHeader = this.$inputHeader.val().trim(),
                    valueBody = this.$inputDetail.val().trim(),
                    capitaliseFirstLetter = function (string) {
                        return string.charAt(0).toUpperCase() + string.slice(1);
                    };
                if (valueHeader) {
                    this.trigger('upload:model:save', this.model, valueBody ? {propertyName: valueHeader} : {propertyName: valueHeader, description: capitaliseFirstLetter(valueHeader)});
                }
                this.$('.upload-header').removeClass('edit');
            },
            closeDetail: function () {
                var valueBody = this.$inputDetail.val().trim();
                if (valueBody) {
                    //this.model.save({description:valueBody});
                    this.trigger('upload:model:save', this.model, {description: valueBody});
                }
                this.$('.upload-detail').removeClass('edit');
            },
            closeGroupDetail: function () {
                var groupObj = this.ui.autoComplete.data('groupData');
                if (groupObj) {
                    //this.model.save({description:valueBody});
                    //this.trigger('upload:model:save', this.model, {_group: groupObj.data});
                    this.trigger('upload:model:save', this.model, {_group: {_id: groupObj.data, groupName: groupObj.value, groupDescription: groupObj.description}});
                }
                this.$('.group-detail').removeClass('edit');
            },
            saveTour: function () {
                var lat, lon;
                lat = reqres.request('latitude');
                lon = reqres.request('longitude');
                //alert('save tour called lat ' + lat + ' lon ' + lon);
                if (lat) {
                    this.trigger('upload:model:save', this.model, {latitude: lat, longitude: lon});
                }
            },
            onRender: function () {
                this.$inputHeader = this.$(".upload-property-name");
                this.$inputDetail = this.$(".upload-property-description");
                this.$tourLink = this.$(".tour-link");
                this.$groupDetail = this.$("#autocomplete");
                if (this.model.selected) {
                    this.$el.addClass('active');
                }
            },
            deleteTour: function () {
                var result = confirm("Sure You Want to delete Tour?");
                if (result === true) {
                    this.model.destroy();
                    this.remove();
                }
            },
            editTour: function () {
                this.setGlobals(this.model);
                vent.trigger("edit_tour_app:show", reqres.request('folderName'));
                this.model.trigger("removeParent");
            },
            setGlobals: function (model) {
                var that = this;
                reqres.setHandlers({
                    'folderName': function () {
                        return that.model.get('filename');
                    },
                    'propertyName': function () {
                        return that.model.get('propertyName');
                    }
                });
            }
        });
    return uploadTourView;
});
//var app = app || {};

