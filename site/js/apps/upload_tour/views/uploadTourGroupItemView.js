/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 8/29/13
 * Time: 8:22 PM
 * To change this template use File | Settings | File Templates.
 */


define(function (require) {
    'use strict';
    var ENTER_KEY = 13,
        templates = require('templates'),
        vent = require('vent'),
        marionette = require('marionette'),
        reqres = require('reqres'),
        uploadTourGroupItemView = marionette.ItemView.extend({
            tagName: 'div',
            className: 'tourContainer row',
            template: templates.UploadTourGroupItemTemplate,
            initialize: function (options) {
                var canDelete = false;
                if (options.role === 'SuperUser') {
                    canDelete = true;
                }
                this.model.set({canDelete: canDelete});
            },
            events: {
                'click .btn-delete': 'deleteTour',
                'click .btn-show-tours': 'showTours',
                'dblclick .upload-header': 'editHeader',
                'dblclick .upload-detail': 'editDetail',
                'blur .upload-header .detail': 'closeHeader',
                'blur .upload-detail .detail': 'closeDetail',
                'keypress .upload-group-name': 'updateGroupNameOnEnter',
                'keypress .upload-group-description': 'updateGroupDetailOnEnter'
            },
            modelEvents: {
                "change": "render"
            },
            updateGroupNameOnEnter: function (e) {
                if (e.which === ENTER_KEY) {
                    e.preventDefault();
                    this.closeHeader();
                    //this.render();
                }
            },
            updateGroupDetailOnEnter: function (e) {
                if (e.which === ENTER_KEY) {
                    e.preventDefault();
                    this.closeDetail();
                    //this.render();
                }
            },
            editHeader: function () {
                this.ui.uploadHeader.addClass('edit');
                this.ui.inputHeader.focus();
                this.ui.inputHeader.select();
            },
            editDetail: function () {
                this.ui.uploadDetail.addClass('edit');
                this.ui.inputDetail.focus();
                this.ui.inputDetail.select();
            },
            closeHeader: function () {
                var valueHeader = this.ui.inputHeader.val().trim(),
                    valueBody = this.ui.inputDetail.val().trim(),
                    capitaliseFirstLetter = function (string) {
                        return string.charAt(0).toUpperCase() + string.slice(1);
                    };
                if (valueHeader) {
                    this.trigger('group:model:save', this.model, valueBody ? {groupName: valueHeader} : {groupName: valueHeader, description: capitaliseFirstLetter(valueHeader)});
                }
                this.$('.upload-header').removeClass('edit');
            },
            closeDetail: function () {
                var valueBody = this.ui.inputDetail.val().trim();
                if (valueBody) {
                    //this.model.save({description:valueBody});
                    this.trigger('group:model:save', this.model, {description: valueBody});
                }
                this.$('.upload-detail').removeClass('edit');
            },
    /*        onRender: function () {
                this.$inputHeader = this.$(".upload-property-name");
                this.$inputDetail = this.$(".upload-property-description");
            },*/
            ui: {
                inputHeader: ".upload-Group-name",
                inputDetail: ".upload-Group-description",
                uploadHeader: ".upload-header",
                uploadDetail: ".upload-detail"
            },
            deleteTour: function () {
                var result = confirm("Sure You Want to delete Tour?");
                if (result === true) {
                    this.model.destroy();
                    this.remove();
                }
            },
            showTours: function () {
                this.setGlobals(this.model);
                vent.trigger("show-tours:trigger", this.model);
                this.model.trigger("removeParent");
            },
            setGlobals: function (model) {
                var that = this;
                reqres.setHandlers({
                    'groupName': function () {
                        return that.model.get('groupName');
                    }
                });
            }
        });
    return uploadTourGroupItemView;
});

