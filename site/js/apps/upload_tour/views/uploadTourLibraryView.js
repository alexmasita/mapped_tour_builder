/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 8/29/13
 * Time: 8:41 PM
 * To change this template use File | Settings | File Templates.
 */
//var app = app || {};

define(function (require) {
    'use strict';
    var TourManager = require('app'),
        commands = require('commands'),
        Backbone = require('backbone'),
        uploadTourItemView = require('apps/upload_tour/views/UploadTourItemView'),
        UploadTourModel = require('apps/upload_tour/models/UploadTourModel'),
        marionette = require('marionette'),
        templates = require('templates'),
        $ = require('jquery'),
        reqres = require('reqres'),
        UploadTourLibraryView = marionette.CompositeView.extend({
            id: 'tours',
            itemView: uploadTourItemView,
            itemViewContainer: '#tourList',
            template: templates.UploadTourLibraryTemplate,
            initialize: function () {
                this.listenTo(TourManager.session, 'change:logged_in', this.reRender);
                //This view had no model so I set it here!!
                this.model = new Backbone.Model({canEditTours: false});
                this.onLoginStatusChange();
            },
            onLoginStatusChange: function () {
                if (TourManager.session.get('logged_in')) {
                    this.role = TourManager.session.user.get('role');
                } else {
                    this.role = '';
                }
                var that = this;
                this.model.set({canEditTours: function () {
                    return that.role === 'SuperUser';
                }});
            },
            reRender: function () {
                this.onLoginStatusChange();
                this.render();
            },
            itemViewOptions: function (model, index) {
                var roleName = this.role;
                return {
                    role: roleName
                };
            },
            bindToMutationEvents: function () {
                var that = this;
                require(['mutate.events', 'mutate.min'], function () {
                    $('#map').mutate('height width', function (el, info) {
                        /*expanded = true;
                         $('#heighter').text('height changed:' + $(el).height());
                         $('#widther').text('Width changed:' + $(el).width());https://api.tiles.mapbox.com/mapbox.js/v1.6.1/mapbox.js*/
                        that.map.closePopup();
                    });
                });
            },
            loadLocations: function (that) {
                require(["https://api.tiles.mapbox.com/mapbox.js/v1.6.2/mapbox.js", "//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.24.0/L.Control.Locate.js"], function (mapbox) {
                    var map;
                    map = L.mapbox.map('map', 'alexmasita.h9oaibc9');
                    that.map = map;
                    //map.on('load', function () {
                    that.locateGeojson();

                    map.addControl(L.mapbox.geocoderControl('alexmasita.h9oaibc9'));
                    L.control.locate().addTo(map);
                    //});
                    //map.setView([-1.27, 36.782], 6);
                });
            },
            /*locate: function () {
             //alert('locate called');
             if (this.map.hasLayer(this.placesLayer)) {
             this.map.removeLayer(this.placesLayer);
             }

             if (this.map.hasLayer(this.editMarkerLayer)) {
             this.map.removeLayer(this.editMarkerLayer);
             }

             var that = this;
             this.placesLayer = L.featureGroup().addTo(this.map);
             this.collection.forEach(function (tour) {
             if (tour.get('latitude') || tour.get('longitude')) {
             var latlng = L.latLng(tour.get('latitude'), tour.get('longitude')),
             marker = L.marker(latlng)
             .bindPopup('<h2><a href="http://www.zuru360.com">' +
             tour.get('propertyName') + '</a></h2>')
             .addTo(that.placesLayer);
             }
             });
             this.map.fitBounds(this.placesLayer.getBounds());
             },*/
            locateGeojson: function () {
                if (this.map.hasLayer(this.placesLayer)) {
                    this.map.removeLayer(this.placesLayer);
                }

                if (this.map.hasLayer(this.editMarkerLayer)) {
                    this.map.removeLayer(this.editMarkerLayer);
                }

                var geoJson = {
                        type: 'FeatureCollection',
                        features: []
                    },
                    that = this;
                this.collection.forEach(function (tour) {
                    if (tour.get('latitude') || tour.get('longitude')) {
                        var Feature = {type: 'Feature', properties: {_id: tour.get('_id'), propertyName: tour.get('propertyName'), description: tour.get('description'), tourLink: tour.get('tourLink'), title: tour.get('propertyName'), 'marker-color': '#1c88b7'}, geometry: {type: 'Point', coordinates: [tour.get('longitude'), tour.get('latitude')]}};
                        geoJson.features.push(Feature);
                    }
                });
                this.placesLayer = L.mapbox.featureLayer(geoJson).addTo(that.map);
                this.map.fitBounds(this.placesLayer.getBounds());
                this.placesLayer.eachLayer(function (layer) {
                    var address = 'http://www.zuru360.com/virtual_tour/stadia_slide/hd/Slide_Desk.html',//http://' + window.location.host + '/
                        addressTwo = layer.feature.properties.tourLink,
                        content = '<iframe id="vtour" src="' + addressTwo + '"></iframe>';
                    //console.log(content);
                    layer.bindPopup(content, {
                        closeButton: false,
                        /*minWidth: 718,*/
                        maxWidth: 1000,
                        autoPanPaddingBottomRight: [200, 200],
                        autoPanPaddingTopLeft: [50, 100]
                    });
                });

                this.placesLayer.on('click', function (e) {
                    //e.layer.unbindPopup();
                    //window.open(e.layer.feature.properties._id);
                    //alert('http://' + window.location.host + '/graceland_hd/Main.html');
                    var tourToSelect = that.collection.find(function (tour) {
                        return tour.get('_id') === e.layer.feature.properties._id;
                    });
                    //tourToSelect.select();
                    tourToSelect.toggleSelected();
                    that.collection.trigger('reset');
                    if (tourToSelect.selected) {
                        commands.execute('navigate', 'tours/' + reqres.request('selectedTourID') + '/' + e.layer.feature.properties._id);
                    } else {
                        commands.execute('navigate', 'tours/' + reqres.request('selectedTourID'));
                    }

                    require(['jquery.scrollintoview'], function () {
                        $('a.active').scrollintoview({
                            duration: 'slow',
                            direction: 'vertical',
                            complete: function () {
                                // highlight the element so user's focus gets where it needs to be
                            }
                        });
                    });
                });
                if (!this.ui.mapOverlay.hasClass('hide')) {
                    this.ui.mapOverlay.addClass('hide');
                }
                if (this.ui.editLocation.hasClass('swap')) {
                    this.ui.editLocation.removeClass('swap');
                }
            },
            itemEvents: {
                'tour:item:selected': 'tourItemSelected'
            },
            tourItemSelected: function (eventName, View, Model) {
                commands.execute('navigate', 'tours/' + reqres.request('selectedTourID') + '/' + Model.get('_id'));
                this.placesLayer.eachLayer(function (marker) {
                    if (marker.feature.properties._id === Model.get('_id')) {
                        marker.openPopup();
                    }
                });
            },
            tourItemSelectedById: function (id) {
                var that = this;
                require(["https://api.tiles.mapbox.com/mapbox.js/v1.6.2/mapbox.js", "//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.24.0/L.Control.Locate.js"], function (mapbox) {
                    //alert('open popup entered: after this.places set result ? '); // + JSON.stringify(this.placesLayer)
                    that.placesLayer.eachLayer(function (marker) {
                        if (marker.feature.properties._id === id) {
                            marker.openPopup();
                        }
                    });
                    var tourToSelect = that.collection.find(function (tour) {
                       // alert('value of tour.get = ' + tour.get('_id') + ' id = ' + id);
                        return tour.get('_id') === id;
                    });
                    tourToSelect.select();
                    that.collection.trigger('reset');
                    //tourToSelect.toggleSelected();

                });


            },
            triggers: {
                'click #btnViewAll': 'trigger:viewAll'
            },
            events: {
                'click #btnAdd': 'addTour',
                'click #btnCancel': 'cancelTour',
                'click #btnAddTour': 'addTourControls',
                'click @ui.btnLocate': 'locateGeojson',
                'click @ui.btnEdit': 'edit',
                'click @ui.hideList': 'hideList',
                'click @ui.showList': 'showList'
            },
            hideList: function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (!this.ui.tourListOverlay.hasClass('showMap')) {
                    this.ui.tourListOverlay.addClass('showMap');
                }
                if (!this.ui.map.hasClass('showMap')) {
                    this.ui.map.addClass('showMap');
                }
                if (!this.ui.showList.hasClass('showMap')) {
                    //alert('found showlist has no show map class');
                    this.ui.showList.addClass('showMap');
                }
                //this.map.closePopup();
            },
            showList: function () {
                if (this.ui.tourListOverlay.hasClass('showMap')) {
                    this.ui.tourListOverlay.removeClass('showMap');
                }
                if (this.ui.map.hasClass('showMap')) {
                    this.ui.map.removeClass('showMap');
                }
                if (this.ui.showList.hasClass('showMap')) {
                    this.ui.showList.removeClass('showMap');
                }
                //this.map.closePopup();
            },
            edit: function () {
                if (this.map.hasLayer(this.editMarkerLayer)) {
                    this.map.removeLayer(this.editMarkerLayer);
                }
                if (this.map.hasLayer(this.placesLayer)) {
                    this.map.removeLayer(this.placesLayer);
                }
                this.editMarkerLayer = L.layerGroup().addTo(this.map);

                var latitude = document.getElementById('latitude'),
                    longitude = document.getElementById('longitude'),
                    marker = L.marker([-1.27, 36.782], {
                        draggable: true
                    }).addTo(this.editMarkerLayer),
                    ondragend = function ondragend() {
                        var ll = marker.getLatLng();
                        latitude.value = ll.lat;
                        longitude.value = ll.lng;
                        reqres.setHandler('latitude', function () {
                            return latitude.value;
                        });
                        reqres.setHandler('longitude', function () {
                            return longitude.value;
                        });
                    };
                marker.on('dragend', ondragend);

                // set the initial values in the form
                ondragend();
                if (this.ui.mapOverlay.hasClass('hide')) {
                    this.ui.mapOverlay.removeClass('hide');
                }
                if (!this.ui.editLocation.hasClass('swap')) {
                    this.ui.editLocation.addClass('swap');
                }
            },
            addTourControls: function () {
                this.ui.tourList.addClass('add');
                this.ui.tourAdd.addClass('add');
            },
            cancelTour: function () {
                this.ui.tourList.removeClass('add');
                this.ui.tourAdd.removeClass('add');
            },
            addTour: function (e) {
                e.preventDefault();

                var formData = {},
                    AppTourModel;
                $('#addTour div div').children('input').each(function (i, el) {

                    if ($(el).val() !== '') {
                        formData[el.id] = $(el).val();
                    }
                    //$(el).val('');
                });
                formData.order = this.collection.nextOrder();
                AppTourModel = new UploadTourModel();
                AppTourModel.set(formData, {validate: true});

                if (AppTourModel.isValid()) {
                    this.collection.create(AppTourModel);
                    $('#fileNameLabel').removeClass('has-warning');

                    $('#addTour div div').children('input').each(function (i, el) {
                        $(el).val('');
                    });
                    this.ui.tourList.removeClass('add');
                    this.ui.tourAdd.removeClass('add');
                    //this.$fileUpload.removeAttr('disabled');
                } else {
                    $('#fileNameLabel').addClass('has-warning');
                    //this.$fileUpload.attr('disabled','disabled');
                }
            },
            /*fetch: function () {
             this.collection.fetch({reset: true});
             return this;
             },*/
            onBeforeRender: function () {
                //alert("this.places set 1 "); //+ JSON.stringify(this.placesLayer)
                this.loadLocations(this);
                this.bindToMutationEvents();
            },
            onRender: function () {
                this.initializeUpload();
            },
            ui: {
                fileInput: '#fileupload',
                tourList: '#tourList',
                tourAdd: '#tourAdd',
                btnLocate: '#btnLocate',
                btnEdit: '#btnEdit',
                mapOverlay: '.map-overlay',
                editLocation: '.editLocation',
                hideList: '.hideList',
                tourListOverlay: '.tourListOverlay',
                map: '#map',
                showList: '#btnShowList'
            },
            initializeUpload: function () {
                var url = 'http://localhost:9090/upload',//is the same as 'upload' without leading slash to show it is relative
                    that = this;
                require(['jqueryui', 'jquery.ui.widget', 'jquery.iframe-transport', 'jquery.fileupload'], function () {
                    $('#fileupload').fileupload({
                        url: url,
                        dataType: 'json',
                        done: function (e, data) {
                            $.each(data.files, function (index, file) {
                                //$('<p/>').text(file.name).appendTo('#files');
                                $('#filename').val(file.name);
                            });
                        },
                        progressall: function (e, data) {
                            var progress = parseInt(data.loaded / data.total * 100, 10);
                            $('#progress .progress-bar').css(
                                'width',
                                progress + '%'
                            );
                        }
                    }).prop('disabled', !$.support.fileInput)
                        .parent().addClass($.support.fileInput ? undefined : 'disabled');

                });

            }
        });
    return UploadTourLibraryView;
});


