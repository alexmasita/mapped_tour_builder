/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/27/13
 * Time: 7:30 PM
 * To change this template use File | Settings | File Templates.
 */

define(function (require) {
    'use strict';
    var marionette = require('marionette'),
        templates = require('templates'),
        MainViewLayout = marionette.Layout.extend({
            id: 'tourBuilder',
            template: templates.MainViewLayoutTemplate,
            regions: {
                appHeader: '#appHeader',
                appContainer: '#appContainer'
            }
        });
    return MainViewLayout;
});