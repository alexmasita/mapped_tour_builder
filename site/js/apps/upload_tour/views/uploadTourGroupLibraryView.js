define(function (require) {
    'use strict';
    var TourManager = require('app'),
        Backbone = require('backbone'),
        TourGroupLibrary = require('apps/upload_tour/collections/uploadTourGroupLibraryCollection'),
        uploadTourGroupItemView = require('apps/upload_tour/views/UploadTourGroupItemView'),
        UploadTourGroupModel = require('apps/upload_tour/models/UploadTourGroupModel'),
        marionette = require('marionette'),
        templates = require('templates'),
        $ = require('jquery'),
        UploadTourGroupLibraryView = marionette.CompositeView.extend({
            id: 'tourGroups',
            className: 'container',
            itemView: uploadTourGroupItemView,
            itemViewContainer: '#tourGroupList',
            template: templates.UploadTourGroupLibraryTemplate,
            initialize: function () {
                this.listenTo(TourManager.session, 'change:logged_in', this.reRender);
                //This view had no model so I set it here!!
                this.model = new Backbone.Model({canAddGroup: false});
                this.onLoginStatusChange();
                this.collection = new TourGroupLibrary();
                this.fetch();
            },
            onLoginStatusChange: function () {
                if (TourManager.session.get('logged_in')) {
                    this.role = TourManager.session.user.get('role');
                } else {
                    this.role = '';
                }
                var that = this;
                this.model.set({canAddGroup: function () {
                    return that.role === 'SuperUser';
                }});
            },
            reRender: function () {
                this.onLoginStatusChange();
                this.render();
            },
            itemViewOptions: function (model, index) {
                var roleName = this.role;
                return {
                    role: roleName
                };
            },
            events: {
                'click #btnGroupAdd': 'addTourGroup',
                'click #btnGroupCancel': 'cancelTourGroup',
                'click #btnAddGroupTour' : 'addTourControls'
            },
            addTourControls: function () {
                this.ui.tourGroupList.addClass('add');
                this.ui.tourGroupAdd.addClass('add');
            },
            cancelTourGroup: function () {
                this.ui.tourGroupList.removeClass('add');
                this.ui.tourGroupAdd.removeClass('add');
            },
            addTourGroup: function (e) {
                e.preventDefault();

                var formData = {},
                    AppTourModel;
                $('#addTourGroup div div').children('input').each(function (i, el) {

                    if ($(el).val() !== '') {
                        formData[el.id] = $(el).val();
                    }
                });
                AppTourModel = new UploadTourGroupModel();
                AppTourModel.set(formData, {validate: true});

                if (AppTourModel.isValid()) {
                    $('#groupNameLabel').removeClass('has-warning');
                    this.collection.create(AppTourModel);

                    $('#addTour div div').children('input').each(function (i, el) {
                        $(el).val('');
                    });
                    this.ui.tourGroupList.removeClass('add');
                    this.ui.tourGroupAdd.removeClass('add');
                } else {
                    $('#groupNameLabel').addClass('has-warning');
                }
            },
            fetch: function () {
                //alert('group library fetch called');
                this.collection.fetch({reset: true});
                return this;
            },
            ui: {
                tourGroupList: '#tourGroupList',
                tourGroupAdd: '#tourGroupAdd'
            }
        });
    return UploadTourGroupLibraryView;
});


