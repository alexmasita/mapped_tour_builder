/**
 * Created by alexmasita on 2/12/14.
 */

define(function (require) {
    'use strict';

    var Backbone = require('backbone'),
        _ = require('underscore'),
        api = "https://api.foursquare.com/v2",
        Model =  Backbone.Model,
        Collection = Backbone.Collection,
        Foursquare = new Backbone.Model({
            api: api,
            token: 0
        });

    // namespace
    Foursquare.Models = {};
    Foursquare.Collections = {};

    // **Models**: ...

    Foursquare.Models.User = Model.extend({
        url: function () {
            return Foursquare.get("api") + "/users/" + this.get("id");
        },

        defaults: { },

        parse: function (data) {
            return (data.user || data);
        }
    });

    Foursquare.Models.Me = Foursquare.Models.User.extend({
        defaults: {
            id: "self"
        }
    });

    Foursquare.Models.AddCheckin = Model.extend({
        url: function () {
            return Foursquare.get("api") + "/checkins/add";
        },
        defaults: {
            venueId: 0
        }
    });

    Foursquare.Models.Friend = Model.extend({
        defaults: {
        },

        parse: function (data) {
            return (data.user || data);
        }
    });

    Foursquare.Models.Venue = Model.extend({
        defaults: { }
    });


    // **Collections**: ...

    Foursquare.Collections.Tips = Collection.extend({
        url: function () {
            return Foursquare.get("api") + "/users/" + this.options.user + "/tips";
        },
        defaults: { },
        options: {
            user: "self"
        },

        parse: function (data) {
            console.log(data);
            return (data.tips) ? data.tips.items : data;
        }
    });

    Foursquare.Collections.Friends = Collection.extend({
        url: function () {
            return Foursquare.get("api") + "/users/" + this.options.user + "/friends";
        },
        options: {
            user: "self"
        },

        parse: function (data) {
            return (data.friends) ? data.friends.items : data;
        }
    });

    Foursquare.Collections.Venues = Collection.extend({
        url: function () {
            return Foursquare.get("api") + "/venues/search?" +
                "ll=" + this.location.coords.latitude + "," + this.location.coords.longitude +
                "&radius=50"; // hard-code radius to 50m
            //+"&oauth_token="+ app.session.get("access_token");
        },
        initialize: function (location) {
            this.location = location;
        },
        parse: function (data) {
            //console.log( data );
            return (data.venues || data);
        }
    });
    return Foursquare;

});