/**
 * Created by alexmasita on 10/27/13.
 */
define(function(require){
    var editTourMenuScenesCollection = require('apps/edit_tour/collections/editTourMenuScenesCollection'),
        $ = require('jquery'),
        reqres = require('reqres');

    var api = {
        getMenuScenesEntities:function(menuModel, folderName,filename){
            this.editTourMenuScenesCollection = new editTourMenuScenesCollection({
                folderName: folderName,
                menuId: menuModel.get('id'),
                filename:filename
            });

            var defer = $.Deferred();
            this.editTourMenuScenesCollection.fetch({
                success: function (data) {
                    defer.resolve(data);
                },error:function(data){
                    defer.resolve(undefined);
                }, reset: true});
            return defer.promise();
        }
    };

    reqres.setHandler('menu_scenes:entities',function(menuModel, folderName,filename){
        return api.getMenuScenesEntities(menuModel, folderName,filename);
    });
});