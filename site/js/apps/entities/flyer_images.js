/**
 * Created by alexmasita on 10/27/13.
 */

define(function(require){

    var editTourFlyerImagesCollection = require('apps/edit_tour/collections/editTourFlyerImagesCollection'),
        reqres = require('reqres'),
        $ = require('jquery');

    var api = {
        getFlyerImagesEntities:function(flyerModel, folderName,filename){
            var name = flyerModel.hotspotName;
            var sceneName = flyerModel.sceneName;
            var editTourFlyerImageCol = new editTourFlyerImagesCollection({folderName: folderName, parent: name,filename:filename,sceneName:sceneName});
            var defer = $.Deferred();
            editTourFlyerImageCol.fetch({success: function (data) {
                defer.resolve(data);
            },error:function(data){
                defer.resolve(undefined);
            }, reset: true});
            return defer.promise();
        }
    };

    reqres.setHandler('flyer_images:entities',function(flyerModel, folderName,filename){
        return api.getFlyerImagesEntities(flyerModel, folderName,filename);
    });
});