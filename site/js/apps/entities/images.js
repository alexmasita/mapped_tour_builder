/**
 * Created by alexmasita on 12/1/13.
 */

define(function(require){
    var editTourImageCollection = require('apps/edit_tour/collections/editTourImageCollection'),
        $ = require('jquery'),
        reqres = require('reqres');

    var api = {
        getImagesEntities:function(folderName){
            var collection = new editTourImageCollection({folderName: folderName});

            var defer = $.Deferred();
            collection.fetch({success: function (data) {
                defer.resolve(data);
            },error:function(data){
                defer.resolve(undefined);
            }, reset: true});
            return defer.promise();
        }
    };

    reqres.setHandler('image:entities',function(folderName){
        return api.getImagesEntities(folderName);
    });

    return api;
});