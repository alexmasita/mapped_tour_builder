/**
 * Created by alexmasita on 10/27/13.
 */

define(function (require) {
    'use strict';
    var EditTourAutoStopsCollection = require('apps/edit_tour/collections/EditTourAutoStopsCollection'),
        reqres = require('reqres'),
        $ = require('jquery'),
        api = {
            getAutoStopsEntities: function (autoViewModel, folderName, filename) {
                var name = autoViewModel.name,
                    editTourAutoStopsCol = new EditTourAutoStopsCollection({folderName: folderName, name: name, filename: filename}),
                    defer = $.Deferred();
                editTourAutoStopsCol.fetch({success: function (data) {
                    defer.resolve(data);
                }, error: function (data) {
                    defer.resolve(undefined);
                }, reset: true});
                return defer.promise();
            }
        };

    reqres.setHandler('auto_stops:entities', function (autoViewModel, folderName, filename) {
        return api.getAutoStopsEntities(autoViewModel, folderName, filename);
    });
});