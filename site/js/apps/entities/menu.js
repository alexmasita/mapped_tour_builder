/**
 * Created by alexmasita on 10/27/13.
 */

define(function(require){
    var editTourMenuCollection = require('apps/edit_tour/collections/editTourMenuCollection'),
        $ = require('jquery'),
        reqres = require('reqres');

    var api = {
        getMenuEntities:function(folderName,filename){
            var collection = new editTourMenuCollection({folderName: folderName, filename: filename});
            var defer = $.Deferred();
            collection.fetch({success: function (data) {
                defer.resolve(data);
            },error:function(data){
                defer.resolve(undefined);
            }, reset: true});
            return defer.promise();
        }
    };
    reqres.setHandler('menu:entities',function(folderName,filename){
        return api.getMenuEntities(folderName,filename);
    });
});