/**
 * Created by alexmasita on 10/27/13.
 */

define(function(require){
    var editTourSceneSettingsModel = require('apps/edit_tour/models/editTourSceneSettingsModel'),
        reqres = require('reqres'),
        $ = require('jquery');

    var api = {
        getSceneSettingsEntities:function(sceneName){
            var folderName = reqres.request('folderName');
            var filename = reqres.request('filename');
            this.editTourSceneSettingsModel = new editTourSceneSettingsModel({folderName: folderName, filename: filename});
            this.editTourSceneSettingsModel.set({name: sceneName});
            var defer = $.Deferred();
            this.editTourSceneSettingsModel.fetch({success: function (data) {
                defer.resolve(data);
            },error:function(data){
                defer.resolve(undefined);
            }, reset: true});
            return defer.promise();
        }
    };

    reqres.setHandler('scene_settings:entities',function(sceneName){
        return api.getSceneSettingsEntities(sceneName);
    });
});