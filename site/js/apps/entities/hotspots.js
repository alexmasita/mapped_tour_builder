/**
 * Created by alexmasita on 10/27/13.
 */

define(function(require){
    var editTourHotspotCollection = require('apps/edit_tour/collections/editTourHotspotCollection'),
        $ = require('jquery'),
        reqres = require('reqres');

    var api = {
        getHotspotEntities:function(folderName, filename){
            var collection = new editTourHotspotCollection({folderName: folderName, filename: filename});

            var defer = $.Deferred();
            collection.fetch({success: function (data) {
                defer.resolve(data);
            },error:function(data){
                defer.resolve(undefined);
            }, reset: true});
            return defer.promise();
        }
    };

    reqres.setHandler('hotspot:entities',function(folderName, filename){
        return api.getHotspotEntities(folderName,filename);
    });

    return api;
});