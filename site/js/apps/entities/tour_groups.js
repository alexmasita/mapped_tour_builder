/**
 * Created by alexmasita on 2/15/14.
 */
/**
 * Created by alexmasita on 10/27/13.
 */

define(function (require) {
    'use strict';
    var TourGroupLibrary = require('apps/upload_tour/collections/uploadTourGroupLibraryCollection'),
        $ = require('jquery'),
        reqres = require('reqres'),
        api = {
            getTourGroupsEntities: function () {
                var coll = new TourGroupLibrary(),
                    defer = $.Deferred();
                coll.fetch({success: function (data) {
                    defer.resolve(data);
                }, error: function (data) {
                    defer.resolve(undefined);
                }, reset: true});
                return defer.promise();
            }
        };
    reqres.setHandler('tour_groups:entities', function () {
        return api.getTourGroupsEntities();
    });
    return api;
});