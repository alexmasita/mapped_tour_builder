/**
 * Created by alexmasita on 10/27/13.
 */

define(function(require){
    var editTourScenesCollection = require('apps/edit_tour/collections/editTourScenesCollection'),
        reqres = require('reqres');

    var api = {
        getSceneEntities:function(folderName,filename){
            var collection = new editTourScenesCollection({folderName: folderName, filename: filename});
            var defer = $.Deferred();
            collection.fetch({success: function (data) {
                defer.resolve(data);
            },error: function (data) {
                defer.resolve(undefined);
            }, reset: true});
            return defer.promise();
        }
    };
    reqres.setHandler('scenes:entities',function(folderName,filename){
        return api.getSceneEntities(folderName,filename);
    });
    return api;
});