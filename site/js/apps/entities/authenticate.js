define(function (require) {
    'use strict';
    var Backbone = require('backbone'),
        _ = require('underscore'),
        User,
        UserLogin,
        SessionModel,
        UserCollection,
        $ = require('jquery'),
        reqres = require('reqres'),
        picky = require('picky'),
        vent = require('vent'),
        api;
    User = Backbone.Model.extend({
        idAttribute: "_id",
        urlRoot: function () {
            if (!this.isLogin) {
                return '/api/users';
            }
            return '/login';
        },
        initialize: function (options) {
            this.isLogin = options.isLogin;
        },
        defaults: {
            firstname: "",
            lastname: "",
            username: "",
            email: "",
            role: ""
        },
        validate: function (attrs, options) {
            console.log(attrs);
            var errors = {};
            if (!attrs.username) {
                errors.username = "can't be blank";
            }
            if (!attrs.password) {
                errors.password = "can't be blank";
            } else {
                if (attrs.username.length < 2) {
                    errors.username = "is too short";
                }
            }
            if (!_.isEmpty(errors)) {
                return errors;
            }
        }
    });
    SessionModel = Backbone.Model.extend({
        // Initialize with negative/empty defaults
        // These will be overriden after the initial checkAuth
        defaults: {
            logged_in: false,
            user_id: ''
        },
        initialize: function () {
            // Singleton user object
            // Access or listen on this throughout any module with app.session.user
            this.user = new User({isLogin: true});
            //alert('user obj in initialize? ' + JSON.stringify(this.user));

        },
        urlRoot: function () {
            return '/auth';
        },
        // Fxn to update user attributes after recieving API response
        updateSessionUser: function (userData) {
            this.user.set(_.pick(userData, _.keys(this.user.defaults)));
            //alert('user obj in session update? ' + JSON.stringify(this));

        },
        /*
         * Check for session from API
         */
        checkAuth: function (callback, args) {
            var self = this;
            this.fetch({
                success: function (mod, res, options) {
                    if (!res.error && res) {
                        //alert('user obj check auth? ' + JSON.stringify(res));
                        self.updateSessionUser(res);
                        self.set({ logged_in: true });
                    } else {
                        self.set({ logged_in: false });
                    }
                },
                error: function (mod, res, options) {
                    self.set({ logged_in: false });
                }
            }).always(function () {
                if (callback.hasOwnProperty('complete')) {
                    callback.complete();
                }
            });
        },
        /*
         * Abstracted fxn to make a POST request to the auth endpoint
         * This takes care of the CSRF header for security, as well as
         * updating the user and session after receiving an API response
         */
        postAuth: function (opts, callback, args) {
            var self = this,
                postData = _.omit(opts, 'method');
            $.ajax({
                url: this.urlRoot() + '/' + opts.method,
                contentType: 'application/json',
                dataType: 'json',
                type: 'POST',
                beforeSend: function (xhr) {
                    // Set the CSRF Token in the header for security
                    var token = $('meta[name="csrf-token"]').attr('content');
                    if (token) {
                        xhr.setRequestHeader('X-CSRF-Token', token);
                    }
                },
                data: JSON.stringify(_.omit(opts, 'method')),
                success: function (res, status, jqXHR) {
                    //alert('One : ' + JSON.stringify(res) + 'Two : ' + JSON.stringify(status) + 'Three : ' + JSON.stringify(jqXHR));
                    if (!res.error) {
                        if (_.indexOf(['login', 'signup'], opts.method) !== -1) {
                            //alert('user obj login auth? ' + JSON.stringify(res));
                            self.updateSessionUser(res || {});
                            self.set({ user_id: res._id, logged_in: true });
                            vent.trigger("showTourLibrary");
                        } else {
                            self.set({ logged_in: false });
                        }
                    }
                },
                error: function (mod, res) {
                    //if (callback && 'error' in callback) callback.error(res);
                }
            });
        },
        login: function (opts, callback, args) {
            this.postAuth(_.extend(opts, { method: 'login' }), callback);
        },
        logout: function (opts, callback, args) {
            this.postAuth(_.extend(opts, { method: 'logout' }), callback);
        },
        signup: function (opts, callback, args) {
            this.postAuth(_.extend(opts, { method: 'signup' }), callback);
        },
        removeAccount: function (opts, callback, args) {
            this.postAuth(_.extend(opts, { method: 'remove_account' }), callback);
        }
    });
    UserCollection = Backbone.Collection.extend({
        url: function () {
            return '/api/users';//"http://localhost:9090/" + this.folderName + "/files/tour.xml"
        },
        model: User,
        initialize: function (options) {
            var singleSelect = new picky.SingleSelect(this);
            _.extend(this, singleSelect);
        }
    });
    api = {
        getUserEntities: function () {
            var coll = new UserCollection(),
                defer = $.Deferred();
            coll.fetch({success: function (collection, response, options) {
                defer.resolve(collection, response, options);
            }, error: function (data) {
                defer.resolve(undefined);
            }, reset: true});
            return defer.promise();
        },
        getUserEntity: function (id) {
            var coll = new User({_id: id}),
                defer = $.Deferred();
            coll.fetch({success: function (model, response, options) {
                defer.resolve(model, response, options);
            }, error: function (data) {
                defer.resolve(undefined);
            }, reset: true});
            return defer.promise();
        }
    };
    reqres.setHandler('user:entities', function () {
        return api.getUserEntities();
    });
    reqres.setHandler('user:entity', function (id) {
        return api.getUserEntity(id);
    });
    reqres.setHandler('user:entity:model', function () {
        return User;
    });
    reqres.setHandler('user:entities:collection', function (id) {
        return UserCollection;
    });
    reqres.setHandler('session:entity:model', function () {
        return SessionModel;
    });
    return api;
});