/**
 * Created by alexmasita on 10/27/13.
 */

define(function (require) {
    'use strict';
    var EditTourMiniCollection = require('apps/edit_tour/collections/EditTourMiniCollection'),
        $ = require('jquery'),
        reqres = require('reqres'),
        api = {
            getMiniTourEntities: function (folderName) {
                var coll = new EditTourMiniCollection({folderName: folderName}),
                    defer = $.Deferred();
                coll.fetch({success: function (data) {
                    defer.resolve(data);
                }, error: function (data) {
                    defer.resolve(undefined);
                }, reset: true});
                return defer.promise();
            }
        };

    reqres.setHandler('mini_tours:entities', function (folderName) {
        return api.getMiniTourEntities(folderName);
    });
    return api;
});