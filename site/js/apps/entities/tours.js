

define(function (require) {
    'use strict';
    var TourLibrary = require('apps/upload_tour/collections/uploadTourLibraryCollection'),
        $ = require('jquery'),
        reqres = require('reqres'),
        api = {
            getToursEntities: function (_id) {
                var coll = new TourLibrary(),
                    defer = $.Deferred();
                coll.fetch({success: function (data) {
                    defer.resolve(data);
                }, error: function (data) {
                    defer.resolve(undefined);
                }, reset: true, data: {groupId: _id}});
                return defer.promise();
            }
        };
    reqres.setHandler('tours:entities', function (_id) {
        return api.getToursEntities(_id);
    });
    return api;
});