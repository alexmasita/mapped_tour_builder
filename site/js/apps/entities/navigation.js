/**
 * Created by alexmasita on 11/21/13.
 */
/**
 * Created by alexmasita on 10/27/13.
 */

define(function (require) {
    'use strict';
    var EditTourNavigationCollection = require('apps/edit_tour/collections/EditTourNavigationCollection'),
        $ = require('jquery'),
        reqres = require('reqres'),
        api = {
            getNavigationEntities: function (folderName, filename) {
                var collection = new EditTourNavigationCollection({folderName: folderName, filename: filename}),
                    defer = $.Deferred();
                collection.fetch({success: function (data) {
                    defer.resolve(data);
                }, error: function (data) {
                    defer.resolve(undefined);
                }, reset: true});
                return defer.promise();
            }
        };

    reqres.setHandler('navigation:entities', function (folderName, filename) {
        return api.getNavigationEntities(folderName, filename);
    });

    return api;
});