/**
 * Created by alexmasita on 10/27/13.
 */

define(function(require){
    var editTourAutoViewCollection = require('apps/edit_tour/collections/editTourAutoViewCollection'),
        $ = require('jquery'),
        reqres = require('reqres');


    var api = {
        getAutoScenesEntities:function(folderName, filename){
            var collection = new editTourAutoViewCollection({folderName: folderName, filename: filename});

            var defer = $.Deferred();
            collection.fetch({success: function (data) {
                defer.resolve(data);
            },error:function(data){
                defer.resolve(undefined);
            }, reset: true});
            return defer.promise();
        }
    };

    reqres.setHandler('auto_view:entities',function(folderName, filename){
        return api.getAutoScenesEntities(folderName,filename);
    });

    return api;
});