/**
 * Created by alexmasita on 10/6/13.
 */

define(function(require){
    'use strict';

    var router = require('apps/main/router/router'),
        reqres = require('reqres');

    var api = {
        showEditor:function(folderName){
            var mainController = reqres.request('mainController');
            mainController.showTourEditor(folderName);
        }
    };

       return new router({controller:api});

});