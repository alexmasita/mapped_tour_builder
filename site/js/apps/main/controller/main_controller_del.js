define(function(require){
    'use strict';
    var vent = require('vent'),
        marionette = require('marionette'),
        commands = require('commands'),
        reqres = require('reqres');
    var controller = marionette.Controller.extend({
        initialize:function(options){
            this.header = options.header;
        },
        show:function(){}
    });
/*
    var controller =  marionette.Controller.extend({
        initialize:function(options){
            var app = reqres.request('app');
            this.mainRegion = app.application;
            this.models = {};

            var editTourController = require('apps/edit_tour/controller/edit_tour_controller');

            var editTourEditorView = require('apps/edit_tour/views/editTourEditorView');
            this.editorView = new editTourEditorView();
            this.etControl = new editTourController(this.editorView);

            var that = this;
            reqres.setHandler('editTourController',function(){
                return that.etControl;
            });
            require('apps/edit_tour/edit_tour_app');

            //this.MainViewLayout = new mainViewLayout();

        },
        show:function(){

            var layout = this._getMainViewLayout();
            this.mainRegion.show(layout);

        },
        _getMainViewLayout:function(){
            require('bootstrap');
            require('affix');

            var mainViewLayout = require('apps/main/views/main_view_layout');
            this.mainViewlayout = new mainViewLayout();

            var that = this;
            commands.setHandler("editTour",function(folderName){
                that.showTourEditor(folderName);
            });

            this.listenTo(this.mainViewlayout, 'render',function(){
                this._showMenuAndContent(this.mainViewlayout);
            });

            return this.mainViewlayout;
        },
        showTourEditor:function(folderName){

            reqres.setHandler('folderName',function(){
                return folderName;
            });
            this._tourEditorContent(this.mainViewlayout.appContainer,folderName);
        },
        _showMenuAndContent:function(layout){
            var menu = this._addMenu(layout.appHeader);
            this._tourLibraryContent(layout.appContainer);

            var that = this;
            menu.on("renderTourLibrary",function(){
                that._tourLibraryContent(layout.appContainer);
                commands.execute('navigate','');
            });
        },
        _addMenu:function(region){
            var MainViewMenu = require('apps/main/views/main_view_menu');
            var menu = new MainViewMenu();
            region.show(menu);
            return menu;
        },
        _tourLibraryContent:function(region){
            var tourLibraryView = require('apps/upload_tour/views/uploadTourLibraryView');
            var tourLibrary = new tourLibraryView();

            region.show(tourLibrary);

        },
        _tourEditorContent:function(region,folderName){

            //var editTourEditorView = require('apps/edit_tour/views/editTourEditorView');
            //var editTourController = require('apps/edit_tour/controller/edit_tour_controller');
            //var editorView = new editTourEditorView();
            alert('tour editor content called ' + folderName);

            region.show(this.editorView);

            //var etControl = new editTourController(editorView);

            this.etControl.show(folderName);
            //etControl.showSelection(folderName);

            commands.execute('navigate','edit/' + folderName);
        }

    });
*/
    return controller;
});