define(function (require) {
    'use strict';
    var View = require('apps/main/views/main_view_menu'),
        TourManager = require('app');
    TourManager.module('MainMenuApp.Show', function (Show, TourManager, Backbone, Marionette, $, _) {
        Show.Controller = {
            showMainMenu: function () {
                var MainViewMenu = new View();
                TourManager.header.show(MainViewMenu);
            }
        };
    });
    return TourManager.MainMenuApp.Show.Controller;
});