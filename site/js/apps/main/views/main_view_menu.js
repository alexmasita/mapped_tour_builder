/**
 * Created by alexmasita on 10/2/13.
 */

define(function (require) {
    'use strict';
    var Backbone = require('backbone'),
        TourManager = require('app'),
        marionette = require('marionette'),
        templates = require('templates'),
        $ = require('jquery'),
        vent = require('vent'),
        reqres = require('reqres'),
        commands = require('commands');

    return marionette.ItemView.extend({
        template: templates.MainViewMenuTemplate,
        initialize: function () {
            this.listenTo(TourManager.session, 'change:logged_in', this.onLoginStatusChange);
            this.model = new Backbone.Model();
        },
        onLoginStatusChange: function () {
            this.model.set({logged_in: TourManager.session.get('logged_in'), firstname: TourManager.session.user.get('firstname'), lastname: TourManager.session.user.get('lastname')});
        },
        modelEvents: {
            "change": "render"
        },
        events: {
            'click #lnkLogOut': 'logOut',
            'click #btnUpload': 'renderTourLibrary'
        },
        ui: {
            loginButton: '#spanDisplayName',
            signInDisplay: '#displaySignIn'
        },
        renderTourLibrary: function (e) {
            e.preventDefault();
            vent.trigger("showTourLibrary");
        },
        /*logOut: function (event) {
         event.preventDefault(); // Don't let this button submit the form
         var that = this,
         url = '/logout';
         console.log('Log Out...');
         $.ajax({
         url: url,
         dataType: 'json',
         success: function (data) {
         if (data.error) {  // If there is an error, show the error messages
         $('.alert-error').text(data.error.text).show();
         } else { // If not, send them back to the home page
         //that.ui.signInDisplay.removeClass('show');
         $('#spanDisplayName').text('Login');
         vent.trigger("showTourLibrary");
         }
         }
         });
         },*/
        logOut: function (e) {
            if (e) {
                e.preventDefault();
            }
            var that = this;
            TourManager.session.logout({});
        },
        setLogin: function (displayName) {
            //this.ui.signInDisplay.addClass('show');
            //alert('display name is ' + displayName);
            $('#spanDisplayName').text(displayName);
            //this.ui.loginButton.text(displayName);

        }
    });
});