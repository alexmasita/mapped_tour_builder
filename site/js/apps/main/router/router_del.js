/**
 * Created by alexmasita on 10/6/13.
 */

define(function(require){
    var marionette = require('marionette');
    var router = marionette.AppRouter.extend({
        initialize:function(){
            //alert('main router initialized');
        },
        appRoutes: {
            "edit/:folderName(/:filename)": "showEditor"
        }
    });
    return router;
});