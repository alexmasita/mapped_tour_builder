/**
 * Created by alexmasita on 3/19/14.
 */
define(function (require) {
    'use strict';
    var TourManager = require('app'),
        MainViewMenuController = require('apps/main/controller/main_view_menu_controller');
    TourManager.module('MainViewMenuApp', function (MainViewMenuApp, TourManager, Backbone, Marionette, $, _) {
        var api = {
            showMenu: function () {
                MainViewMenuController.showMainMenu();
            }
        };

        MainViewMenuApp.on('start', function () {
            api.showMenu();
        });
    });
    return TourManager.MainViewMenuApp;
});