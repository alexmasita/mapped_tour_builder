/**
 * Created by alexmasita on 11/7/13.
 */

define(function(require){
    var picky = require('picky'),
        Backbone = require('backbone'),
        _ = require('underscore');
    var editTourHospotModel = Backbone.Model.extend({
        idAttribute:'id',
        initialize:function(){
            var selectable = new picky.Selectable(this);
            _.extend(this,selectable);
        }
    });
    return editTourHospotModel
});