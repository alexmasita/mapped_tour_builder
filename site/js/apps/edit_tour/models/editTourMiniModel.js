/**
 * Created by alexmasita on 10/21/13.
 */

define(function(require){
    var Backbone = require('backbone'),
        picky = require('picky'),
        _ = require('underscore');
    var editTourMiniModel = Backbone.Model.extend({
        idAttribute:'id',
        defaults:{
            text:'No Selection Name',
            filename:'No File Name'
        },
        initialize:function(){
            var selectable = new picky.Selectable(this);
            _.extend(this, selectable);
        }
    });
    return editTourMiniModel;
});