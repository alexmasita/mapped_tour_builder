/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/11/13
 * Time: 10:26 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};
define(function (require) {
    'use strict';
    var Backbone = require('backbone'),
        picky = require('picky'),
        _ = require('underscore'),
        editTourMenuItemModel = Backbone.Model.extend({
            idAttribute: 'id',
            defaults: {
                text: 'Enter Title',
                type: '',
                load: ''
            },
            initialize: function () {
                var selectable = new picky.Selectable(this);
                _.extend(this, selectable);
                //Initialize shared object and functions
            }
        });
    return editTourMenuItemModel;
});
