/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/17/13
 * Time: 8:29 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};

define(function(require){
    var Backbone = require('backbone');
    var editTourBulkScenesModel = Backbone.Model.extend({
        idAttribute:'filename',
        urlRoot: function () {
            return '/bulkscenes/' + this.folderName;
        },
        initialize:function(options){
            this.folderName = options.folderName;
        }
    });
    return editTourBulkScenesModel;
});


