/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/15/13
 * Time: 1:42 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};

define(function(require){
    var Backbone = require('backbone');
    var editTourMenuSceneModel = Backbone.Model.extend({
        initialize:function(){

        },
        idAttribute:'sceneName',
        default:{
            load:''
        }
    });
    return editTourMenuSceneModel;
});


