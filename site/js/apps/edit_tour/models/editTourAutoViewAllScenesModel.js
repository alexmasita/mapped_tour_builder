/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/17/13
 * Time: 8:29 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};

define(function (require) {
    var Backbone = require('backbone');
    var editTourAutoViewAllScenesModel = Backbone.Model.extend({
        url: function () {
            return '/api/autoview/' + this.folderName + '/' + this.filename;
        },
        toJSON: function () {
            return this.scenesCollection.toJSON(); // where model is the collection class YOU defined above
        }/*,
         defaults: {
         scenesCollection: new Backbone.Collection([])
         }*/,
        initialize: function (options) {
            this.folderName = options.folderName;
            this.filename = options.filename;
        },
        populateCollection: function (modelsCollection) {

            this.scenesCollection = new Backbone.Collection(modelsCollection.models);
            //alert(JSON.stringify(this.scenesCollection));
        }
    });
    return editTourAutoViewAllScenesModel;
});


