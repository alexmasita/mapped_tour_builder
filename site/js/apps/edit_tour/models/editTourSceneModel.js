/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/1/13
 * Time: 8:49 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};

define(function (require) {
    var Backbone = require('backbone'),
        picky = require('picky'),
        _ = require('underscore');
    var editTourSceneModel = Backbone.Model.extend({
        idAttribute: 'name',
        defaults: {
            title: 'Set Title',
            description: 'Set Description'
        },
        initialize: function () {
            var selectable = new picky.Selectable(this);
            _.extend(this, selectable);
            console.log('edit tour model has been initialized');
        }

    });
    return editTourSceneModel;
});
