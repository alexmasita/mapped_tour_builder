/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/17/13
 * Time: 5:51 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};

define(function (require) {
    'use strict';
    var Backbone = require('backbone'),
        picky = require('picky'),
        _ = require('underscore'),
        editTourAutoViewModel = Backbone.Model.extend({
            idAttribute: 'name', //go to server as id (retrieved in node server as id from :id)
            initialize: function () {
                var selectable = new picky.Selectable(this);
                _.extend(this, selectable);
            }
        });
    return editTourAutoViewModel;
});

