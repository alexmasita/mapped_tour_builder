/**
 * Created by alexmasita on 12/1/13.
 */

define(function(require){
    var Backbone = require('backbone'),
        _ = require('underscore'),
        picky = require('picky');
    var editTourImageModel = Backbone.Model.extend({
        idAttribute:'id',
        initialize:function(){
            var selectable = new picky.Selectable(this);
            _.extend(this,selectable);
        }
    });
    return editTourImageModel;
});