define(function (require) {
    var Backbone = require('backbone'),
        _ = require('underscore'),
        editTourApplyImagesModel = Backbone.Model.extend({
            url: function () {
                return '/api/applyimages/' + this.folderName + '/' + this.filename;
            },
            toJSON: function () {
                var cols = {flyerCollection: this.flyerCollection, imagesCollection: this.imagesCollection, isReplace: this.isReplace, isFilm: this.isFilm};
                return cols; // where model is the collection class YOU defined above
            },
            initialize: function (options) {
                this.folderName = options.folderName;
                this.filename = options.filename;
            },
            populateCollection: function (flyerCollection, imagesCollection, isReplace, isFilm) {
                this.flyerCollection = flyerCollection;
                this.imagesCollection = imagesCollection;
                this.isReplace = isReplace;
                this.isFilm = isFilm;
                //alert(JSON.stringify(this.scenesCollection));
                //alert('flyer is Object '+ JSON.stringify(this.flyerCollection)+' images is Object '+ JSON.stringify(this.imagesCollection));
                return (_.keys(this.flyerCollection).length > 0) && (_.keys(this.imagesCollection).length > 0);
            }
        });
    return editTourApplyImagesModel;
});


