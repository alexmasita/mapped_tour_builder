/**
 * Created by alexmasita on 11/21/13.
 */
define(function(require){
    var Backbone = require('backbone'),
        picky = require('picky'),
        _ = require('underscore');
    var editTourNavigationModel = Backbone.Model.extend({
        idAttribute:'id',
        initialize:function(){
            var selectable = new picky.Selectable(this);
            _.extend(this,selectable);
        }

    });
    return editTourNavigationModel;
});