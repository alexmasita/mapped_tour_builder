/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/20/13
 * Time: 6:21 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app||{};

define(function(require){
    var Backbone = require('backbone');
    var editTourSceneSettingsModel = Backbone.Model.extend({
        idAttribute:'name',
        urlRoot: function () {
            return '/api/sceneSettings/' + this.folderName + '/' + this.filename;
        },
        initialize:function(options){
            this.folderName = options.folderName;
            this.filename = options.filename;
            //alert('Scene Settings Initialized with ' + this.folderName);
        }
    });
    return editTourSceneSettingsModel;
});
