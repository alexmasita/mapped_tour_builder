/**
 * Created by alexmasita on 11/14/13.
 */
define(function(require){
    var Backbone = require('backbone');
    var editTourHotspotsModel = Backbone.Model.extend({
        url:function(){
            return '/api/hotspot/' + this.folderName + '/' + this.filename;
        },
        initialize:function(options){
            this.folderName = options.folderName;
            this.filename = options.filename;
        },
        toJSON: function () {
            return this.hotspotObject.toJSON(); // where model is the collection class YOU defined above
        },
        populateHotspotObject:function(sceneName,collection){
            this.hotspotObject ={
                sceneName:sceneName,
                collection:collection,
                toJSON:function(){
                    var that = this;
                    return {
                        sceneName:that.sceneName,
                        collection:that.collection};
                }
            };
        }
    });
    return editTourHotspotsModel;
});