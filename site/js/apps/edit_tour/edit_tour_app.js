/**
 * Created by alexmasita on 10/6/13.
 * this module is the entry point for all commands and events
 * The public api here calls the controller to manipulate related views
 * And it is where the state handlers on the controller are called
 */

define(function (require) {
    'use strict';

    var Router = require('apps/edit_tour/Router/Router'),
        reqres = require('reqres'),
        vent = require('vent'),
        commands = require('commands'),
        $ = require('jquery'),
        editTourControl,
        api = {
            show: function (folderName) {
                reqres.setHandler('filename', function () {
                    return '';
                });
                reqres.setHandler('folderName', function () {
                    return folderName;
                });

                var app = reqres.request('app'),
                    EditTourController = require('apps/edit_tour/controller/edit_tour_controller'),
                    editControl;
                editTourControl = new EditTourController({mainRegion: app.application});

                editControl = editTourControl.show(folderName); // returns a promise
                return editControl;
                //****** Manage selection state here ******miniSelectionCollection
            },
            showSelection: function (folderName, filename) {
                var promise = this.show(folderName);
                reqres.setHandler('filename', function () {
                    return filename;
                });

                $.when(promise).done(function () {
                    commands.execute('set:active:item', reqres.request('miniSelectionCollection').find(function (selection) {
                        return selection.get('filename') === filename;
                    }).get('id'));
                });

                $.when(promise).done(function () {
                    editTourControl.showSelection(folderName, filename);
                });

                //****** Manage selection state here ******
                //Selections states for menus will be shifted here from their module views once they have their routes defined
            },
            showSceneList: function (folderName, filename, region) {
                editTourControl.showTransferSceneList(folderName, filename, region);
            }
        };

    //******* under normal circumstances these code snippets would appear in their own module
    commands.setHandler('itemview:set:active:scene', function (name) {
        if (editTourControl !== undefined) {
            editTourControl.setActiveScene(name);
        } else {
            alert('editTourControl is undefined (edit_tour_app line 53)');
        }
    });
    commands.setHandler('itemview:set:active:transfer:scene', function (name) {
        if (editTourControl !== undefined) {
            editTourControl.setActiveTransferScene(name);
        } else {
            alert('editTourControl is undefined (edit_tour_app line 53)');
        }
    });
    commands.setHandler('set:active:item', function (Id) {
        if (editTourControl !== undefined) {

            editTourControl.setActiveItem(Id);
        } else {
            alert('editTourControl is undefined (edit_tour_app line 60)');
        }
    });
    commands.setHandler('set:transfer:active:item', function (Id) {
        if (editTourControl !== undefined) {
            editTourControl.setActiveTransferItem(Id);
        } else {
            alert('editTourControl is undefined (edit_tour_app line 60)');
        }
    });
    commands.setHandler('set:active:menu', function (id) {
        if (editTourControl !== undefined) {
            editTourControl.setActiveMenu(id);
        } else {
            alert('editTourControl is undefined (edit_tour_app line 67)');
        }
    });
    commands.setHandler('set:active:flyer', function (id) {
        if (editTourControl !== undefined) {
            editTourControl.setActiveFlyer(id);
        } else {
            alert('editTourControl is undefined (edit_tour_app line 98)');
        }
    });
    commands.setHandler('set:active:player:flyer', function (hsName) {
        if (editTourControl !== undefined) {
            editTourControl.setActivePlayerFlyer(hsName);
        } else {
            alert('editTourControl is undefined (edit_tour_app line 98)');
        }
    });
    commands.setHandler('set:active:image', function (id) {
        if (editTourControl !== undefined) {
            editTourControl.setActiveImage(id);
        } else {
            alert('editTourControl is undefined (edit_tour_app line 98)');
        }
    });
    commands.setHandler('itemview:set:active:auto:view', function (name) {
        if (editTourControl !== undefined) {
            //alert('name entered** ' + name);
            editTourControl.setActiveAutoViewScene(name);
        } else {
            alert('editTourControl is undefined (edit_tour_app line 77)');
        }
    });
    //******* under normal circumstances these code snippets would appear in their own module

    vent.on('edit_tour_app:show', function (folderName) {
        api.show(folderName);
        commands.execute('navigate', 'edit/' + folderName);
    });

    vent.on('itemview:show:editor', function (folderName, filename) {
        api.showSelection(folderName, filename);
        commands.execute('navigate', 'edit/' + folderName + '/' + filename);
    });
    vent.on('show:transfer:scene:list', function (filename, region) {
        api.showSceneList(reqres.request('folderName'), filename, region);
    });

    return new Router({controller: api});
});