/**
 * Created by alexmasita on 11/7/13.
 */
define(function (require) {
    var editTourHotspotModel = require('apps/edit_tour/models/editTourHotspotModel'),
        picky = require('picky'),
        Backbone = require('backbone'),
        _ = require('underscore');
    var editTourFlyerPlayerCollection = Backbone.Collection.extend({
        model: editTourHotspotModel,
        initialize: function (options) {
            var singleSelect = new picky.SingleSelect(this);
            _.extend(this, singleSelect);
        }
    });
    return editTourFlyerPlayerCollection;
});