/**
 * Created by alexmasita on 11/7/13.
 */
define(function(require){
    var editTourHotspotModel = require('apps/edit_tour/models/editTourHotspotModel'),
        picky = require('picky'),
        Backbone = require('backbone'),
        _ = require('underscore');
    var editTourHotspotCollection = Backbone.Collection.extend({
        model:editTourHotspotModel,
        url:function(){
            return '/api/hotspot/' + this.folderName + '/' + this.filename;
        },
        initialize:function(options){
            var multiSelect = new picky.MultiSelect(this);
            _.extend(this,multiSelect);
            this.folderName = options.folderName;
            this.filename = options.filename;
        }
    });
    return editTourHotspotCollection;
});