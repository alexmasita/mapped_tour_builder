/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/18/13
 * Time: 4:26 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};

define(function(require){
    var Backbone = require('backbone'),
        editTourAutoStopsModel=require('apps/edit_tour/models/editTourAutoStopsModel');
    var editTourFlyerImagesCollection = Backbone.Collection.extend({
        model:editTourAutoStopsModel,
        url:function(){
            return '/api/applyimages/' + this.folderName + '/' + this.filename + '/' + this.parent + '/' + this.sceneName;
        },
        initialize:function(options){
            this.folderName = options.folderName;
            this.filename = options.filename;
            this.parent = options.parent;
            this.sceneName = options.sceneName;
        }
    });
    return editTourFlyerImagesCollection;
});
