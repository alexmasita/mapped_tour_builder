/**
 * Created by alexmasita on 12/1/13.
 */

define(function (require) {
    var Backbone = require('backbone'),
        _ = require('underscore'),
        picky = require('picky'),
        editTourImageModel = require('apps/edit_tour/models/editTourImageModel');
    var editTourImageCollection = Backbone.Collection.extend({
        model: editTourImageModel,
        url: function () {
            return '/api/images/' + this.folderName;
        },
        initialize: function (options) {
            var multiSelect = new picky.MultiSelect(this);
            _.extend(this, multiSelect);
            this.folderName = options.folderName;
        }
    });
    return editTourImageCollection;
});