/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/15/13
 * Time: 1:41 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};
define(function (require) {
    var Backbone = require('backbone'),
        editTourMenuSceneModel = require('apps/edit_tour/models/editTourMenuSceneModel');
    var editTourMenuScenesCollection = Backbone.Collection.extend({
        model: editTourMenuSceneModel,
        url: function () {
            //alert('Fetch called with Foldername'+this.folderName + ' and Menu Id ' + this.menuId);
            return '/api/menuScenes/' + this.folderName + '/' + this.filename + '/' + this.menuId;
        },
        initialize: function (options) {
            this.menuId = options.menuId;
            this.folderName = options.folderName;
            this.filename = options.filename;
            //alert("this function entered : " + this.load + ' ' + this.folderName);

        }
    });
    return editTourMenuScenesCollection;
});

