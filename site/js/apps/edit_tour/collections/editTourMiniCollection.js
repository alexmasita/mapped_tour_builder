/**
 * Created by alexmasita on 10/21/13.
 */

define(function(require){
    var Backbone = require('backbone'),
        editTourMiniModel = require('apps/edit_tour/models/editTourMiniModel'),
        picky = require('picky'),
        _ = require('underscore');

    var editTourMiniCollection = Backbone.Collection.extend({
        model:editTourMiniModel,
        url:function(){
            return '/api/mini/' + this.folderName;
        },
        initialize:function(options){
            var singleSelect = new picky.SingleSelect(this);
            _.extend(this, singleSelect);

            this.folderName = options.folderName;
        }
    });
    return editTourMiniCollection;
});