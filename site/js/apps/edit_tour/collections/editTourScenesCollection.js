/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/1/13
 * Time: 4:35 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};
define(function(require){
    var Backbone = require('backbone'),
        editTourSceneModel = require('apps/edit_tour/models/editTourSceneModel'),
        picky = require('picky'),
        _ = require('underscore');
    var editTourScenesCollection = Backbone.Collection.extend({
        model: editTourSceneModel,
        url: function () {
            //alert("called edit tour collections"+this.folderName);
            return '/edit/' + this.folderName + '/' + this.filename;//"http://localhost:9090/" + this.folderName + "/files/tour.xml"
        },
        initialize: function (options) {
            var singleSelect = new picky.SingleSelect(this);
            _.extend(this,singleSelect);

            this.folderName = options.folderName;
            this.filename = options.filename;
            //alert("called edit Tour scene collections"+options.folderName);
        }/*,
         parse: function (response) {

         this.root = response;

         var sceneCol = [];
         var folderName = this.folderName;
         $(this.root).find("scene").each(function () {
         //alert($(this).attr("name"));
         var sceneObj = {
         name: $(this).attr("name"),
         title: $(this).attr("title"),
         description: $(this).attr("descripcion"),
         folderName:folderName
         };

         sceneCol.push(sceneObj);
         });
         //alert(JSON.stringify(sceneCol));
         return sceneCol;
         //this.collection = new app.editTourScenesCollection(sceneCol);
         }*/
        /*parse:function(response){
         //alert(JSON.stringify(response));
         var sceneCol = [];
         _.each(response,function(el,index,list){
         var sceneObj = {
         name:el.name,
         title:el.title,//.replace("[h1]","").replace("[/h1]",""),
         description:el.description,//.replace("[p]","").replace("[/p]",""),
         folderName:el.folderName
         };
         sceneCol.push(sceneObj);
         });
         return sceneCol;
         }*/
    });
    return editTourScenesCollection;
});

