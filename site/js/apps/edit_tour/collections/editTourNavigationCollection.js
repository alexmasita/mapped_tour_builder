/**
 * Created by alexmasita on 11/21/13.
 */

define(function(require){
    var Backbone = require('backbone'),
        editTourNavigationModel = require('apps/edit_tour/models/editTourNavigationModel'),
        picky = require('picky'),
        _ = require('underscore');
    var editTourNavigationCollection = Backbone.Collection.extend({
        model:editTourNavigationModel,
        url:function(){
            return '/api/navigation/' + this.folderName + '/' + this.filename;
        },
        initialize:function(options){
            var singleSelect = new picky.SingleSelect(this);
            _.extend(this,singleSelect);
            this.folderName = options.folderName;
            this.filename = options.filename;
        }
    });
    return editTourNavigationCollection;
});