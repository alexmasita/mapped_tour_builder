/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/18/13
 * Time: 4:26 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};

define(function (require) {
    'use strict';
    var Backbone = require('backbone'),
        editTourAutoStopsModel = require('apps/edit_tour/models/editTourAutoStopsModel');
    var editTourAutoStopsCollection = Backbone.Collection.extend({
        model: editTourAutoStopsModel,
        url: function () {
            return '/api/stops/' + this.folderName + '/' + this.filename + '/' + this.name;
        },
        initialize: function (options) {
            this.folderName = options.folderName;
            this.filename = options.filename;
            this.name = options.name;
        }
    });
    return editTourAutoStopsCollection;
});
