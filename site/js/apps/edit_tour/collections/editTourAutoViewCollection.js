/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/17/13
 * Time: 5:54 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};

define(function (require) {
    'use strict';
    var Backbone = require('backbone'),
        editTourAutoViewModel = require('apps/edit_tour/models/editTourAutoViewModel'),
        picky = require('picky'),
        _ = require('underscore'),
        editTourAutoViewCollection = Backbone.Collection.extend({
            model: editTourAutoViewModel,
            url: function () {
                return '/api/autoview/' + this.folderName + '/' + this.filename;
            },
            initialize: function (options) {
                var singleSelect = new picky.SingleSelect(this);
                _.extend(this, singleSelect);
                this.folderName = options.folderName;
                this.filename = options.filename;
            }
        });
    return editTourAutoViewCollection;
});

