/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/11/13
 * Time: 11:04 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};
define(function(require){
        var editTourMenuItemModel = require('apps/edit_tour/models/editTourMenuItemModel'),
            Backbone = require('backbone'),
            picky = require('picky'),
            _ = require('underscore');
        var editTourMenuCollection = Backbone.Collection.extend({
            model:editTourMenuItemModel,
            url: function () {
                return '/api/menus/' + this.folderName + '/' + this.filename;//"http://localhost:9090/" + this.folderName + "/files/tour.xml"
            },
            nextOrder:function(){
                return +this.length;
            },
            initialize: function (options) {
                var singleSelect = new picky.SingleSelect(this);
                _.extend(this,singleSelect);
                this.folderName = options.folderName;
                this.filename = options.filename;
            }

        });
        return editTourMenuCollection;
    });