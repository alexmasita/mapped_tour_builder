/**
 * Created by alexmasita on 10/8/13.
 * The controller co-ordinates all views and channels all events that need to be communicated to the
 * outside world.
 *
 * Methods for setting Menus Active state are set in the controller managing the lists
 */

define(function (require) {
    'use strict';
    var marionette = require('marionette'),
        vent = require('vent'),
        Backbone = require('backbone'),
        $ = require('jquery'),
        mediator = require('Mediators/mediator'),
        reqres = require('reqres'),
        commands = require('commands'),
        _ = require('underscore'),
        syncPlayer = function (sceneName, hotspotName, FlyerCompositeView) {
            var tourPlayer = reqres.request('tourPlayer'),
                hotspotCollection,
                hotspotCount,
                getFileNameFromPath,
                i,
                len,
                listCol,
                pluckedListColArray,
                pluckedHotspotsArray,
                missingHotspotsNameArray,
                deletedHotspotsNameArray;
            if (tourPlayer.get('xml.scene') !== sceneName) {
                commands.execute('itemview:call:action', "loadscene('" + sceneName + "',null,MERGE,BLEND(1));");

                //var tourPlayer = reqres.request('tourPlayer');
                hotspotCollection = [];
                hotspotCount = tourPlayer.get('hotspot.count');
                getFileNameFromPath = function (path) {
                    var ary = path.split("/");
                    return ary[ary.length - 1];
                };

                //Note : This is only the hotspot count for this scene
                for (i = 0, len = hotspotCount; i < len; i++) {
                    //alert(tourPlayer.get('hotspot['+ i + '].url'));

                    if (tourPlayer.get('hotspot[' + i + '].keep') === false && !tourPlayer.get('hotspot[' + i + '].parent')) {
                        hotspotCollection.push({
                            sceneName: tourPlayer.get('xml.scene'),
                            hotspotName: tourPlayer.get('hotspot[' + i + '].name'),
                            url: getFileNameFromPath(tourPlayer.get('hotspot[' + i + '].url')),
                            ath: tourPlayer.get('hotspot[' + i + '].ath'),
                            atv: tourPlayer.get('hotspot[' + i + '].atv'),
                            distorted: tourPlayer.get('hotspot[' + i + '].distorted'),
                            rx: tourPlayer.get('hotspot[' + i + '].rx'),
                            ry: tourPlayer.get('hotspot[' + i + '].ry'),
                            rz: tourPlayer.get('hotspot[' + i + '].rz'),
                            width: tourPlayer.get('hotspot[' + i + '].width'),
                            height: tourPlayer.get('hotspot[' + i + '].height'),
                            scale: tourPlayer.get('hotspot[' + i + '].scale'),
                            keep: tourPlayer.get('hotspot[' + i + '].keep'),
                            id: tourPlayer.get('hotspot[' + i + '].id')
                        });//Delete all hotspots in scene before posting this
                    }
                }

                listCol = new Backbone.Collection(FlyerCompositeView.collection.where({sceneName: sceneName}));
                //var listCol = FlyerCompositeView.collection.where({sceneName:sceneName});
                pluckedListColArray = listCol.pluck('hotspotName');//_.pluck(listCol,'hotspotName'); //listCol.pluck('hotspotName');
                //alert(JSON.stringify(pluckedListColArray));
                pluckedHotspotsArray = _.pluck(hotspotCollection, 'hotspotName'); //hotspotCollection.pluck('hotspotName');
                //alert(JSON.stringify(pluckedHotspotsArray));

                //alert('list col array = ' + JSON.stringify(pluckedListColArray) + ' Hotspot Array ' + JSON.stringify(pluckedHotspotsArray));
                missingHotspotsNameArray = _.difference(pluckedListColArray, pluckedHotspotsArray); //pluckedListColArray.without(pluckedHotspotsArray);
                //alert(missingHotspotsNameArray.length);

                _.each(missingHotspotsNameArray, function (hotspotName) {
                    var folderName = reqres.request('folderName'),
                        hotspotModel = listCol.findWhere({hotspotName: hotspotName}),
                        sceneName = hotspotModel.get('sceneName'),
                    /*hotspotName = hotspotModel.get('hotspotName'),*/
                        url = folderName + '/files/graphics/' + getFileNameFromPath(hotspotModel.get('url')),
                        ath = hotspotModel.get('ath'),
                        atv = hotspotModel.get('atv'),
                        distorted = hotspotModel.get('distorted'),
                        rx = hotspotModel.get('rx'),
                        ry = hotspotModel.get('ry'),
                        rz = hotspotModel.get('rz'),
                        width = hotspotModel.get('width'),
                        height = hotspotModel.get('height'),
                        scale = hotspotModel.get('scale'),
                        keep = hotspotModel.get('keep'),
                        id = hotspotModel.get('id');
                    commands.execute('itemview:call:action', 'addhotspot(' + hotspotName + ');set(hotspot[' + hotspotName +
                        '].sceneName,' + sceneName + ');set(hotspot[' + hotspotName + '].hotspotName,' + hotspotName + ');set(hotspot[' + hotspotName +
                        '].url,' + url + ');set(hotspot[' + hotspotName + '].ath,' + ath + ');set(hotspot[' + hotspotName +
                        '].atv,' + atv + ');set(hotspot[' + hotspotName + '].distorted,' + distorted + ');set(hotspot[' + hotspotName +
                        '].rx,' + rx + ');set(hotspot[' + hotspotName + '].ry,' + ry + ');set(hotspot[' + hotspotName +
                        '].rz,' + rz + ');set(hotspot[' + hotspotName + '].width,' + width + ');set(hotspot[' + hotspotName +
                        '].height,' + height + ');set(hotspot[' + hotspotName + '].scale,' + scale + ');set(hotspot[' + hotspotName +
                        '].keep,' + keep + ');set(hotspot[' + hotspotName + '].id,' + id + ');');
                    //This call refreshes the url
                    //commands.execute('itemview:call:action','for(set(i,0), i LT hotspot.count, inc(i), copy(hotspot[get(i)].keepbackup,hotspot[get(i)].keep);set(hotspot[get(i)].keep,true););reloadpano();for(set(i,0), i LT hotspot.count, inc(i), copy(hotspot[get(i)].keep,hotspot[get(i)].keepbackup););');
                });

                deletedHotspotsNameArray = _.difference(pluckedHotspotsArray, pluckedListColArray);
                _.each(deletedHotspotsNameArray, function (hotspotName) {
                    commands.execute('itemview:call:action', 'removehotspot(' + hotspotName + ');');
                });
            }
        },
        syncNavPlayer = function (sceneName, name, CompositeView) {
            var tourPlayer = reqres.request('tourPlayer');
            if (tourPlayer.get('xml.scene') !== sceneName) {
                commands.execute('itemview:call:action', "loadscene('" + sceneName + "',null,MERGE,BLEND(1));");

                //var tourPlayer = reqres.request('tourPlayer');
                var hotspotCollection = [];
                var hotspotCount = tourPlayer.get('hotspot.count');

                //Note : This is only the hotspot count for this scene
                for (var i = 0, len = hotspotCount; i < len; i++) {
                    //alert(tourPlayer.get('hotspot['+ i + '].url'));

                    if ((tourPlayer.get('hotspot[' + i + '].keep') === false) && (tourPlayer.get('hotspot[' + i + '].linkedscene') !== 'null') && (tourPlayer.get('hotspot[' + i + '].highlight') !== 'true')) {
                        hotspotCollection.push({
                            sceneName: tourPlayer.get('xml.scene'),
                            name: tourPlayer.get('hotspot[' + i + '].name'),
                            ath: tourPlayer.get('hotspot[' + i + '].ath'),
                            atv: tourPlayer.get('hotspot[' + i + '].atv'),
                            id: tourPlayer.get('hotspot[' + i + '].id'),
                            rotate: tourPlayer.get('hotspot[' + i + '].rotate'),
                            linkedscene: tourPlayer.get('hotspot[' + i + '].linkedscene')
                        });//Delete all hotspots in scene before posting this
                    }
                }

                //alert(hotspotCollection.length);
                var listCol = new Backbone.Collection(CompositeView.collection.where({sceneName: sceneName}));
                //var listCol = FlyerCompositeView.collection.where({sceneName:sceneName});
                var pluckedListColArray = listCol.pluck('name');//_.pluck(listCol,'hotspotName'); //listCol.pluck('hotspotName');
                //alert(JSON.stringify(pluckedListColArray));
                var pluckedHotspotsArray = _.pluck(hotspotCollection, 'name'); //hotspotCollection.pluck('hotspotName');
                //alert(JSON.stringify(pluckedHotspotsArray));

                //alert('list col array = ' + JSON.stringify(pluckedListColArray) + ' Hotspot Array ' + JSON.stringify(pluckedHotspotsArray));
                var missingHotspotsNameArray = _.difference(pluckedListColArray, pluckedHotspotsArray); //pluckedListColArray.without(pluckedHotspotsArray);
                //alert(missingHotspotsNameArray.length);

                _.each(missingHotspotsNameArray, function (name) {
                    var folderName = reqres.request('folderName');
                    var hotspotModel = listCol.findWhere({name: name}); //FlyerCompositeView.collection.findWhere({hotspotName:hotspotName});
                    //alert(JSON.stringify(hotspotModel));
                    var sceneName = hotspotModel.get('sceneName');
                    var name = hotspotModel.get('name');
                    var ath = hotspotModel.get('ath');
                    var atv = hotspotModel.get('atv');
                    var id = hotspotModel.get('id');
                    var rotate = hotspotModel.get('rotate');
                    var linkedscene = hotspotModel.get('linkedscene');

                    commands.execute('itemview:call:action', 'addhotspot(' + name + ');set(hotspot[' + name
                        + '].sceneName,' + sceneName + ');set(hotspot[' + name + '].name,' + name + ');set(hotspot[' + name + '].ath,' + ath + ');set(hotspot[' + name
                        + '].atv,' + atv + ');set(hotspot[' + name + '].id,' + id + ');set(hotspot[' + name
                        + '].rotate,' + rotate + ');set(hotspot[' + name + '].linkedscene,' + linkedscene + ');hotspot[' + name + '].loadstyle(skin_hotspotstyle);');
                    //This call refreshes the url
                    //commands.execute('itemview:call:action','for(set(i,0), i LT hotspot.count, inc(i), copy(hotspot[get(i)].keepbackup,hotspot[get(i)].keep);set(hotspot[get(i)].keep,true););reloadpano();for(set(i,0), i LT hotspot.count, inc(i), copy(hotspot[get(i)].keep,hotspot[get(i)].keepbackup););');

                });

                var deletedHotspotsNameArray = _.difference(pluckedHotspotsArray, pluckedListColArray);
                _.each(deletedHotspotsNameArray, function (name) {
                    commands.execute('itemview:call:action', 'removehotspot(' + name + ');');
                });
            }
        };

    var getNewBannerHotspotsCollection = function (collection) {//collection from database
        var tourPlayer = reqres.request('tourPlayer');
        var hotspotCollection = [];
        var hotspotCount = tourPlayer.get('hotspot.count');
        var getFileNameFromPath = function (path) {
            var ary = path.split("/");
            return ary[ary.length - 1];
        };

        for (var i = 0, len = hotspotCount; i < len; i++) {
            if (tourPlayer.get('hotspot[' + i + '].keep') === false) {
                //Checking to see if Flyers Collection already contains image hotspot with scene name and hotspot name
                var flyersFound = collection.where({sceneName: tourPlayer.get('xml.scene'), hotspotName: tourPlayer.get('hotspot[' + i + '].name')});
                //alert('Flyers Found Length = ' + flyersFound.length);
                if (tourPlayer.get('hotspot[' + i + '].url') && flyersFound.length === 0) {
                    hotspotCollection.push({
                        sceneName: tourPlayer.get('xml.scene'),
                        hotspotName: tourPlayer.get('hotspot[' + i + '].name'),
                        url: getFileNameFromPath(tourPlayer.get('hotspot[' + i + '].url')),
                        ath: tourPlayer.get('hotspot[' + i + '].ath'),
                        atv: tourPlayer.get('hotspot[' + i + '].atv'),
                        distorted: tourPlayer.get('hotspot[' + i + '].distorted'),
                        rx: Math.round(tourPlayer.get('hotspot[' + i + '].rx') * 10) / 10,
                        ry: Math.round(tourPlayer.get('hotspot[' + i + '].ry') * 10) / 10,
                        rz: Math.round(tourPlayer.get('hotspot[' + i + '].rz') * 10) / 10,
                        width: Math.round(tourPlayer.get('hotspot[' + i + '].width') * 10) / 10,
                        height: Math.round(tourPlayer.get('hotspot[' + i + '].height') * 10) / 10,
                        scale: Math.round(tourPlayer.get('hotspot[' + i + '].scale') * 100) / 100,
                        keep: tourPlayer.get('hotspot[' + i + '].keep'),
                        linkedscene: tourPlayer.get('hotspot[' + i + '].linkedscene'),
                        linkedScene: tourPlayer.get('hotspot[' + i + '].linkedScene'),
                        parent: tourPlayer.get('hotspot[' + i + '].parent'),
                        id: tourPlayer.get('hotspot[' + i + '].id')
                    });//Delete all hotspots in scene before posting this
                }
            }
        }
        return hotspotCollection;
    };

    var edit_tour_controller = marionette.Controller.extend({
        initialize: function (options) {
            this.mainRegion = options.mainRegion;

            reqres.setHandler('activeSceneName', function () {
                return '';
            });
            //this.selectionList = layout.selectionList;

            //this events are raised here directly as they have no route apis otherwise,
            //they would be triggered from the app entry file.
            this.listenTo(vent, 'itemview:load:menu:scenes', this.showMenuScenesList);
            this.listenTo(vent, 'itemview:load:auto:stops', this.loadAutoStops);
            this.listenTo(vent, 'itemview:show:flyer:images', this.loadFlyerImages);
            this.listenTo(vent, 'itemview:load:scene:settings', this.loadSceneSettings);
            this.listenTo(vent, 'reload:tour:player', this.showTourPlayer);
            //this.listenTo(vent, 'itemview:show:editor',this.showSelection);
        },
        show: function (folderName) {
            //************* Here you can have an initial view before the main view
            var promise = reqres.request('mini_tours:entities', folderName);//defer.promise();
            var editTourEditorView = require('apps/edit_tour/views/editTourEditorView');
            var layout = new editTourEditorView();
            /*var menu = require('apps/main/views/main_view_menu');
            var mainMenu = new menu();*/

            this.sceneList = layout.sceneList;
            this.tourPlayer = layout.tourPlayer;
            this.menuList = layout.menuList;
            this.menuScenesList = layout.menuScenesList;
            this.autoViewList = layout.autoViewList;
            this.autoViewStopsList = layout.autoViewStopsList;
            this.editSceneSettings = layout.editSceneSettings;
            this.miniTourScenesSection = layout.miniTourScenesSection;
            this.flyerSection = layout.flyerSection;
            this.flyerEditSection = layout.flyerEditSection;
            this.imageSection = layout.imageSection;
            this.navSection = layout.navSection;
            var that = this;
            reqres.setHandler('miniTourScenesSection', function () {
                return that.miniTourScenesSection;
            });

            $.when(promise).done(
                function (collection) {
                    var collectionView, collectionViewTwo;
                    if (collection !== undefined) {
                        var miniSelCollection = collection;
                        var miniTransferCollection = collection.clone();
                        reqres.setHandler('miniSelectionCollection', function () {
                            return miniSelCollection;
                        });
                        reqres.setHandler('miniTransferCollection', function () {
                            return miniTransferCollection;
                        });
                        var selectionItemCompositeView = require('apps/edit_tour/views/selectionItemCompositeView');
                        var transferItemCompositeView = require('apps/edit_tour/views/transferItemCompositeView');
                        collectionView = new selectionItemCompositeView({collection: miniSelCollection});
                        //collectionViewTwo = new selectionItemCompositeView({collection: miniTransferCollection});
                        collectionViewTwo = new transferItemCompositeView({collection: miniTransferCollection});
                    }
                    //before you attach a view to region see if there are event handlers to attach
                    collectionView.on('itemview:set:active:item', function (childView, id) {
                        commands.execute('set:active:item', id);
                    });
                    collectionView.on('create:minitour:item', function (collection, attrObj) {
                        collection.create({text: attrObj.text, filename: reqres.request('filename')});
                    });
                    collectionViewTwo.on('itemview:set:transfer:active:item', function (childView, id) {
                        commands.execute('set:transfer:active:item', id);
                    });
                    collectionViewTwo.on('itemview:show:transfer:scene:list', function (childView, model) {
                        vent.trigger('show:transfer:scene:list', model.get('filename'), that.miniTourScenesSection);
                    });
                    collectionView.on('itemview:show:editor', function (childView, filename) {
                        vent.trigger('itemview:show:editor', reqres.request('folderName'), filename);
                    });

                    layout.on('show', function () {
                        //layout.header.show(mainMenu);
                        layout.selectionList.show(collectionView);
                        layout.miniToursSection.show(collectionViewTwo);
                    });

                    /*mainMenu.on('renderTourLibrary', function () {
                        //alert('show tour library called from edittourcontroller');
                        vent.trigger("showTourLibrary");
                    });*/

                    that.mainRegion.show(layout);
                });
            return promise;
        },
        showSelection: function (folderName, filename) {
            /*            reqres.setHandler('filename',function(){
             return filename;
             });*/
            this
                .showSceneList(folderName, filename, this.sceneList)
                .showTourPlayer(folderName, filename)
                .showMenuList(folderName, filename)
                .showAutoViewList(folderName, filename)
                .showFlyerList(folderName, filename)
                .showNavigationList(folderName, filename)
                .showImageList(folderName, filename);

            return this;
        },
        showSceneList: function (folderName, filename, region) {
            var promise = reqres.request('scenes:entities', folderName, filename);//defer.promise();
            var that = this;
            $.when(promise).done(function (collection) {
                var colView;
                if (collection !== undefined) {
                    //that.sceneCollection = collection;
                    reqres.setHandler('scenesCollection', function () {
                        return collection;//that.sceneCollection;
                    });
                    var editTourSceneCollectionView = require('apps/edit_tour/views/editTourSceneCollectionView');
                    colView = new editTourSceneCollectionView({collection: collection});
                }
                //Any events' handlers should be set here (channelling event to controller from view allow you to change behaviour and target of the events easily)
                colView.on('itemview:set:selected:scene:model', function (childView, modelJSON) {
                    reqres.setHandler('selectedSceneModel', function () {
                        return modelJSON;
                    });
                });
                colView.on('itemview:scene:delete', function (childView, model) {
                    model.destroy();
                });
                colView.on('itemview:load:scene:settings', function (childView, folderName, sceneName) {
                    vent.trigger('itemview:load:scene:settings', folderName, sceneName);
                });
                colView.on('itemview:scene:model:save', function (childView, model, options) {
                    model.save(options);
                });
                colView.on('itemview:call:action', function (childView, sceneName) {
                    commands.execute('itemview:call:action', "pauseAutotour();");
                    commands.execute('itemview:call:action', "loadscene('" + sceneName + "',null,MERGE,BLEND(1));");
                });
                colView.on('itemview:set:active:scene', function (childView, sceneName) {
                    commands.execute('itemview:set:active:scene', sceneName);
                });
                colView.on('itemview:scene:dropped', function (childView, index, model) {
                    model.once('sync', function () {
                        this.collection.fetch({reset: true});
                    });
                    model.save({index: index});
                    model.set({index: undefined});

                });
                //that.sceneList.show(colView);
                region.show(colView);
            });
            return this;
        },
        showTransferSceneList: function (folderName, filename, region) {
            var promise = reqres.request('scenes:entities', folderName, filename);//defer.promise();
            var that = this;
            $.when(promise).done(function (collection) {
                var cloneCollection = collection.clone();
                var colView;
                if (cloneCollection !== undefined) {
                    //that.sceneCollection = collection;
                    reqres.setHandler('scenesTransferCollection', function () {
                        return cloneCollection;//that.sceneCollection;
                    });
                    var editTourSceneCollectionTransferView = require('apps/edit_tour/views/editTourSceneCollectionTransferView');
                    colView = new editTourSceneCollectionTransferView({collection: cloneCollection});
                }
                //Any events' handlers should be set here (channelling event to controller from view allow you to change behaviour and target of the events easily)
                colView.on('itemview:set:selected:scene:model', function (childView, modelJSON) {
                    reqres.setHandler('selectedSceneModel', function () {
                        return modelJSON;
                    });
                });
                colView.on('itemview:scene:delete', function (childView, model) {
                    model.destroy();
                });

                colView.on('itemview:scene:model:save', function (childView, model, options) {
                    model.save(options);
                });
                colView.on('itemview:call:action', function (childView, sceneName) {
                    commands.execute('itemview:call:action', "pauseAutotour();");
                    commands.execute('itemview:call:action', "loadscene('" + sceneName + "',null,MERGE,BLEND(1));");
                });
                colView.on('itemview:set:active:transfer:scene', function (childView, sceneName) {
                    commands.execute('itemview:set:active:transfer:scene', sceneName);
                });
                colView.on('itemview:scene:dropped', function (childView, index, model) {
                    model.save({index: index});
                    model.set({index: undefined});
                });
                //that.sceneList.show(colView);
                region.show(colView);
            });
            return this;
        },
        showTourPlayer: function (folderName, filename) {
            var editTourPlayerView = require('apps/edit_tour/views/editTourPlayerView');
            var editTourPlayerModel = require('apps/edit_tour/models/editTourPlayerModel');
            var editTourPlayer = new editTourPlayerModel({/*put any options **** folderName:this.folderName*/});

            this.playerInstance = new editTourPlayerView({
                model: editTourPlayer,
                folderName: folderName,
                filename: filename
            });
            //Any events' handlers should be set here from itemview and bubbled up to collection view is necessary
            var that = this;
            /*commands.setHandler('itemview:call:action', function (action) {
             var player = that.playerInstance;
             player.krpanoPlayer().call(action);
             });*/
            commands.setHandler('itemview:call:action', function (action) {
                var player = that.playerInstance;
                player.krpanoPlayer().call(action);
            });
            reqres.setHandler('tourPlayer', function () {
                return that.playerInstance.krpanoPlayer();
            });

            this.playerInstance.on('nav:model:values', function (sceneName, hotspotname, atv, ath, linkedscene, rotate, parentName, id) {
                vent.trigger('nav:model:values', sceneName, hotspotname, atv, ath, linkedscene, rotate, parentName, id);
            });

            this.tourPlayer.show(this.playerInstance);
            return this;
        },
        showMenuList: function (folderName, filename) {
            var promise = reqres.request('menu:entities', folderName, filename);//defer.promise();
            var that = this;

            $.when(promise).done(function (collection) {
                var menuCollectionView;
                if (collection !== undefined) {
                    reqres.setHandler('menuCollection', function () {
                        return collection;
                    });
                    var editTourMenuItemCompositeView = require('apps/edit_tour/views/editTourMenuItemCompositeView');
                    menuCollectionView = new editTourMenuItemCompositeView({collection: collection});
                } else {/*set here empty collection view*/
                }
                //Any events' handlers should be set here from itemview and bubbled up to collection view is necessary
                menuCollectionView.on('itemview:load:menu:scenes', function (childview, model, folderName, filename) {
                    reqres.setHandler('selectedMenuItemModel', function () {
                        return model;
                    });
                    vent.trigger('itemview:load:menu:scenes', model, folderName, filename);
                });
                menuCollectionView.on('itemview:menu:item:dropped', function (childView, index, model) {
                    model.once('sync', function () {
                        this.collection.fetch({reset: true});
                    });
                    model.save({index: index});
                    model.set({index: undefined});
                });
                menuCollectionView.on('itemview:set:active:menu', function (childView, id) {
                    commands.execute('set:active:menu', id);
                });
                that.menuList.show(menuCollectionView);
            });
            return this;
        },
        showMenuScenesList: function (menuModel, folderName, filename) {
            var promise = reqres.request('menu_scenes:entities', menuModel, folderName, filename);//defer.promise();
            var that = this;
            $.when(promise).done(function (collection) {
                var menuScenesCollectionView;
                if (collection !== undefined) {
                    var editTourMenuSceneCompositeView = require('apps/edit_tour/views/editTourMenuSceneCompositeView');
                    menuScenesCollectionView = new editTourMenuSceneCompositeView({collection: collection});
                }
                menuScenesCollectionView.on('itemview:call:action', function (childView, sceneName) {
                    commands.execute('itemview:call:action', "pauseAutotour();");
                    commands.execute('itemview:call:action', "loadscene('" + sceneName + "',null,MERGE,BLEND(1));");

                });

                //Any events' handlers should be set here from itemview and bubbled up to collection view is necessary
                that.menuScenesList.show(menuScenesCollectionView);
            });

            return this;
        },
        showAutoViewList: function (folderName, filename) {
            var promise = reqres.request('auto_view:entities', folderName, filename); //defer.promise();

            var that = this;
            $.when(promise).done(function (collection) {
                var autoCollectionView,
                    editTourAutoCompositeView = require('apps/edit_tour/views/editTourAutoCompositeView');

                if (collection !== undefined) {
                    reqres.setHandler('autoViewCollection', function () {
                        return collection;//that.sceneCollection;
                    });
                    autoCollectionView = new editTourAutoCompositeView({collection: collection});
                } else {/*collection to show if null collection*/
                    autoCollectionView = new editTourAutoCompositeView({collection: collection});
                }

                //Any events' handlers should be set here from item View and bubbled up to collection view is necessary
                autoCollectionView.on('itemview:load:auto:stops', function (childView, model, folderName, filename) {
                    vent.trigger('itemview:load:auto:stops', model, folderName, filename);
                });
                autoCollectionView.on('itemview:set:selected:auto:model', function (childView, modelJSON) {
                    reqres.setHandler('selectedAutoViewModel', function () {
                        return modelJSON;
                    });
                });
                autoCollectionView.on('itemview:load:scene:settings', function (childView, sceneName) {
                    vent.trigger('itemview:load:scene:settings', sceneName);
                });
                autoCollectionView.on('itemview:call:action', function (childView, sceneName) {
                    reqres.setHandler('activeSceneName', function () {
                        return sceneName;
                    });
                    commands.execute('itemview:call:action', "pauseAutotour();");
                    commands.execute('itemview:call:action', "loadscene('" + sceneName + "',null,MERGE,BLEND(1));");
                });
                autoCollectionView.on('itemview:auto:view:dropped', function (childView, index, model) {
                    model.once('sync', function () {
                        this.collection.fetch({reset: true});
                    });
                    model.save({index: index});
                    model.set({index: undefined});
                });
                autoCollectionView.on('itemview:auto:view:delete', function (childView, model) {
                    model.destroy();
                });
                autoCollectionView.on('itemview:set:active:auto:view', function (childView, name) {
                    commands.execute('itemview:set:active:auto:view', name);
                });
                that.autoViewList.show(autoCollectionView);

                //Appears now bootstrap is loaded somewhere and this is irrelevant.
                //require(['jquery','bootstrap'], function ($) {
                //that.autoViewList.show(autoCollectionView);
                //$('.btn-group').button();
                //});

            });
            return this;
        },
        ChangeSliderEnabledState: function (isOneRadioEnabled, slider, sliderScale, selectedRadioValue, selectedPlayerFlyer, height, width, scale, sliderVarLabel, SliderScaleVarLabel) {
            if (!slider) {
                return
            }
            if (!sliderScale) {
                return
            }
            var flyerPlayerCollection = reqres.request('flyerPlayerCollection');
            if (isOneRadioEnabled && flyerPlayerCollection.selected) {
                var tourPlayer = reqres.request('tourPlayer');
                slider.slider('enable');
                if (selectedRadioValue === 'height') {
                    sliderVarLabel.text(height);
                    slider.slider('setValue', height);
                } else {
                    sliderVarLabel.text(width);
                    slider.slider('setValue', width);
                }
                sliderScale.slider('enable')
                SliderScaleVarLabel.text(scale);
                sliderScale.slider('setValue', scale);
            } else {
                slider.slider('disable');
                slider.slider('disable');
            }
        },
        showFlyerList: function (folderName, filename) {
            var promise = reqres.request('hotspot:entities', folderName, filename);//defer.promise();
            var that = this;
            $.when(promise).done(function (collection) {
                //var FlyerCompositeView;
                if (collection !== undefined) {
                    reqres.setHandler('flyerCollection', function () {
                        return collection;
                    });
                    var editTourFlyerCompositeView = require('apps/edit_tour/views/editTourFlyerCompositeView');
                    that.FlyerCompositeView = new editTourFlyerCompositeView({collection: collection});
                }

                that.FlyerCompositeView.on('add:editor', function () {
                    commands.execute('itemview:call:action', "pauseAutotour();");
                    commands.execute('itemview:call:action', 'addplugin(editor);set(layer[editor].url,img/editor.swf);set(plugin[editor].align,top);set(plugin[editor].keep,true);');
                });
                that.FlyerCompositeView.on('itemview:set:active:flyer', function (childView, id) {
                    commands.execute('set:active:flyer', id);
                });

                that.FlyerCompositeView.on('itemview:look:to:hotspot', function (childView, sceneName, hotspotName) {
                    reqres.setHandler('hotspotSceneName', function () {
                        return sceneName;
                    });
                    reqres.setHandler('hotspotName', function () {
                        return hotspotName;
                    });
                    //that.lookToHotspot(sceneName,hotspotName);
                    that.lookToHotspot(reqres.request('hotspotSceneName'), reqres.request('hotspotName'));

                });
                that.FlyerCompositeView.on('itemview:show:flyer:images', function (childView, model, folderName, filename) {
                    //alert('first load flyer image');
                    vent.trigger('itemview:show:flyer:images', model.toJSON(), folderName, filename);
                });

                var flyerComp = that.FlyerCompositeView;
                reqres.setHandler('selectedFlyerCollection', function () {
                    return flyerComp.selectedFlyerCollection();
                });

                that.FlyerCompositeView.on('add:all:flyers', function () {
                    var hotspotCollection = getNewBannerHotspotsCollection(collection);
                    var editTourHotspotsModel = require('apps/edit_tour/models/editTourHotspotsModel');
                    var hotspotModel = new editTourHotspotsModel({folderName: reqres.request('folderName'), filename: reqres.request('filename')});
                    var tourPlayer = reqres.request('tourPlayer');
                    hotspotModel.populateHotspotObject(tourPlayer.get('xml.scene'), hotspotCollection);

                    hotspotModel.once('sync', function () {
                        that.FlyerCompositeView.collection.selectNone();
                        that.FlyerCompositeView.collection.fetch({reset: true});
                        //for(set(i,0), i LT hotspotCount, inc(i), copy(hotspot[get(i)].keepbackup,hotspot[get(i)].keep);set(hotspot[get(i)].keep,true); );
                        //commands.execute('itemview:call:action','for(set(i,0), i LT hotspot.count, inc(i), copy(hotspot[get(i)].keepbackup,hotspot[get(i)].keep);set(hotspot[get(i)].keep,true););reloadpano();for(set(i,0), i LT hotspot.count, inc(i), copy(hotspot[get(i)].keep,hotspot[get(i)].keepbackup););');

                    });
                    hotspotModel.save();


                });
                that.FlyerCompositeView.on('disable:controls', function () {
                    delete that.isOneRadioEnabled;
                    delete that.slider;
                });
                that.FlyerCompositeView.on('radio:enabled', function (isOneRadioEnabled, slider, sliderScale, selectedRadioValue, selectedPlayerFlyer, sliderVarLabel, SliderScaleVarLabel) {
                    that.isOneRadioEnabled = isOneRadioEnabled;
                    that.slider = slider;
                    that.sliderScale = sliderScale;
                    that.selectedRadioValue = selectedRadioValue;
                    that.selectedPlayerFlyer = selectedPlayerFlyer
                    var tourPlayer = reqres.request('tourPlayer');

                    var height = selectedPlayerFlyer ? parseFloat(tourPlayer.get('hotspot[' + selectedPlayerFlyer.get('hotspotName') + '].height')) : 0;
                    var width = selectedPlayerFlyer ? parseFloat(tourPlayer.get('hotspot[' + selectedPlayerFlyer.get('hotspotName') + '].width')) : 0;
                    var scale = selectedPlayerFlyer ? parseFloat(tourPlayer.get('hotspot[' + selectedPlayerFlyer.get('hotspotName') + '].scale')) : 0;

                    //alert('height = ' + height + ' width ' + width);
                    that.ChangeSliderEnabledState(isOneRadioEnabled, slider, sliderScale, selectedRadioValue, selectedPlayerFlyer, height, width, Math.round(scale * 10) / 10, sliderVarLabel, SliderScaleVarLabel);
                });

                that.FlyerCompositeView.on('slide:action', function (slideValue, type, selectedRadioValue, playerFlyerModel) {
                    var tourPlayer = reqres.request('tourPlayer');
                    if (type === 'size') {
                        if (selectedRadioValue === 'height') {
                            tourPlayer.set('hotspot[' + playerFlyerModel.get('hotspotName') + '].height', slideValue);
                            playerFlyerModel.set({height: slideValue})
                        } else {
                            //console.log(slideValue);
                            tourPlayer.set('hotspot[' + playerFlyerModel.get('hotspotName') + '].width', slideValue);
                            playerFlyerModel.set({width: slideValue})

                        }
                    } else {
                        tourPlayer.set('hotspot[' + playerFlyerModel.get('hotspotName') + '].scale', slideValue);
                        playerFlyerModel.set({scale: slideValue})

                    }
                });
                that.FlyerCompositeView.on('paste:action', function (playerFlyerModel, copiedPlayerFlyer) {
                    var tourPlayer = reqres.request('tourPlayer');
                    tourPlayer.set('hotspot[' + playerFlyerModel.get('hotspotName') + '].distorted', copiedPlayerFlyer.get('distorted'));
                    tourPlayer.set('hotspot[' + playerFlyerModel.get('hotspotName') + '].height', copiedPlayerFlyer.get('height'));
                    tourPlayer.set('hotspot[' + playerFlyerModel.get('hotspotName') + '].width', copiedPlayerFlyer.get('width'));
                    tourPlayer.set('hotspot[' + playerFlyerModel.get('hotspotName') + '].scale', copiedPlayerFlyer.get('scale'));
                    //tourPlayer.set('hotspot[' + playerFlyerModel.get('hotspotName') + '].rx',copiedPlayerFlyer.get('rx'));
                    //tourPlayer.set('hotspot[' + playerFlyerModel.get('hotspotName') + '].ry',copiedPlayerFlyer.get('ry'));
                    //tourPlayer.set('hotspot[' + playerFlyerModel.get('hotspotName') + '].rz',copiedPlayerFlyer.get('rz'));
                    playerFlyerModel.set({height: copiedPlayerFlyer.get('height')});
                    playerFlyerModel.set({width: copiedPlayerFlyer.get('width')});
                    playerFlyerModel.set({scale: copiedPlayerFlyer.get('scale')})


                });

                that.FlyerCompositeView.on('load:new:flyers', function (playerFlyerRegion, sliderVarLabel, sliderScaleVarLabel) {
                    var hotspotCollection = getNewBannerHotspotsCollection(collection);

                    //alert('Player Flyer Collection Values = ' + JSON.stringify(hotspotCollection));
                    var playerFlyerCollectionView = require('apps/edit_tour/views/editTourFlyerPlayerCollectionView');

                    var editTourFlyerPlayerCollection = require('apps/edit_tour/collections/editTourFlyerPlayerCollection');

                    var playerFlyerCollection = new editTourFlyerPlayerCollection(hotspotCollection);
                    playerFlyerCollection.on("select:one", function (playerFlyerModel) {
                        //alert('player Flyer Model = '+JSON.stringify(playerFlyerModel));

                        var height = playerFlyerModel.get('height');
                        var width = playerFlyerModel.get('width');
                        var scale = playerFlyerModel.get('scale');
                        that.ChangeSliderEnabledState(that.isOneRadioEnabled, that.slider, that.sliderScale, that.selectedRadioValue, that.selectedPlayerFlyer, height, width, Math.round(scale * 10) / 10, sliderVarLabel, sliderScaleVarLabel);
                    });
                    reqres.setHandler('flyerPlayerCollection', function () {
                        return playerFlyerCollection;
                    });
                    var playerFlyerColView = new playerFlyerCollectionView({collection: playerFlyerCollection});

                    playerFlyerColView.on('itemview:set:active:player:flyer', function (childView, hotspotName) {
                        //alert('set active player called with = ' + hotspotName);
                        commands.execute('set:active:player:flyer', hotspotName);
                    });
                    playerFlyerColView.on('itemview:look:to:hotspot', function (childView, sceneName, hotspotName) {
                        commands.execute('itemview:call:action', "pauseAutotour();");
                        //syncPlayer(sceneName, hotspotName, that.FlyerCompositeView); //Load scene if different and syncs player and collection
                        //alert('hotspot name check for invalid = ' + hotspotName);
                        commands.execute('itemview:call:action', "looktohotspot('" + hotspotName + "',70, smooth(720,-720,720);");

                    });
                    playerFlyerRegion.show(playerFlyerColView);
                });
                that.flyerSection.show(that.FlyerCompositeView);
            });
            return this;
        },
        lookToHotspot: function (sceneName, hotspotName) {
            commands.execute('itemview:call:action', "pauseAutotour();");
            syncPlayer(sceneName, hotspotName, this.FlyerCompositeView); //Load scene if different and syncs player and collection
            commands.execute('itemview:call:action', "looktohotspot('" + hotspotName + "',70, smooth(720,-720,720);");

        },
        showImageList: function (folderName, filename) {

            var promise = reqres.request('image:entities', folderName);//defer.promise();
            var that = this;
            $.when(promise).done(function (collection) {
                //var ImageCompositeView;
                if (collection !== undefined) {
                    reqres.setHandler('imageCollection', function () {
                        //alert('is Image Selected ' + isImageSelected);
                        return collection;
                    });
                    var editTourImageCompositeView = require('apps/edit_tour/views/editTourImageCompositeView');
                    that.ImageCompositeView = new editTourImageCompositeView({collection: collection, folderName: folderName});
                }
                that.ImageCompositeView.on('itemview:set:active:image', function (childView, id) {
                    commands.execute('set:active:image', id);
                });
                that.ImageCompositeView.on('fetch:flyer:list', function () {
                    that.FlyerCompositeView.collection.fetch({reset: true});
                });
                that.ImageCompositeView.on('reload:tour:player', function () {
                    that.tourPlayer.once('show', function (view) {
                        //commands.execute('itemview:call:action', "pauseAutotour();");
                        //alert('show on reload tour called');
                        that.lookToHotspot(reqres.request('hotspotSceneName'), reqres.request('hotspotName'));
                    });
                    that.showTourPlayer(reqres.request('folderName'), reqres.request('filename'));

                });
                //fetch:flyer:list
                that.ImageCompositeView.on('apply:images', function () {
                    alert('Image composite apply images called');
                });
                that.imageSection.show(that.ImageCompositeView);
            });
            return this;

        },
        showNavigationList: function (folderName, filename) {

            var promise = reqres.request('navigation:entities', folderName, filename);//defer.promise();
            var that = this;
            $.when(promise).done(function (collection) {
                //alert(collection.length);
                var editTourNavCompositeView = require('apps/edit_tour/views/editTourNavCompositeView');
                var NavCompositeView = new editTourNavCompositeView({collection: collection});

                NavCompositeView.on('add:editor', function () {
                    commands.execute('itemview:call:action', "pauseAutotour();");
                    commands.execute('itemview:call:action',
                        'addplugin(vtoureditor);set(plugin[vtoureditor].url,img/vtoureditor.swf);' +
                            'set(plugin[vtoureditor].align,topleft);set(plugin[vtoureditor].keep,true);set(plugin[vtoureditor].zorder,9999);');
                });
                NavCompositeView.on('remove:editor', function () {
                    commands.execute('itemview:call:action', "pauseAutotour();");
                    commands.execute('itemview:call:action', 'removeplugin(vtoureditor);');
                });
                NavCompositeView.on('add:highlight', function () {
                    commands.execute('itemview:call:action', "pauseAutotour();");
                    var tourPlayer = reqres.request('tourPlayer');
                    //var hotspotCollection = [];
                    var hotspotCount = tourPlayer.get('hotspot.count');
                    var getFileNameFromPath = function (path) {
                        var ary = path.split("/");
                        return ary[ary.length - 1];
                    };

                    for (var i = 0, len = hotspotCount; i < len; i++) {
                        if ((tourPlayer.get('hotspot[' + i + '].keep') === false) && (tourPlayer.get('hotspot[' + i + '].linkedscene') !== 'null') && (tourPlayer.get('hotspot[' + i + '].highlight') !== 'true')) {

                            var origHotName = tourPlayer.get('hotspot[' + i + '].name');
                            var origId = tourPlayer.get('hotspot[' + i + '].id');
                            var origRotate = +tourPlayer.get('hotspot[' + i + '].rotate') || 0;
                            var hotParentName = 'parent_' + i + '_' + tourPlayer.get('hotspot[' + i + '].name');
                            var childOneName = 'child_1_' + i + '_' + tourPlayer.get('hotspot[' + i + '].name');
                            var childTwoName = 'child_2_' + i + '_' + tourPlayer.get('hotspot[' + i + '].name');
                            var childThreeName = 'child_3_' + i + '_' + tourPlayer.get('hotspot[' + i + '].name');
                            var childFourName = 'child_4_' + i + '_' + tourPlayer.get('hotspot[' + i + '].name');
                            var childFiveName = 'child_5_' + i + '_' + tourPlayer.get('hotspot[' + i + '].name');
                            var childSixName = 'child_6_' + i + '_' + tourPlayer.get('hotspot[' + i + '].name');
                            var childSevenName = 'child_7_' + i + '_' + tourPlayer.get('hotspot[' + i + '].name');
                            var childEightName = 'child_8_' + i + '_' + tourPlayer.get('hotspot[' + i + '].name');
                            var centerChild = 'child_9_' + i + '_' + tourPlayer.get('hotspot[' + i + '].name');
                            var ath = tourPlayer.get('hotspot[' + i + '].ath');
                            var atv = tourPlayer.get('hotspot[' + i + '].atv');
                            var linkedscene = tourPlayer.get('hotspot[' + i + '].linkedscene');

                            //alert('loop alert ' + linkedscene);
                            commands.execute('itemview:call:action',
                                'addhotspot(' + hotParentName + ');set(hotspot[' + hotParentName + '].width,52);set(hotspot[' + hotParentName + '].height,52);hotspot[' + hotParentName + '].loadstyle(parent_style);set(hotspot[' + hotParentName + '].ath,' + ath + ');set(hotspot[' + hotParentName + '].atv,' + atv + ');set(hotspot[' + hotParentName + '].highlight,true);set(hotspot[' + hotParentName + '].linkedscene,' + linkedscene + ');set(hotspot[' + hotParentName + '].id,' + origId + ');' +
                                    'addplugin(' + childOneName + ');set(plugin[' + childOneName + '].align,lefttop);set(plugin[' + childOneName + '].original,' + origHotName + ');set(plugin[' + childOneName + '].edge,center);set(plugin[' + childOneName + '].parent,hotspot[' + hotParentName + ']);plugin[' + childOneName + '].loadstyle(nav_style);set(plugin[' + childOneName + '].zoom,false);set(plugin[' + childOneName + '].zorder,9999);set(plugin[' + childOneName + '].onclick,js(onHotspotClick(get(original),get(parent),get(rotate),get(id))););set(plugin[' + childOneName + '].crop,0|0|26|26);set(plugin[' + childOneName + '].rotate,-45);' +
                                    'addplugin(' + childTwoName + ');set(plugin[' + childTwoName + '].align,top);set(plugin[' + childTwoName + '].original,' + origHotName + ');set(plugin[' + childTwoName + '].edge,center);plugin[' + childTwoName + '].loadstyle(nav_style);set(plugin[' + childTwoName + '].zoom,false);set(plugin[' + childTwoName + '].crop,0|0|26|26);set(plugin[' + childTwoName + '].rotate,0);set(plugin[' + childTwoName + '].onclick,js(onHotspotClick(get(original),get(parent),get(rotate),get(id))););set(plugin[' + childTwoName + '].parent,hotspot[' + hotParentName + ']);' +
                                    'addplugin(' + childThreeName + ');set(plugin[' + childThreeName + '].align,righttop);set(plugin[' + childThreeName + '].original,' + origHotName + ');set(plugin[' + childThreeName + '].edge,center);plugin[' + childThreeName + '].loadstyle(nav_style);set(plugin[' + childThreeName + '].zoom,false);set(plugin[' + childThreeName + '].crop,0|0|26|26);set(plugin[' + childThreeName + '].rotate,45);set(plugin[' + childThreeName + '].onclick,js(onHotspotClick(get(original),get(parent),get(rotate),get(id))););set(plugin[' + childThreeName + '].parent,hotspot[' + hotParentName + ']);' +
                                    'addplugin(' + childFourName + ');set(plugin[' + childFourName + '].align,left);set(plugin[' + childFourName + '].original,' + origHotName + ');set(plugin[' + childFourName + '].edge,center);plugin[' + childFourName + '].loadstyle(nav_style);set(plugin[' + childFourName + '].zoom,false);set(plugin[' + childFourName + '].crop,0|0|26|26);set(plugin[' + childFourName + '].rotate,-90);set(plugin[' + childFourName + '].onclick,js(onHotspotClick(get(original),get(parent),get(rotate),get(id))););set(plugin[' + childFourName + '].parent,hotspot[' + hotParentName + ']);' +
                                    'addplugin(' + childFiveName + ');set(plugin[' + childFiveName + '].align,right);set(plugin[' + childFiveName + '].original,' + origHotName + ');set(plugin[' + childFiveName + '].edge,center);plugin[' + childFiveName + '].loadstyle(nav_style);set(plugin[' + childFiveName + '].zoom,false);set(plugin[' + childFiveName + '].crop,0|0|26|26);set(plugin[' + childFiveName + '].rotate,90);set(plugin[' + childFiveName + '].onclick,js(onHotspotClick(get(original),get(parent),get(rotate),get(id))););set(plugin[' + childFiveName + '].parent,hotspot[' + hotParentName + ']);' +
                                    'addplugin(' + childSixName + ');set(plugin[' + childSixName + '].align,leftbottom);set(plugin[' + childSixName + '].original,' + origHotName + ');set(plugin[' + childSixName + '].edge,center);plugin[' + childSixName + '].loadstyle(nav_style);set(plugin[' + childSixName + '].zoom,false);set(plugin[' + childSixName + '].crop,0|0|26|26);set(plugin[' + childSixName + '].rotate,-135);set(plugin[' + childSixName + '].onclick,js(onHotspotClick(get(original),get(parent),get(rotate),get(id))););set(plugin[' + childSixName + '].parent,hotspot[' + hotParentName + ']);' +
                                    'addplugin(' + childSevenName + ');set(plugin[' + childSevenName + '].align,bottom);set(plugin[' + childSevenName + '].original,' + origHotName + ');set(plugin[' + childSevenName + '].edge,center);plugin[' + childSevenName + '].loadstyle(nav_style);set(plugin[' + childSevenName + '].zoom,false);set(plugin[' + childSevenName + '].crop,0|0|26|26);set(plugin[' + childSevenName + '].rotate,180);set(plugin[' + childSevenName + '].onclick,js(onHotspotClick(get(original),get(parent),get(rotate),get(id))););set(plugin[' + childSevenName + '].parent,hotspot[' + hotParentName + ']);' +
                                    'addplugin(' + childEightName + ');set(plugin[' + childEightName + '].align,rightbottom);set(plugin[' + childEightName + '].original,' + origHotName + ');set(plugin[' + childEightName + '].edge,center);plugin[' + childEightName + '].loadstyle(nav_style);set(plugin[' + childEightName + '].zoom,false);set(plugin[' + childEightName + '].crop,0|0|26|26);set(plugin[' + childEightName + '].rotate,135);set(plugin[' + childEightName + '].onclick,js(onHotspotClick(get(original),get(parent),get(rotate),get(id))););set(plugin[' + childEightName + '].parent,hotspot[' + hotParentName + ']);' +
                                    'addplugin(' + centerChild + ');set(plugin[' + centerChild + '].align,center);set(plugin[' + centerChild + '].original,' + origHotName + ');set(plugin[' + centerChild + '].edge,center);plugin[' + centerChild + '].loadstyle(nav_style);set(plugin[' + centerChild + '].zoom,false);set(plugin[' + centerChild + '].crop,0|0|26|26);set(plugin[' + centerChild + '].rotate,' + origRotate + ');set(plugin[' + centerChild + '].onclick,js(onHotspotClick(get(original),get(parent),get(rotate),get(id))););set(plugin[' + centerChild + '].parent,hotspot[' + hotParentName + ']);' +
                                    ');');

                            commands.execute('itemview:call:action', 'removehotspot(' + origHotName + ')');

                        }
                    }
                    ;

                    //commands.execute('itemview:call:action', 'for(set(i,0), i LT layer.count, inc(i),trace('layer[',i,'].name=',layer[get(i)].name););');
                });
                NavCompositeView.on('itemview:look:to:hotspot', function (childView, sceneName, name) {
                    var tourPlayer = reqres.request('tourPlayer');
                    commands.execute('itemview:call:action', "pauseAutotour();");
                    syncNavPlayer(sceneName, name, NavCompositeView); //Load scene if different and syncs player and collection
                    commands.execute('itemview:call:action', "looktohotspot('" + name + "',70, smooth(720,-720,720);");


                });
                vent.on('nav:model:values', function (sceneName, hotspotname, atv, ath, linkedscene, rotate, parentName, id) {
                    var navModelValues = {
                        sceneName: sceneName,
                        name: hotspotname,
                        atv: atv,
                        ath: ath,
                        linkedscene: linkedscene,
                        rotate: rotate,
                        parentName: parentName
                    };

                    if (id !== undefined && id !== null && id !== 'null') {
                        navModelValues.id = id
                    }
                    ;

                    var navComp = NavCompositeView.collection.create(navModelValues, {wait: true});
                    navComp.once('sync', function (model) {
                        var parentName = model.get('parentName');
                        var hotspotname = model.get('name');
                        var atv = model.get('atv');
                        var ath = model.get('ath');
                        var linkedscene = model.get('linkedscene');
                        var rotate = model.get('rotate');
                        var id = model.get('id');

                        var tourPlayer = reqres.request('tourPlayer');
                        var pluginCount = tourPlayer.get('plugin.count');
                        //alert('alert one called' + pluginCount);

                        var tempPluginNames = [];
                        for (var i = 0, len = pluginCount; i < len; i++) {
                            if (tourPlayer.get('plugin[' + i + '].parent') !== undefined && tourPlayer.get('plugin[' + i + '].parent') !==
                                null && tourPlayer.get('plugin[' + i + '].parent') !== 'null' && tourPlayer.get('plugin[' + i + '].parent').indexOf(parentName) >= 0) {

                                var origPlugName = tourPlayer.get('plugin[' + i + '].name');
                                tempPluginNames.push(origPlugName);
                                //commands.execute('itemview:call:action', 'removeplugin(' + origPlugName + ')');
                            }
                        }
                        ;
                        _.forEach(tempPluginNames, function (elem, i, list) {
                            commands.execute('itemview:call:action', 'removeplugin(' + elem + ')');
                        });
                        commands.execute('itemview:call:action', 'removehotspot(' + parentName + ')');
                        commands.execute('itemview:call:action', 'addhotspot(' + hotspotname + ');hotspot[' + hotspotname + '].loadstyle(skin_hotspotstyle);set(hotspot[' + hotspotname + '].ath,' + ath + ');set(hotspot[' + hotspotname + '].rotate,' + rotate + ');set(hotspot[' + hotspotname + '].atv,' + atv + ');set(hotspot[' + hotspotname + '].linkedscene,' + linkedscene + ');set(hotspot[' + hotspotname + '].id,' + id + '););');
                        NavCompositeView.collection.fetch({reset: true});
                    });
                });

                //alert('nav called');
                that.navSection.show(NavCompositeView);
            });
            return this;

        },
        loadAutoStops: function (autoViewModel, folderName, filename) {
            var promise = reqres.request('auto_stops:entities', autoViewModel, folderName, filename);//defer.promise();
            var that = this;
            $.when(promise).done(function (collection) {
                var editTourAutoStopsCompView;
                if (collection !== undefined) {
                    var editTourAutoStopsCompositeView = require('apps/edit_tour/views/editTourAutoStopsCompositeView');
                    editTourAutoStopsCompView = new editTourAutoStopsCompositeView({collection: collection});
                    //Any events' handlers should be set here from itemview and bubbled up to collection view is necessary
                } else {
                }
                that.autoViewStopsList.show(editTourAutoStopsCompView);
            });
        },
        loadFlyerImages: function (flyerModel, folderName, filename) {
            //alert('load flyer images called');
            var promise = reqres.request('flyer_images:entities', flyerModel, folderName, filename);//defer.promise();
            var that = this;
            $.when(promise).done(function (collection) {
                var editTourFlyerImagesColView;
                if (collection !== undefined) {
                    //alert('controller collection called ' + JSON.stringify(collection));
                    var editTourFlyerImagesCollectionView = require('apps/edit_tour/views/editTourFlyerImagesCollectionView');
                    editTourFlyerImagesColView = new editTourFlyerImagesCollectionView({collection: collection});
                    //Any events' handlers should be set here from itemview and bubbled up to collection view is necessary
                } else {
                }
                that.flyerEditSection.show(editTourFlyerImagesColView);
            });
        },
        loadSceneSettings: function (sceneName) {

            var promise = reqres.request('scene_settings:entities', sceneName);//defer.promise();

            var that = this;
            $.when(promise).done(function (model) {
                var settingsView;
                if (model !== undefined) {
                    var editTourSceneSettingsView = require('apps/edit_tour/views/editTourSceneSettingsView');
                    settingsView = new editTourSceneSettingsView({model: model});
                }
                //Any events' handlers should be set here from itemview and bubbled up to collection view if necessary
                settingsView.on('save:scene:settings:model', function (childView, scenesSettingsModel, options) {
                    //scenesSettingsModel.save(options);
                });
                settingsView.on('reload:tour:player', function () {
                    var folderName = reqres.request('folderName'),
                        filename = reqres.request('filename');
                    vent.trigger('reload:tour:player', folderName, filename);
                });
                settingsView.on('call:action', function () {
                    /*var folderName = reqres.request('folderName'),
                     filename = reqres.request('filename');
                     vent.trigger('reload:tour:player', folderName, filename);*/
                    commands.execute('itemview:call:action', 'reloadpano();');
                });
                that.editSceneSettings.show(settingsView);
            });
        },
        setActiveScene: function (name) {
            var scenesCollection = reqres.request('scenesCollection');
            var sceneToSelect = scenesCollection.find(function (scene) {
                return scene.get('name') === name;
            });
            sceneToSelect.select();
            scenesCollection.trigger('reset');
        },
        setActiveTransferScene: function (name) {
            var scenesCollection = reqres.request('scenesTransferCollection');
            var sceneToSelect = scenesCollection.find(function (scene) {
                return scene.get('name') === name;
            });
            sceneToSelect.select();
            scenesCollection.trigger('reset');
        },
        setActiveItem: function (Id) {
            var miniCollection = reqres.request('miniSelectionCollection');
            var miniToSelect = miniCollection.find(function (selection) {
                return selection.get('id') === Id;
            });
            miniToSelect.select();
            miniCollection.trigger('reset');
        },
        setActiveTransferItem: function (Id) {
            var miniCollection = reqres.request('miniTransferCollection');
            var miniToSelect = miniCollection.find(function (selection) {
                return selection.get('id') === Id;
            });
            miniToSelect.select();
            miniCollection.trigger('reset');
        },
        setActiveMenu: function (id) {

            var menuCollection = reqres.request('menuCollection');
            var menuToSelect = menuCollection.find(function (menu) {
                return menu.get('id') === id;
            });
            menuToSelect.select();
            menuCollection.trigger('reset');

        },
        setActiveFlyer: function (id) {

            var flyerCollection = reqres.request('flyerCollection');
            var flyerToSelect = flyerCollection.find(function (flyer) {
                return flyer.get('id') === id;
            });
            flyerToSelect.toggleSelected();
            flyerCollection.trigger('reset');

        },
        setActivePlayerFlyer: function (hsName) {
            var flyerPlayerCollection = reqres.request('flyerPlayerCollection');
            var flyerPlayerToSelect = flyerPlayerCollection.find(function (flyer) {
                return flyer.get('hotspotName') === hsName;
            });
            flyerPlayerToSelect.select();
            flyerPlayerCollection.trigger('reset');

        },
        setActiveImage: function (id) {

            var imageCollection = reqres.request('imageCollection');
            var imageToSelect = imageCollection.find(function (image) {
                return image.get('id') === id;
            });
            imageToSelect.toggleSelected();
            imageCollection.trigger('reset');

        },
        setActiveAutoViewScene: function (name) {
            var autoCollection = reqres.request('autoViewCollection');
            var autoViewToSelect = autoCollection.find(function (auto) {
                return auto.get('name') === name;
            });
            autoViewToSelect.select();
            //alert(JSON.stringify(autoViewToSelect));
            autoCollection.trigger('reset');
        }
    });
    return edit_tour_controller;
});