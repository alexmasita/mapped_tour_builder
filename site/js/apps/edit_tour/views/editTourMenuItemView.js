/**
 * Created by alexmasita on 10/19/13.
 */

define(function (require) {
    var $ = require('jquery'),
        listItemView = require('apps/edit_tour/views/listItemView'),
        mediator = require('Mediators/mediator'),
        vent = require('vent'),
        reqres = require('reqres'),
        commands = require('commands'),
        editTourMenuItemView = listItemView.extend({

            selectItem: function (e) {
                listItemView.prototype.selectItem.apply(this);
                if (!this.model.selected) {
                    this.trigger('load:menu:scenes', this.model, reqres.request('folderName'), reqres.request('filename'));
                    this.trigger('set:active:menu', this.model.get('id'));
                    //commands.execute('set:active:menu',this.model.get('id'));
                }
            },
            drop: function (event, index) {
                /* this.model.save({index: index});
                 this.model.set({index: undefined});*/
                this.trigger('menu:item:dropped', index, this.model);
            }
        });
    return editTourMenuItemView;
});