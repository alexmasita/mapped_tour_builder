/**
 * Created by alexmasita on 10/21/13.
 */

define(function(require){
    var listItemCompositeView = require('apps/edit_tour/views/listItemCompositeView'),
        transferItemView = require('apps/edit_tour/views/transferItemView');
    var transferItemCompositeView = listItemCompositeView.extend({
        itemView:transferItemView
    });
    return transferItemCompositeView;
});