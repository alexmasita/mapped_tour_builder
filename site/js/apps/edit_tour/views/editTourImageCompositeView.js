/**
 * Created by alexmasita on 11/17/13.
 */

define(function (require) {
    'use strict';
    var listItemCommandCompositeView = require('apps/edit_tour/views/listItemCommandCompositeView'),
        editTourImageItemView = require('apps/edit_tour/views/editTourImageItemView'),
        templates = require('templates'),
        reqres = require('reqres'),
        $ = require('jquery'),
        _ = require('underscore'),
        editTourImageCompositeView = listItemCommandCompositeView.extend({
            itemView: editTourImageItemView,
            template: templates.editTourImageContainerTemplate,
            isFilmChanged: function () {
                var isFilm = $('.isFilm').is(':checked');
                if (isFilm) {
                    $('.flyers').addClass('film');
                    $('.videos').addClass('film');

                    $('.isParent').prop('checked', false);
                    $('.isReplace').prop('checked', false);
                    $('.isParentLabel').removeClass('active');
                    $('.isReplaceLabel').removeClass('active');
                } else {
                    $('.flyers').removeClass('film');
                    $('.videos').removeClass('film');
                }
                this.collection.fetch({data: {isFilm: $('.isFilm').is(':checked')}, reset: true});
            },
            applyImages: function () {
                //this.trigger('apply:images');
                var editTourApplyImagesModel = require('apps/edit_tour/models/editTourApplyImagesModel');
                var applyImagesModel = new editTourApplyImagesModel({folderName: reqres.request('folderName'), filename: reqres.request('filename')});
                var isFilm = $('.isFilm').is(':checked');
                /*if(isFilm){
                 $('.isParent').prop('checked', false);
                 $('.isReplace').prop('checked', false);
                 $('.isParentLabel').removeClass('active');
                 $('.isReplaceLabel').removeClass('active');
                 }*/
                var canApplyImages = applyImagesModel.populateCollection(reqres.request('selectedFlyerCollection'), this.selectedImageCollection(), $('.isReplace').is(':checked'), isFilm);
                //alert('Selected Flyer Collection ' + reqres.request('selectedFlyerCollection').length + ' Selected Image Collection ' + this.selectedImageCollection());
                var that = this;
                if (canApplyImages) {
                    var parentCount = 0;
                    for (var key in this.selectedImageCollection()) {
                        if (this.selectedImageCollection().hasOwnProperty(key)) {
                            var objImage = this.selectedImageCollection()[key];
                            if (objImage.get('isParent') === 'true') {
                                parentCount++;
                            }
                        }
                    }

                    if (isFilm) {
                        applyImagesModel.once('sync', function () {
                            //that.collection.fetch({reset:true});
                            //****that.trigger('fetch:flyer:list'); just disabled
                            that.reloadTourPlayer();
                        });
                        applyImagesModel.save();
                    } else {
                        if ($('.isReplace').is(':checked')) {
                            if (parentCount === 1) {
                                applyImagesModel.once('sync', function () {
                                    //that.collection.fetch({reset:true});
                                    //****that.trigger('fetch:flyer:list'); just disabled
                                    that.reloadTourPlayer();
                                });
                                applyImagesModel.save();
                            } else {
                                alert('image collection must have only One parent');
                            }
                        } else {
                            if (parentCount === 0) {
                                applyImagesModel.once('sync', function () {
                                    //that.collection.fetch({reset:true});
                                    //****that.trigger('fetch:flyer:list'); just disabled
                                    that.reloadTourPlayer();
                                });
                                applyImagesModel.save();
                            } else {
                                alert('image collection must not have any parent image');
                            }
                        }
                    }
                    //alert('just before deselect all');
                    /*that.collection.selectNone();
                     that.collection.trigger('reset');*/
                } else {
                    alert('Please select the position and ' + (isFilm ? 'video' : 'image'));
                }
            },
            reloadTourPlayer: function () {
                this.trigger('reload:tour:player');
            },
            fileOptionClicked: function () {
                this.onClickFileOptions();
                $('.file-options').hide();
                $('.file-selection').show();
            },
            fileSelectionClose: function () {
                $('.new-address').val('');
                $('.file-options').show();
                $('.file-selection').hide();
            },
            baseEvents: {
                'click .apply-images': 'applyImages',
                'change .isFilm': 'isFilmChanged',
                'click .btn-file-opt': 'fileOptionClicked',
                'click .btn-file-selection-close': 'fileSelectionClose'
            },
            initialize: function (options) {
                this.folderName = options.folderName;
                //alert(options.folderName);
            },
            selectedImageCollection: function () {
                //alert(JSON.stringify(this.collection.selected));
                if (_.isObject(this.collection.selected)) {
                    return this.collection.selected;
                } else {
                    return {};
                }

            },
            onRender: function () {
                this.initializeUpload();
                this.initializeVideoUpload();
            },
            onClickFileOptions: function () {
                this.isParent = $('.isParent').is(':checked');
                this.isFilm = $('.isFilm').is(':checked');
            },
            initializeUpload: function () {
                var that = this;
                var url = function () {
                    return '/flyerupload/?' + $.param({folderName: that.folderName});
                    /*,isFilm:$('.isFilm').is(':checked')*/
                };//is the same as 'upload' without leading slash to show it is relative
                /*var url = function () {
                 return 'http://localhost:9090/flyerupload/' + that.folderName;
                 };*///is the same as 'upload' without leading slash to show it is relative
                //var url = 'http://localhost:9090/flyerupload';//is the same as 'upload' without leading slash to show it is relative
                require(['jqueryui', 'jquery.ui.widget', 'jquery.iframe-transport', 'jquery.fileupload'], function () {
                    $('#flyerfileupload').fileupload({
                        url: url(),
                        dataType: 'json',
                        done: function (e, data) {
                            $.each(data.files, function (index, file) {
                                //$('<p/>').text(file.name).appendTo('#files');
                                //$('#filename').val(file.name);
                                //alert(JSON.stringify(data));$('.isParent').is(':checked');$('.isFilm').is(':checked')
                                var imageCol = that.collection.create({filename: file.name, isParent: that.isParent, isFilm: that.isFilm, webAddress: $('.new-address').val() ? 'http://' + $('.new-address').val() : ''}, {wait: true});
                                imageCol.once('sync', function (model) {
                                    that.fileSelectionClose();
                                    $('.isParent').prop('checked', false);
                                    $('.isParentLabel').removeClass('active');
                                    //**********just added**********
                                    $('.isFilm').prop('checked', that.isFilm);
                                    that.isFilm ? $('.isFilmLabel').addClass('active') : $('.isFilmLabel').removeClass('active');
                                    //**********just added**********

                                    that.collection.selectNone();
                                    //that.collection.trigger('reset');
                                    that.collection.fetch({data: {isFilm: that.isFilm}, reset: true});
                                });
                                //console.log(data);
                                //alert('images collection called');
                                //that.collection.fetch({reset:true});
                            });
                        },
                        progressall: function (e, data) {
                            var progress = parseInt(data.loaded / data.total * 100, 10);
                            $('#image-progress .progress-bar').css(
                                'width',
                                progress + '%'
                            );
                        }
                    }).prop('disabled', !$.support.fileInput)
                        .parent().addClass($.support.fileInput ? undefined : 'disabled');

                });
            },
            initializeVideoUpload: function () {
                var that = this;
                var url = function () {
                    return '/videoupload/?' + $.param({folderName: that.folderName});
                    /*,isFilm:$('.isFilm').is(':checked')*/
                };
                //is the same as 'upload' without leading slash to show it is relative
                /*var url = function () {
                 return 'http://localhost:9090/flyerupload/' + that.folderName;
                 };*///is the same as 'upload' without leading slash to show it is relative
                //var url = 'http://localhost:9090/flyerupload';//is the same as 'upload' without leading slash to show it is relative
                require(['jqueryui', 'jquery.ui.widget', 'jquery.iframe-transport', 'jquery.fileupload'], function () {
                    $('#videofileupload').fileupload({
                        url: url(),
                        dataType: 'json',
                        done: function (e, data) {
                            $.each(data.files, function (index, file) {
                                //$('<p/>').text(file.name).appendTo('#files');
                                //$('#filename').val(file.name);
                                //alert(JSON.stringify(data));
                                var imageCol = that.collection.create({filename: file.name, isParent: $('.isParent').is(':checked'), isFilm: $('.isFilm').is(':checked')}, {wait: true});
                                imageCol.once('sync', function (model) {
                                    $('.isParent').prop('checked', false);
                                    $('.isParentLabel').removeClass('active');
                                    that.collection.selectNone();
                                    that.collection.fetch({data: {isFilm: $('.isFilm').is(':checked')}, reset: true});
                                });
                                //console.log(data);
                                //alert('images collection called');
                                //that.collection.fetch({reset:true});
                            });
                        },
                        progressall: function (e, data) {
                            var progress = parseInt(data.loaded / data.total * 100, 10);
                            $('#image-progress .progress-bar').css(
                                'width',
                                progress + '%'
                            );
                        }
                    }).prop('disabled', !$.support.fileInput)
                        .parent().addClass($.support.fileInput ? undefined : 'disabled');

                });
            }


        });
    return editTourImageCompositeView;
});