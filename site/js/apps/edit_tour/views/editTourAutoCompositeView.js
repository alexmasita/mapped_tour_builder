/**
 * Created by alexmasita on 10/8/13.
 */

define(function (require) {
    var marionette = require('marionette'),
        editTourAutoView = require('apps/edit_tour/views/editTourAutoView'),
        templates = require('templates'),
        mediator = require('Mediators/mediator'),
        reqres = require('reqres'),
        listItemCommandCompositeView = require('apps/edit_tour/views/listItemCommandCompositeView');
    var editTourAutoCompositeView = listItemCommandCompositeView.extend({
        itemView: editTourAutoView,
        itemViewContainer: '.list-item',
        template: templates.autoViewContainerTemplate,
        /* modelEvents:{
         'sync':'syncedModel'
         },
         syncedModel:function(){
         this.render();
         },*/
        onRender: function () {
            require(['jqueryui'], function () {
                this.$('.list-item').sortable({
                    stop: function (event, ui) {
                        //alert(ui.item.index());
                        ui.item.trigger('drop', ui.item.index());
                    }});
            });
        },
        newAttributes: function () {
            return {
                name: reqres.request('selectedSceneModel').name,
                title: reqres.request('selectedSceneModel').title
            };
        },
        addSelected: function () {
            if (reqres.request('selectedSceneModel')) {
                var AutoScenes = this.collection.where({name: reqres.request('selectedSceneModel').name});
                if (AutoScenes.length) {
                    alert("This scene has already been added");
                    return;
                } else {
                    //this.listenToOnce(this.editTourMenuScenesCollection, 'add', this.renderMenuScene);
                    this.collection.create(this.newAttributes());
                }
            } else {
                alert("Please select a Scene");
            }
        },
        addAll: function () {
            var editTourAutoViewAllScenesModel = require('apps/edit_tour/models/editTourAutoViewAllScenesModel');
            var allScenesModel = new editTourAutoViewAllScenesModel({folderName: reqres.request('folderName'), filename: reqres.request('filename')});
            allScenesModel.populateCollection(reqres.request('scenesCollection'));
            var that = this;
            allScenesModel.once('sync', function () {
                that.collection.fetch({reset: true});
            });
            allScenesModel.save();
        }

    });
    return editTourAutoCompositeView;
});