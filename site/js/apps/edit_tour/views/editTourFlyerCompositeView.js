/**
 * Created by alexmasita on 11/6/13.
 */

define(function (require) {
    var marionette = require('marionette'),
        $ = require('jquery'),
        listItemCommandCompositeView = require('apps/edit_tour/views/listItemCommandCompositeView'),
        editTourFlyerItemView = require('apps/edit_tour/views/editTourFlyerItemView'),
        templates = require('templates'),
        _ = require('underscore'),
        editTourFlyerCompositeView = listItemCommandCompositeView.extend({
            itemView: editTourFlyerItemView,
            template: templates.editTourFlyerContainerTemplate,
            addAll: function () {
                this.trigger('add:all:flyers');
                $('.editor-options').show();
                $('.editor-update').hide();
            },
            addEditor: function () {
                this.trigger('add:editor');
            },
            addOptions: function () {
                $('.editor-options').hide();
                $('.editor-update').show();
                //this.sliderEnabledState();
            },
            closeOptions: function () {
                $('.editor-options').show();
                $('.editor-update').hide();
            },
            HWChange: function () {
                //alert('btnH is checked? '+$('.btnH').is(':checked') + ' btnW is checked? ' + $('.btnW').is(':checked'));
                if (!this.PlayerFlyersRegion) return;
                var isOneRadioEnabled = this.isRadioEnabledState();
                var selectedRadioValue = this.selectedRadioValue();
                this.selectedPlayerFlyer = function () {
                    return this.PlayerFlyersRegion.currentView.collection.selected;
                };
                this.trigger('radio:enabled', isOneRadioEnabled, this.$sliderVar, this.$sliderScaleVar, selectedRadioValue, this.selectedPlayerFlyer(), this.$sliderVarLabel, this.$sliderScaleVarLabel);
            },
            loadNewFlyers: function () {
                this.PlayerFlyersRegion = new marionette.Region({
                    el: ".player-flyer-section"
                });
                this.trigger('load:new:flyers', this.PlayerFlyersRegion, this.$sliderVarLabel, this.$sliderScaleVarLabel);
                //alert(JSON.stringify(this.PlayerFlyersRegion.currentView.collection));
                this.disableControls();
            },
            disableControls: function () {
                $('.labelH').removeClass('active');
                $('.labelW').removeClass('active');
                $('.btnH').prop('checked', false);
                $('.btnW').prop('checked', false);
                this.$sliderVar.slider('disable');
                this.$sliderScaleVar.slider('disable');
                this.trigger('disable:controls');
            },
            selectedFlyerCollection: function () {
                if (_.isObject(this.collection.selected)) {
                    return this.collection.selected;
                } else {
                    return {};
                }
            },
            copySelectedFlyer: function () {
                this.copiedSelectedFlyer = this.PlayerFlyersRegion.currentView.collection.selected;
            },
            pasteSelectedFlyer: function () {

                this.trigger('paste:action', this.selectedPlayerFlyer(), this.copiedSelectedFlyer);
            },
            baseEvents: {
                /*'click .add-selected': 'addSelected',*/
                'click .add-all': 'addAll',
                'click .add-editor': 'addEditor',
                'click .add-options': 'addOptions',
                'click .close-options': 'closeOptions',
                'click .btnLD': 'loadNewFlyers',
                'change .btnH': 'HWChange',
                'change .btnW': 'HWChange',
                'click .btnC': 'copySelectedFlyer',
                'click .btnP': 'pasteSelectedFlyer'
            },
            isRadioEnabledState: function () {
                /*if($('.btnH').is(':checked') || $('.btnW').is(':checked')){
                 this.$sliderVar.slider("enable");
                 } else {
                 this.$sliderVar.slider("disable");
                 }*/
                return ($('.btnH').is(':checked') || $('.btnW').is(':checked'));
            },
            selectedRadioValue: function () {
                return $("input[type='radio']:checked", ".form-group").val();
            },
            onRender: function () {
                var that = this;
                require(['bootstrap.slider'], function () {
                    that.$sliderVar = $('.slider').slider();
                    that.$sliderVarLabel = $("#lblSliderVal");
                    that.$sliderVar.on('slide', function (slideEvt) {

                        $("#lblSliderVal").text(slideEvt.value);//$("#lblSliderVal")
                        that.trigger('slide:action', slideEvt.value, 'size', that.selectedRadioValue(), that.selectedPlayerFlyer());
                    });
                    that.$sliderScaleVar = $('.sliderScale').slider();
                    that.$sliderScaleVarLabel = $("#lblSliderScaleVal");
                    that.$sliderScaleVar.on('slide', function (slideEvt) {
                        that.$sliderScaleVarLabel.text(Math.round(slideEvt.value * 100) / 100);
                        that.trigger('slide:action', slideEvt.value, 'scale', that.selectedRadioValue(), that.selectedPlayerFlyer());
                    });
                    //that.sliderEnabledState();
                });
            }
        });
    return editTourFlyerCompositeView;
});