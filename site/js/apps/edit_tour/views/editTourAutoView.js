define(function (require) {
    'use strict';
    var $ = require('jquery'),
        mediator = require('Mediators/mediator'),
        marionette = require('marionette'),
        templates = require('templates'),
        reqres = require('reqres'),
        vent = require('vent'),
        editTourAutoView = marionette.ItemView.extend({
            tagName: 'a',
            className: 'list-group-item',
            template: templates.autoViewTemplate,
            events: {
                'click .destroy': 'clear',
                'click': 'selectAutoScene',
                'drop': 'drop'

            },
            modelEvents: {
                'change': 'render',
                'destroy': 'remove'
            },
            clear: function (e) {
                e.preventDefault();
                //alert("destroy called");
                this.trigger('auto:view:delete', this.model);
                //this.model.destroy();
            },
            selectAutoScene: function (e) {
                e.preventDefault();
                if (!this.model.selected) {
                    this.trigger('set:selected:auto:model', this.model.toJSON);
                    this.trigger('load:auto:stops', this.model.toJSON(), reqres.request('folderName'), reqres.request('filename'));
                    this.trigger('load:scene:settings', this.model.get('name'));
                    this.trigger('call:action', this.model.get('name'));
                    this.trigger('set:active:auto:view', this.model.get('name'));
                }

            },
            onRender: function () {
                if (this.model.selected) {
                    this.$el.addClass('active');
                }
            },
            drop: function (event, index) {
                /*this.model.save({index: index});
                 this.model.set({index:undefined});*/
                this.trigger('auto:view:dropped', index, this.model);
            }
        });
    return editTourAutoView;

});