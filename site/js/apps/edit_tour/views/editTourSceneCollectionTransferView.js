/**
 * Created by alexmasita on 10/30/13.
 */

define(function(require){
    var editTourSceneCollectionView = require('apps/edit_tour/views/editTourSceneCollectionView'),
        editTourSceneTransferView = require('apps/edit_tour/views/editTourSceneTransferView');

    var editTourSceneCollectionTransferView = editTourSceneCollectionView.extend({
        itemView:editTourSceneTransferView
    });
    return editTourSceneCollectionTransferView;
});