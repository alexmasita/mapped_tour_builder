/**
 * Created by alexmasita on 10/30/13.
 */

define(function(require){
    var editTourSceneView = require('apps/edit_tour/views/editTourSceneView');
    var editTourSceneTransferView = editTourSceneView.extend({
        selectScene:function(e){
            e.preventDefault();
            if (!this.model.selected){
                //this.trigger('set:selected:scene:model',this.model.toJSON());
                //this.trigger('load:scene:settings',this.model.get('name'));
                //this.trigger('play:active:scene',this.model.get('name'));
                this.trigger('call:action',this.model.get('name'));
                this.trigger('set:active:transfer:scene',this.model.get('name'));
            }
        }
    });
    return editTourSceneTransferView;
});