/**
 * Created by alexmasita on 10/21/13.
 */

define(function (require) {
    var ENTER_KEY = 13,
        listItemCompositeView = require('apps/edit_tour/views/listItemCompositeView'),
        $ = require('jquery'),
        selectionItemView = require('apps/edit_tour/views/selectionItemView');
    var selectionItemCompositeView = listItemCompositeView.extend({
        itemView: selectionItemView,
        newAttributes: function () {
            var that = this;
            return {
                text: this.$input.val().trim()
            }
        },
        createOnEnter: function (event) {
            if (event.which !== ENTER_KEY || !this.$input.val().trim()) {
                return;
            }

            var selectionNames = this.collection.where({text: this.$input.val().trim()});
            if (selectionNames.length) {
                alert('This Name Already Exists : Select Another Name');
            } else {
                //this.collection.create(this.newAttributes());
                this.trigger('create:minitour:item',this.collection,this.newAttributes());
            }
            this.$input.val('');
        }
    });
    return selectionItemCompositeView;
});