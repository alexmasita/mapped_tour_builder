/**
 * Created by alexmasita on 10/8/13.
 */

define(function (require) {
    var marionette = require('marionette'),
        editTourAutoView = require('apps/edit_tour/views/editTourAutoView'),
        templates = require('templates'),
        mediator = require('Mediators/mediator'),
        reqres = require('reqres'),
        _ = require('underscore');
    var listItemCommandCompositeView = function (options) {
        this.inheritedEvents = [];
        marionette.CompositeView.call(this, options);
    };

    _.extend(listItemCommandCompositeView.prototype, marionette.CompositeView.prototype, {
        itemView: editTourAutoView,
        itemViewContainer: '.list-item',
        template: templates.autoViewContainerTemplate,
        /*modelEvents:{
         'sync':'render'
         },
         collectionEvents:{
         'reset':'syncedModel'
         },
         syncedModel:function(){
         this.render();
         },
         onRender:function(){
         },*/
        baseEvents: {
            'click .add-selected': 'addSelected',
            'click .add-all': 'addAll'
        },
        events: function () {
            var e = _.extend({}, this.baseEvents);
            _.forEach(this.inheritedEvents, function (events) {
                e = _.extend(e, events);
            });
            return e;
        },
        addEvent: function (eventObj) {
            this.inheritedEvents.push(eventObj);
        },
        newAutoSceneAttributes: function () {
            return {
                name: reqres.request('selectedSceneModel').name,
                title: reqres.request('selectedSceneModel').title
            };
        },
        addSelected: function () {
        },
        addAll: function () {
        }

    });
    listItemCommandCompositeView.extend = marionette.CompositeView.extend;
    return listItemCommandCompositeView;
});