/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/18/13
 * Time: 4:10 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app ||{};
define(function (require) {
    var $ = require('jquery'),
        marionette = require('marionette'),
        mediator = require('Mediators/mediator'),
        templates = require('templates'),
        reqres = require('reqres'),
        editTourAutoStopsView = marionette.ItemView.extend({
            tagName: 'a',
            className: 'list-group-item',
            template: templates.autoViewStopTemplate,
            events: {
                'click .destroy': 'destroy',
                'click': 'select'
            },
            destroy: function () {
                //alert('destroy called');
                if (!this.model.get('id')) {
                    this.model.set('id', this.model.get('name'));
                }
                this.model.destroy();
            },
            initialize: function () {

            },
            select: function (e) {
                e.preventDefault();
                if ($('#auto-view-stops-list').find('a').hasClass('active')) {
                    $('#auto-view-stops-list').find('a').removeClass('active');
                }
                mediator.models.selectedAutoStopModel = null;
                mediator.models.selectedAutoStopModel = this.model.toJSON();
                //Backbone.trigger('loadAutoStops',this.model.toJSON());
                this.$el.addClass('active');
                //app.krpano.call("loadscene('"+this.model.get("name")+"',null,MERGE,BLEND(1));");
                reqres.request('tourPlayer').call(
                    "tween(view.hlookat,'" + this.model.get('h') + "', '" + this.model.get('tweentime') + "','" + this.model.get('tween') + "');" +
                        "tween(view.vlookat,'" + this.model.get('v') + "', '" + this.model.get('tweentime') + "','" + this.model.get('tween') + "');" +
                        "tween(view.fov,'" + this.model.get('fov') + "', '" + this.model.get('tweentime') + "','" + this.model.get('tween') + "');"
                );
            }
        });
    return editTourAutoStopsView;
});

