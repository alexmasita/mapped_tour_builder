
define(function(require){
    var listItemView = require('apps/edit_tour/views/listItemView'),
        vent = require('vent'),
        reqres = require('reqres'),
        commands = require('commands');
    var selectionItemView = listItemView.extend({
        selectItem:function(e){
            //listItemView.prototype.selectItem.apply(this);
            if (!this.model.selected){
                //alert(this.model.selected);
                this.trigger('show:editor',this.model.get('filename'));
                this.trigger('set:active:item',this.model.get('id'));
            }
        },
        clear:function(){
            var result = confirm("Want to delete?");
            if (result==true) {
                //Logic to delete the item
                this.model.destroy({wait: true});
                this.model.destroy({success: function(model, response) {
                    vent.trigger('edit_tour_app:show',reqres.request('folderName'));
                }});
            }
        }
    });
    return selectionItemView;
});