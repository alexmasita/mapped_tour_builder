/**
 * Created by alexmasita on 10/29/13.
 */

define(function(require){
    var listItemView = require('apps/edit_tour/views/listItemView'),
        vent = require('vent'),
        reqres = require('reqres'),
        commands = require('commands');
    var transferItemView = listItemView.extend({
        selectItem:function(e){
            //listItemView.prototype.selectItem.apply(this);
            //if (!this.model.selected){
                var that = this;
                reqres.setHandler('transferfilename',function(){
                    return that.model.get('filename');
                });
                this.trigger('show:transfer:scene:list',this.model);
                this.trigger('set:transfer:active:item',this.model.get('id'));
            //}
        }
    });
    return transferItemView;
});