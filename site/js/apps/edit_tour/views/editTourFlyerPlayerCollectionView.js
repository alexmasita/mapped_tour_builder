define(function (require) {
    'use strict';
    var marionette = require('marionette'),
        editTourFlyerPlayerItemView = require('apps/edit_tour/views/editTourFlyerPlayerItemView');
    var editTourFlyerPlayerCollectionView = marionette.CollectionView.extend({
        itemView: editTourFlyerPlayerItemView
    });
    return editTourFlyerPlayerCollectionView;
});