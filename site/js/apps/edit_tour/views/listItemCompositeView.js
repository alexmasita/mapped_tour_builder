
define(function(require){
    var ENTER_KEY = 13,
        marionette = require('marionette'),
        listItemView = require('apps/edit_tour/views/listItemView'),
        emptyItemView = require('apps/edit_tour/views/emptyItemView'),
        templates = require('templates'),
        _ = require('underscore'),
        $ = require('jquery');


    var listItemCompositeView = function(options){
        this.inheritedEvents = [];
        marionette.CompositeView.call(this,options);
    };

    _.extend(listItemCompositeView.prototype,marionette.CompositeView.prototype,{
        itemView:listItemView,
        itemViewContainer:'.list-item',
        template:templates.listItemContainerTemplate,
        emptyView: emptyItemView,
        initialize:function(){
            this.initializeChild();
        },
        initializeChild:function(){},
        modelEvents:{
            "change":"render",
            "reset":"render"
        },
        onRender:function(){
            this.$input = this.$('.new-item');
            this.onRenderChild();
        },
        onRenderChild:function(){},
        baseEvents:{
            'keypress .new-item':'createOnEnter'
        },
        events:function(){
            var e = _.extend({},this.baseEvents);
            _.forEach(this.inheritedEvents,function(events){
                e = _.extend(e,events);
            });
            return e;
        },
        addEvent:function(eventObj){
            this.inheritedEvents.push(eventObj);
        },
        newAttributes: function () {
            var that = this;

            return {
                text: this.$input.val().trim()
            }
        },
        createOnEnter: function (event) {
            if (event.which !== ENTER_KEY || !this.$input.val().trim()) {
                return;
            }

            var selectionNames = this.collection.where({text: this.$input.val().trim()});
            if (selectionNames.length){
                alert('This Name Already Exists : Select Another Name');
            } else {
                this.collection.create(this.newAttributes());

            }
            this.$input.val('');
        }


    });

/*
    var listItemCompositeView = marionette.CompositeView.extend({
        itemView:listItemView,
        itemViewContainer:'#list-item',
        template:templates.listItemContainerTemplate,
        events:{
            'keypress #new-menu':'createOnEnter'
        },
        createOnEnter: function (event) {
            if (event.which !== ENTER_KEY || !this.$input.val().trim()) {
                return;
            }
            this.collection.create(this.newAttributes());
            this.$input.val('');
        },
        newAttributes: function () {
            var that = this;

            return {
                text: this.$input.val().trim(),
                name: that.collection.nextOrder()
            }
        },
        onRender:function(){
            this.$input = this.$('#new-item');
            this.onRenderChild();
        },
        onRenderChild:function(){}
    });
*/
    listItemCompositeView.extend = marionette.CompositeView.extend;
    return listItemCompositeView;
});