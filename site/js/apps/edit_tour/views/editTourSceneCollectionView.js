/**
 * Created by alexmasita on 10/8/13.
 */

define(function(require){
    var marionette = require('marionette'),
        editTourSceneView = require('apps/edit_tour/views/editTourSceneView'),
        reqres = require('reqres'),
        _ = require('underscore'),
        templates = require('templates'),
        vent = require('vent');

    var editTourSceneCollectionView = function(options){
        this.inheritedEvents = [];
        marionette.CollectionView.call(this,options);
    };

    _.extend(editTourSceneCollectionView.prototype,marionette.CompositeView.prototype,{
        itemView:editTourSceneView,
        itemViewContainer:'.scene-list',
        template:templates.editTourSceneContainerTemplate,
        className:'list-group list-padding',
/*
        id:'scene-list',
*/
        baseEvents: {
            'click .remove-scenes-class':'removeScenes',
            'click .add-scene-class':'addScene'

        },
        events: function() {
            var e = _.extend({}, this.baseEvents);
            _.forEach(this.inheritedEvents, function(events) {
                e = _.extend(e, events);
            });
            return e;
        },
        addEvents: function(eventObj) {
            this.inheritedEvents.push(eventObj);
        },
        removeScenes:function(){
            var result = confirm("Sure you want to delete all scenes?");
            if (result==true) {
                var editTourBulkScenesModel = require('apps/edit_tour/models/editTourBulkScenesModel');
                var allScenesModel = new editTourBulkScenesModel({folderName: reqres.request('folderName')});
                allScenesModel.set({filename:reqres.request('filename')});
                //allScenesModel.populateCollection(this.collection);
                this.listenToOnce(allScenesModel,'sync',function(){
                    this.collection.reset();
                });
                allScenesModel.destroy();
            }
        },
        addScene:function(){
            if (!reqres.request('transferfilename')){
                alert('Please Select a Mini Tour Scene');
                return;
            } else if (!reqres.request('filename')){
                alert('Please Select a Main Tour Scene');
                return;
            } else if (!reqres.request('selectedSceneModel').name){
                alert('Please Select Main Scene');
                return;
            }

            var editTourBulkScenesModel = require('apps/edit_tour/models/editTourBulkScenesModel');
            var allScenesModel = new editTourBulkScenesModel({folderName: reqres.request('folderName')});
            //get filename and name from the main scenes file
            allScenesModel.set({filename:reqres.request('transferfilename'),mainfilename:reqres.request('filename'),name:reqres.request('selectedSceneModel').name});
            this.listenToOnce(allScenesModel,'sync',function(){
                vent.trigger('show:transfer:scene:list', reqres.request('transferfilename'), reqres.request('miniTourScenesSection'));
            });
            allScenesModel.save();
        },
        onRender:function(){
            require(['jqueryui'],function(){
                this.$('.scene-list').sortable({
                    stop: function (event, ui) {
                        ui.item.trigger('drop', ui.item.index());
                    }});
            });
        }
    });

    editTourSceneCollectionView.extend = marionette.CompositeView.extend;
    return editTourSceneCollectionView;
});