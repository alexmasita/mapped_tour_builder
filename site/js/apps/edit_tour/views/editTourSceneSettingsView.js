/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/20/13
 * Time: 6:14 PM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};

define(function (require) {

    var $ = require('jquery'),
        templates = require('templates'),
        marionette = require('marionette'),
        mediator = require('Mediators/mediator'),
        vent = require('vent'),
        reqres = require('reqres'),
        commands = require('commands');

    var editTourSceneSettingsView = marionette.ItemView.extend({
        events: {
            'click #btnspinnerHlookatmin': 'setHlookatmin',
            'click #btnspinnerHlookatmax': 'setHlookatmax',
            'click #btnVlookatmin': 'setVlookatmin',
            'click #btnVlookatmax': 'setVlookatmax',
            'click #btnspinnerDefaultlookAt': 'setDefaultlookAt',
            'click #btnSaveAllSettings': 'btnSaveAllSettings'
        },
        template: templates.editSceneSettingsTemplate,
        onRender: function (view, options) {
            var that = this;
            require(['jqueryui'], function () {
                that.$spinnerHlookatmin = $("#spinnerHlookatmin").spinner();
                that.$spinnerHlookatmin.spinner('value', view.model.get('hlookatmin'));
                that.$spinnerHlookatmax = $("#spinnerHlookatmax").spinner();
                that.$spinnerHlookatmax.spinner('value', view.model.get('hlookatmax'));
                that.$spinnerVlookatmin = $("#spinnerVlookatmin").spinner();
                that.$spinnerVlookatmin.spinner('value', view.model.get('vlookatmin'));
                that.$spinnerVlookatmax = $("#spinnerVlookatmax").spinner();
                that.$spinnerVlookatmax.spinner('value', view.model.get('vlookatmax'));
                that.$spinnerFovMin = $("#spinnerFovMin").spinner();
                that.$spinnerFovMin.spinner('value', view.model.get('fovmin'));
                that.$spinnerFovMax = $("#spinnerFovMax").spinner();
                that.$spinnerFovMax.spinner('value', view.model.get('fovmax'));
                that.$spinnerLat = $("#spinnerLat").spinner();
                that.$spinnerLat.spinner('value', view.model.get('lat'));
                that.$spinnerLng = $("#spinnerLng").spinner();
                that.$spinnerLng.spinner('value', view.model.get('lng'));
                that.$spinnerHlookAt = $("#spinnerHlookAt").spinner();
                that.$spinnerHlookAt.spinner("value", view.model.get('hlookat'));
                that.$spinnerVlookAt = $("#spinnerVlookAt").spinner();
                that.$spinnerVlookAt.spinner("value", view.model.get('vlookat'));
                that.$spinnerFov = $("#spinnerFov").spinner();
                that.$spinnerFov.spinner('value', view.model.get('fov'));
                that.$spinnerMaxPxZm = $("#spinnerMaxPxZm").spinner();
                that.$spinnerMaxPxZm.spinner('value', view.model.get('maxpixelzoom'));
                that.$allScenes = function(){
                    return $('#allScenesCheck').is(":checked");
                };
            });

        },
        setHlookatmin: function (event) {
            event.preventDefault();
            this.$spinnerHlookatmin.spinner('value', Math.round(reqres.request('tourPlayer').get("view.hlookat")));
            if (!this.$spinnerVlookatmin.spinner('value')) {
                this.$spinnerVlookatmin.spinner('value', '90');
                this.$spinnerVlookatmax.spinner('value', '-90');
            } else {
                console.log("spinner into else");
            }

        },
        setHlookatmax: function (event) {
            event.preventDefault();
            this.$spinnerHlookatmax.spinner('value', Math.round(reqres.request('tourPlayer').get("view.hlookat")));
            if (!this.$spinnerHlookatmax.spinner('value')) {
                this.$spinnerVlookatmin.spinner('value', '90');
                this.$spinnerVlookatmax.spinner('value', '-90');
            }
        },
        setVlookatmin: function (event) {
            event.preventDefault();
            this.$spinnerVlookatmin.spinner('value', Math.round(reqres.request('tourPlayer').get("view.vlookat")));

        },
        setVlookatmax: function (event) {
            event.preventDefault();
            this.$spinnerVlookatmax.spinner('value', Math.round(reqres.request('tourPlayer').get("view.vlookat")));
        },
        setDefaultlookAt: function (event) {
            event.preventDefault();

            this.$spinnerHlookAt.spinner("value", reqres.request('tourPlayer').get("view.hlookat"));
            this.$spinnerVlookAt.spinner("value", reqres.request('tourPlayer').get("view.vlookat"));
            this.$spinnerFov.spinner('value', reqres.request('tourPlayer').get("view.fov"));

        },
        newSceneSettingsAttrPromise: function () {
            var that = this;
            return {
                hlookatmin: that.$spinnerHlookatmin.spinner('value'),
                hlookatmax: that.$spinnerHlookatmax.spinner('value'),
                vlookatmin: that.$spinnerVlookatmin.spinner('value'),
                vlookatmax: that.$spinnerVlookatmax.spinner('value'),
                fovmin: that.$spinnerFovMin.spinner('value'),
                fovmax: that.$spinnerFovMax.spinner('value'),
                allScenes:that.$allScenes(),//use ui key attribute to fetch ckecked state
                lat: that.$spinnerLat.spinner('value'),
                lng: that.$spinnerLng.spinner('value'),
                hlookat: that.$spinnerHlookAt.spinner("value"),
                vlookat: that.$spinnerVlookAt.spinner("value"),
                fov: that.$spinnerFov.spinner('value'),
                maxpixelzoom: that.$spinnerMaxPxZm.spinner('value')
            };

        },
        btnSaveAllSettings: function (event) {
            event.preventDefault();
            //console.log('this entered' + JSON.stringify(this.newSceneSettingsAttrPromise()));

            this.model.save(this.newSceneSettingsAttrPromise());
            //this.trigger('save:scene:settings:model',this.model,this.newSceneSettingsAttrPromise());
            //this.trigger('reload:tour:player');
            //commands.execute('itemview:play:active:scene','reloadpano();');
            this.trigger('call:action');
           // vent.trigger('showTourPlayer', reqres.request('folderName'));
        }

    });
    return editTourSceneSettingsView;
});

