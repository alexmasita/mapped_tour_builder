
define(function(require){
    var marionette = require('marionette'),
        templates = require('templates');

    var editTourEditorView = marionette.Layout.extend({
        className:'editor-container',
        template: templates.editTourEditorViewTemplate,
        regions: {
            header:".appHeader",
            selectionList:".page-selections",
            sceneList:".scene-section",
            tourPlayer: ".tour-player",
            miniToursSection:".mini-tours-section",
            miniTourScenesSection:".tour-scenes-section",
            menuList:".menu-section",
            menuScenesList:".menu-scenes-section",
            autoViewList:".auto-view-section",
            autoViewStopsList:".auto-view-stops-section",
            editSceneSettings:".edit-scene-settings",
            flyerSection:".flyer-section",
            flyerEditSection:".flyer-edit-section",
            imageSection:".image-section",
            navSection:".nav-section",
            navEditSection:".nav-edit-section"
        }
    });
    return editTourEditorView;
});


