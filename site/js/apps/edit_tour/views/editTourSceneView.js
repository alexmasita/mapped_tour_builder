define(function (require) {

    var $ = require('jquery'),
        mediator = require('Mediators/mediator'),
        marionette = require('marionette'),
        templates = require('templates'),
        reqres = require('reqres'),
        vent = require('vent'),
        commands = require('commands'),
        _ = require('underscore'),
        ENTER_KEY = 13;

    var editTourSceneView = function (options) {
        this.inheritedEvents = [];
        marionette.ItemView.call(this, options);
    };

    _.extend(editTourSceneView.prototype, marionette.ItemView.prototype, {
        tagName: 'a',
        className: 'list-group-item',
        template: templates.sceneTemplate,
        events: {
            'click': 'selectScene',
            'click .destroy': 'clear',
            'dblclick .sceneViewerHeader': 'editHeader',
            'dblclick .sceneViewerBody': 'editBody',
            'blur .sceneEditorHeader': 'closeHeader',
            'blur .sceneEditorBody': 'closeBody',
            'keypress .sceneHeader': 'updateHeaderOnEnter',
            'keypress .sceneDescription': 'updateBodyOnEnter',
            'drop': 'drop'
        },
        clear: function () {
            //alert("destroy called");
            var result = confirm("Want to delete?");
            if (result == true) {
                //Logic to delete the item
                //this.model.destroy();
                this.trigger('scene:delete', this.model);
            }
        },
        removeScene: function () {
            //this.trigger('scene:delete',this.model);
            this.model.destroy({wait: true});
        },//similar to clear but without the confirm prompt
        modelEvents: function () {
            var that = this;
            return {"change": that.render}; // equivalent to view.listenTo(view.model, "change:name", view.nameChanged, view)
        },
        onRender: function () {
            this.$inputHeader = this.$(".sceneHeader");
            this.$inputBody = this.$(".sceneDescription");
            if (this.model.selected) {
                this.$el.addClass('active');
            }
        },
        editHeader: function () {
            this.$('.tourHeader').addClass('editing');
            this.$inputHeader.focus();
            this.$inputHeader.select();
        },
        editBody: function () {
            this.$('.tourBody').addClass('editing');
            this.$inputBody.focus();
            this.$inputBody.select();
        },
        selectScene: function (e) {
            e.preventDefault();
            if (!this.model.selected) {
                this.trigger('set:selected:scene:model', this.model.toJSON());
                this.trigger('load:scene:settings', this.model.get('name'));
                this.trigger('call:action', this.model.get('name'));
                this.trigger('set:active:scene', this.model.get('name'));
            }
        },
        closeHeader: function () {
            var valueHeader = this.$inputHeader.val().trim();
            if (valueHeader) {

                function capitaliseFirstLetter(string) {
                    return string.charAt(0).toUpperCase() + string.slice(1);
                }

                this.trigger('scene:model:save', this.model, {title: valueHeader, description: capitaliseFirstLetter(valueHeader)});
            }

            this.$('.tourHeader').removeClass('editing');

        },
        closeBody: function () {
            var valueBody = this.$inputBody.val().trim();
            if (valueBody) {
                //this.model.save({description:valueBody});
                this.trigger('scene:model:save', this.model, {description: valueBody});

            }
            this.$('.tourBody').removeClass('editing');
        },
        updateHeaderOnEnter: function (e) {
            if (e.which === ENTER_KEY) {
                e.preventDefault();
                this.closeHeader();
                //this.render();
            }
        },
        updateBodyOnEnter: function (e) {
            if (e.which === ENTER_KEY) {
                e.preventDefault();
                this.closeBody();
                this.render();
            }
        },
        drop: function (event, index) {
            this.trigger('scene:dropped', index, this.model);
        }
    });
    editTourSceneView.extend = marionette.ItemView.extend;
    return editTourSceneView;
});
