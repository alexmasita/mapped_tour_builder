/**
 * Created by alexmasita on 10/8/13.
 */

define(function (require) {
    var ENTER_KEY = 13,
        listItemCompositeView = require('apps/edit_tour/views/listItemCompositeView'),
        editTourMenuItemView = require('apps/edit_tour/views/editTourMenuItemView'),
        vent = require('vent');

    var editTourMenuItemCompositeView = listItemCompositeView.extend({
        itemView: editTourMenuItemView,
        initializeChild: function () {
            this.listenTo(vent, 'menuSceneModel', this.renderItem);
        },
        renderItem: function (menuId) {
            this.children.each(function (view) {
                if (view.model.get('id') === menuId) {
                    view.model.fetch();
                }
            });
        },
        onRenderChild: function () {
            require(['jqueryui'], function () {
                $('.list-item').sortable({
                    stop: function (event, ui) {
                        //alert(ui.item.index());
                        ui.item.trigger('drop', ui.item.index());
                    }});
            });

        }


    });
    return editTourMenuItemCompositeView;
});