/**
 * Created by alexmasita on 11/6/13.
 */

define(function (require) {
    var listItemView = require('apps/edit_tour/views/listItemView'),
        templates = require('templates'),
        commands = require('commands'),
        reqres = require('reqres');
    var editTourFlyerItemView = listItemView.extend({
        clear: function () {
            //alert("destroy called");
            var result = confirm("Want to delete?");
            if (result == true) {
                //Logic to delete the item
                this.model.destroy();
                commands.execute('itemview:call:action', 'removehotspot(' + this.model.get('hotspotName') + ');');
                //commands.execute('itemview:call:action','for(set(i,0), i LT hotspot.count, inc(i), copy(hotspot[get(i)].keepbackup,hotspot[get(i)].keep);set(hotspot[get(i)].keep,true););reloadpano();for(set(i,0), i LT hotspot.count, inc(i), copy(hotspot[get(i)].keep,hotspot[get(i)].keepbackup););');
            }
        },
        template: templates.editTourFlyerTemplate,
        selectItem: function (e) {
            e.preventDefault();
            this.trigger('look:to:hotspot', this.model.get('sceneName'), this.model.get('hotspotName'));
            if (!this.model.selected) {
                //this.trigger('look:to:hotspot',this.model.get('sceneName'),this.model.get('hotspotName'));
                this.trigger('show:flyer:images', this.model, reqres.request('folderName'), reqres.request('filename'));
                //alert('alerted load flyer');
                //commands.execute('set:active:menu',this.model.get('id'));
            }
            this.trigger('set:active:flyer', this.model.get('id'));
        }
    });
    return editTourFlyerItemView;
});