/**
 * Created by alexmasita on 11/17/13.
 */

define(function (require) {
    'use strict';
    var marionette = require('marionette'),
        editTourFlyerImageItemView = require('apps/edit_tour/views/editTourFlyerImageItemView');
    var editTourFlyerImagesCollectionView = marionette.CollectionView.extend({
        itemView: editTourFlyerImageItemView,
        initialize:function(options){
            this.folderName = options.folderName;
            this.filename = options.filename;
            //alert(options.folderName);
        }
    });
    return editTourFlyerImagesCollectionView;
});