/**
 * Created by alexmasita on 10/21/13.
 */

define(function(require){

    var marionette = require('marionette'),
        templates = require('templates');

    var emptyItemView = marionette.ItemView.extend({
        tagName:'a',
        className:'list-group-item',
        template:templates.emptyItemTemplate
    });
    return emptyItemView;
});