/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/11/13
 * Time: 10:01 PM
 * To change this template use File | Settings | File Templates.
 * for help on events visit http://www.scottlogic.com/blog/2012/12/14/view-inheritance-in-backbone.html
 */

//var app = app || {};

define(function (require) {
    'use strict';
    var $ = require('jquery'),
        _ = require('underscore'),
        mediator = require('Mediators/mediator'),
        marionette = require('marionette'),
        templates = require('templates'),
        reqres = require('reqres'),
        vent = require('vent'),
        ENTER_KEY = 13,
        listItemView = function (options) {
            this.inheritedEvents = [];
            marionette.ItemView.call(this, options);
        };

    _.extend(listItemView.prototype, marionette.ItemView.prototype, {
        tagName: 'a',
        className: 'list-group-item',
        template: templates.listItemTemplate,
        /*modelEvents:{
         "change":"render"
         },*/
        edit: function (e) {
            e.preventDefault();
            e.stopPropagation();
            this.$('.viewWrapper').addClass('editing');
            this.$input.focus();
        },
        closeEdit: function () {
            var value = this.$input.val().trim();
            if (value) {
                this.model.save({text: value});
            }
            this.$('.viewWrapper').removeClass('editing');
        },
        updateOnEnter: function (e) {
            if (e.which === ENTER_KEY) {
                this.closeEdit();
            }
        },
        clear: function () {
            //alert("destroy called");
            var result = confirm("Want to delete?");
            if (result == true) {
                //Logic to delete the item
                var that = this;
                this.model.destroy();

            }
        },
        selectItem: function (e) {

            /*e.preventDefault();
             e.stopPropagation();*/
            /* if ($('.list-item').find('a').hasClass('active')){
             $('.list-item').find('a').removeClass('active editing');
             }*/
            //this.$el.addClass('active');
        },
        onRender: function () {
            this.$input = this.$(".list-input");
            if (this.model.selected) {
                this.$el.addClass('active');
                //alert(this.model.selected);
            }
        },
        onRenderChild: function () {
        },
        baseEvents: {
            'click': 'selectItem',
            'dblclick .viewItem': 'edit',
            'blur .editItem': 'closeEdit',
            'keypress .editItem': 'updateOnEnter',
            'click .destroy': 'clear',
            'drop': 'drop'
        },
        events: function () {
            var e = _.extend({}, this.baseEvents);
            _.forEach(this.inheritedEvents, function (events) {
                e = _.extend(e, events);
            });
            return e;
        },
        addEvents: function (eventObj) {
            this.inheritedEvents.push(eventObj);
        }
    });

    listItemView.extend = marionette.ItemView.extend;

    return listItemView;
});
