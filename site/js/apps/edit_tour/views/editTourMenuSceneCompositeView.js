/**
 * Created by alexmasita on 10/8/13.
 */

define(function(require){

    var marionette = require('marionette'),
        editTourMenuSceneView = require('apps/edit_tour/views/editTourMenuSceneView'),
        templates = require('templates'),
        mediator = require('Mediators/mediator'),
        vent = require('vent'),
        reqres = require('reqres');

    var editTourMenuSceneCompositeView = marionette.CompositeView.extend({
        itemView:editTourMenuSceneView,
        itemViewContainer:'#menu-scenes-list',
        template:templates.menuSceneContainerTemplate,
        events:{
            'click #btnAddSelectedScene': 'addMenuScene'
        },
        collectionEvents: {
            "add": "itemAdded" // equivalent to view.listenTo(view.collection, "add", view.itemAdded, view)
        },
        itemAdded:function(model){
            vent.trigger('menuSceneModel',model.get('menuId'));
        },
        newMenuSceneAttributes: function () {
            return {
                sceneName: reqres.request('selectedSceneModel').name,
                title: reqres.request('selectedSceneModel').title,
                load: reqres.request('selectedMenuItemModel').load,
                menuId: reqres.request('selectedMenuItemModel').id};
        },
        addMenuScene: function () {

            if (reqres.request('selectedSceneModel')) {
                var menuScenes = this.collection.where({sceneName:reqres.request('selectedSceneModel').name});

                if (menuScenes.length) {
                    alert("This scene has already been added");
                    return;
                } else {
                    //this.listenToOnce(this.collection, 'add', this.renderMenuScene);
                    this.collection.create(this.newMenuSceneAttributes(),{wait: true});
                }
            } else {
                alert("Please select a Scene");
            }
        }
    });
    return editTourMenuSceneCompositeView;
});