/**
 * Created by alexmasita on 11/17/13.
 */

define(function (require) {
    var listItemView = require('apps/edit_tour/views/listItemView'),
        templates = require('templates');
    var editTourImageItemView = listItemView.extend({
        template: templates.editTourImageTemplate,
        selectItem: function (e) {
            e.preventDefault();
            this.trigger('set:active:image', this.model.get('id'));
            if (!this.model.selected) {
                //this.trigger('load:menu:scenes', this.model, reqres.request('folderName'),reqres.request('filename'));
                //commands.execute('set:active:menu',this.model.get('id'));
            }
        },
        onRender: function () {
            if (this.model.selected) {
                this.$el.addClass('active');
                //alert(this.model.selected);
            }
        }
    });
    return editTourImageItemView;
});