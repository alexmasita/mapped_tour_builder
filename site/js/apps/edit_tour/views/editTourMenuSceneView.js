/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/16/13
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 */

//var app = app || {};

define(function (require) {

    var $ = require('jquery'),
        mediator = require('Mediators/mediator'),
        marionette = require('marionette'),
        templates = require('templates'),
        vent = require('vent');

    var editTourMenuSceneView = marionette.ItemView.extend({
        tagName: 'a',
        className: 'list-group-item',
        template: templates.menuSceneTemplate,
        events: {
            'click .destroy': 'clear',
            'click': 'select'

        },
        initialize: function () {
            /*
             this.listenTo(this.model,"change",this.render);
             this.listenTo(this.model,'destroy',this.remove);
             */
        },
        select: function () {
            if ($('#menu-scenes-list').find('a').hasClass('active')) {
                $('#menu-scenes-list').find('a').removeClass('active editing');
            }

            this.$el.addClass('active');
            vent.trigger('loadSceneSettings', this.model.get("sceneName"));
            this.trigger('call:action', this.model.get("sceneName"));

            /*mediator.models.krpano.call("loadscene('"+this.model.get("sceneName")+"',null,MERGE,BLEND(1));");
             mediator.models.krpano.call("pauseAutotour();");*/

        },
        clear: function () {
            //alert("destroy called");
            this.model.destroy();
        }
    });
    return editTourMenuSceneView;
});

