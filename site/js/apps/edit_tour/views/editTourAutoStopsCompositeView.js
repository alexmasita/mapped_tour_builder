/**
 * Created by alexmasita on 10/8/13.
 */


define(function (require) {
    var marionette = require('marionette'),
        editTourAutoStopsView = require('apps/edit_tour/views/editTourAutoStopsView'),
        templates = require('templates'),
        mediator = require('Mediators/mediator'),
        reqres = require('reqres'),
        $ = require('jquery');


    var editTourAutoStopsCompositeView = marionette.CompositeView.extend({
        itemView: editTourAutoStopsView,
        itemViewContainer: '#auto-view-stops-list',
        template: templates.autoViewStopContainerTemplate,
        events: {
            'click #btnAddSelectedAutoViewStop': 'addAutoViewStop'
        },
        collectionEvents: {
            'sync': 'syncedModel'
        },
        newAutoViewStopAttributes: function () {
            var selectedTween = this.$('input[name=options]:checked').val();
            var that = this;
            return {
                h: Math.round(reqres.request('tourPlayer').get("view.hlookat")),
                v: Math.round(reqres.request('tourPlayer').get("view.vlookat")),
                fov: Math.round(reqres.request('tourPlayer').get("view.fov")),
                tween: selectedTween,
                tweentime: that.$spinnerTime.spinner('value')
            }
        }, /*"easeOutExpo"*/
        syncedModel: function () {
            //alert('render called');
            this.render();
        },
        addAutoViewStop: function () {
            if (reqres.request('selectedAutoViewModel')) {
                this.collection.create(this.newAutoViewStopAttributes());
//                alert(JSON.stringify(this.newAutoViewStopAttributes()));
            } else {
                alert("Please select a view");
            }
        },
        onRender: function () {
            var that = this;
            require(['jqueryui'], function(){
                that.$spinnerTime = $("#spinnerTime").spinner();
                that.$spinnerTime.spinner('value', 4);
            });
        }
    });
    return editTourAutoStopsCompositeView;
});