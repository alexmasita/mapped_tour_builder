/**
 * Created by alexmasita on 11/19/13.
 */

define(function(require){
    var listItemCommandCompositeView = require('apps/edit_tour/views/listItemCommandCompositeView'),
        editTourNavView = require('apps/edit_tour/views/editTourNavItemView'),
        templates = require('templates');
    var editTourNavCompositeView = listItemCommandCompositeView.extend({
        itemView:editTourNavView,
        template:templates.editTourNavContainerTemplate,
        baseEvents:{
            'click .add-editor': 'addEditor',
            'click .remove-editor':'removeEditor',
            'click .add-highlight': 'addHighlight'
        },
        addEditor:function(){
            this.trigger('add:editor');
        },
        removeEditor:function(){
            this.trigger('remove:editor');
        },
        addHighlight:function(){
            this.trigger('add:highlight');
        }
    });
    return editTourNavCompositeView;
});