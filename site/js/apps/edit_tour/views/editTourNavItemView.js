/**
 * Created by alexmasita on 11/19/13.
 */

define(function (require) {
    var listItemView = require('apps/edit_tour/views/listItemView'),
        commands = require('commands'),
        templates = require('templates');
    var editTourNavItemView = listItemView.extend({
        clear: function () {
            //alert("destroy called");
            var result = confirm("Want to delete?");
            if (result == true) {
                //Logic to delete the item
                this.model.destroy();
                commands.execute('itemview:call:action', 'removehotspot(' + this.model.get('name') + ');');
                //commands.execute('itemview:call:action','for(set(i,0), i LT hotspot.count, inc(i), copy(hotspot[get(i)].keepbackup,hotspot[get(i)].keep);set(hotspot[get(i)].keep,true););reloadpano();for(set(i,0), i LT hotspot.count, inc(i), copy(hotspot[get(i)].keep,hotspot[get(i)].keepbackup););');
            }
        },
        template: templates.editTourNavTemplate,
        selectItem: function (e) {
            this.trigger('look:to:hotspot', this.model.get('sceneName'), this.model.get('name'));
        }

    });
    return editTourNavItemView;
});