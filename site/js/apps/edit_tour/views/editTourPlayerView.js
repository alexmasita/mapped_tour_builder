/**
 * Created by alexmasita on 9/28/13.
 */

define(function (require) {
    //'use strict';
    var $$ = require('jquery'),
        Backbone = require('backbone'),
        marionette = require('marionette'),
        Handlebars = require('handleBars'),
        editTourPlayerTemplate = require('text!templates/editTourPlayerTemplate.html'),
        mediator = require('Mediators/mediator'),
        reqres = require('reqres'),
        templates = require('templates'),
        editTourPlayerView = marionette.ItemView.extend({
            template: templates.editTourPlayerTemplate,
            /*        render:function(){
             this.$el.html(this.template(this.model.toJSON()));
             var that = this;
             require(['swfkrpano','krpanoiphone','krpanoiphone.license'],function(){
             embedpano({swf:  that.folderName + "/files/tour.swf", xml: that.folderName + "/files/tour.xml", html5: "auto", target: "pano"});
             mediator.models.krpano = document.getElementById("krpanoSWFObject");
             that.krpano = mediator.models.krpano;
             });
             return this;
             },*/
            onRender: function () {
                var that = this;
                require(['swfkrpano', 'krpanoiphone', 'krpanoiphone.license'], function () {
                    var connectedFileName = that.filename.split(' ').join('_');
                    embedpano({swf: that.folderName + "/files/tour.swf", xml: that.folderName + "/files/" + connectedFileName + "_tour.xml", html5: "auto", target: "pano"});
                    that.krpano = document.getElementById("krpanoSWFObject");

                    onLoadHandler = function () {
                        //Global variable declared in the main.js file.
                        //This event is called from krpano.
                        that.onLoadHandler();
                    };
                    onHotspotClick = function (name, parent, rotate, id) {
                        that.onHotspotClick(name, parent, rotate, id);
                    };
                    /*getHotspotCount = function(scenename,hotspotname,url,ath,atv,distorted,rx,ry,rz,width,height,scale,keep){
                     alert('Hotspot scenename = ' + scenename + ' hotspot name = ' + hotspotname + ' url = ' + url + ' keep ' + keep);
                     };*/
                });
            },
            onLoadHandler: function () {
                //alert('on load handler called');
            },
            onHotspotClick: function (hotspotname, parent, rotate, id) {
                var atv = this.krpanoPlayer().get(parent + '.atv'),
                    ath = this.krpanoPlayer().get(parent + '.ath'),
                    linkedscene = this.krpanoPlayer().get(parent + '.linkedscene'),
                    sceneName = this.krpanoPlayer().get('xml.scene'),
                    parentName = this.krpanoPlayer().get(parent + '.name'),
                    origId = this.krpanoPlayer().get(parent + '.id');


                this.trigger('nav:model:values', sceneName, hotspotname, atv, ath, linkedscene, rotate, parentName, origId);
                //alert('onHotspotClick called hotspotname ' + hotspotname + ' scenename ' +sceneName + ' atv ' +atv + ' ath ' +ath + ' linked ' +linkedscene + ' rotate ' +rotate);
                //alert('scene name = ' + sceneName + ' ' + ' linked scene = ' + linkedscene);
            },
            modelEvents: {
                'change': 'render'
            },
            krpanoPlayer: function () {
                return this.krpano;
            },
            initialize: function (options) {
                this.folderName = options.folderName;
                this.filename = options.filename;
                this.listenTo(this.model, 'change', this.render);
                this.render();
            }
        });
    return editTourPlayerView;
});
