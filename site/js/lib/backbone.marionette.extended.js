/**
 * Created by alexmasita on 10/5/13.
 */
define(function(require){
    var Marionette = require('marionette.original');
/*
    Marionette.TemplateCache.clear();
*/
    Marionette.TemplateCache.prototype.loadTemplate = function(templateText){
        var myTemplate = templateText;
        return myTemplate;
    };

    var Handlebars = require('handleBars');
    Marionette.TemplateCache.prototype.compileTemplate = function(rawTemplate) {
        return Handlebars.compile(rawTemplate);
    };

    return Marionette;

});