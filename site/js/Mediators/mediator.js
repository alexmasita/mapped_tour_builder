

define(function(require){

         var $ = require('jquery'),
             Backbone = require('backbone'),
             _ = require('underscore');

        var app = {};

        app.models = {};

        app.globalEvent = _.extend({}, Backbone.Events);

        return app;
});

