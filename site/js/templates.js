/**
 * Created by alexmasita on 10/1/13.
 */

define(function (require) {
    'use strict';
    return {
        MainViewLayoutTemplate: require('text!templates/MainViewLayoutTemplate.html'),
        MainViewMenuTemplate: require('text!templates/MainViewMenuTemplate.html'),
        UploadTourItemTemplate: require('text!templates/UploadTourItemTemplate.html'),
        UploadTourGroupItemTemplate: require('text!templates/UploadTourGroupItemTemplate.html'),
        UploadTourLibraryTemplate: require('text!templates/UploadTourLibraryTemplate.html'),
        UploadTourGroupLibraryTemplate: require('text!templates/UploadTourGroupLibraryTemplate.html'),
        editTourEditorViewTemplate: require('text!templates/editTourEditorViewTemplate.html'),
        editSceneSettingsTemplate: require('text!templates/editSceneSettingsTemplate.html'),
        sceneTemplate: require('text!templates/scene-template.html'),
        menuSceneTemplate: require('text!templates/menu-scene-template.html'),
        listItemTemplate: require('text!templates/list-item-template.html'),
        autoViewTemplate: require('text!templates/auto-view-template.html'),
        autoViewStopTemplate: require('text!templates/auto-view-stop-template.html'),
        listItemContainerTemplate: require('text!templates/list-item-container-template.html'),
        menuSceneContainerTemplate: require('text!templates/menu-scene-container-template.html'),
        autoViewContainerTemplate: require('text!templates/auto-view-container-template.html'),
        autoViewStopContainerTemplate: require('text!templates/auto-view-stop-container-template.html'),
        emptyItemTemplate: require('text!templates/empty-item-template.html'),
        editTourPlayerTemplate: require('text!templates/editTourPlayerTemplate.html'),
        editTourSceneContainerTemplate: require('text!templates/edit-tour-scene-container-template.html'),
        editTourFlyerTemplate: require('text!templates/edit-tour-flyer-template.html'),
        editTourFlyerPlayerTemplate: require('text!templates/edit-tour-flyer-player-template.html'),
        editTourFlyerImageTemplate: require('text!templates/edit-tour-flyer-image-template.html'),
        editTourFlyerContainerTemplate: require('text!templates/edit-tour-flyer-container-template.html'),
        editTourImageTemplate: require('text!templates/edit-tour-image-template.html'),
        editTourImageContainerTemplate: require('text!templates/edit-tour-image-container-template.html'),
        editTourNavContainerTemplate: require('text!templates/edit-tour-nav-container-template.html'),
        editTourNavTemplate: require('text!templates/edit-tour-nav-template.html'),
        loginViewTemplate: require('text!apps/authenticate/login/templates/login-view-template.html'),
        authenticateListViewContainerTemplate: require('text!apps/authenticate/list/templates/list-view-container-template.html'),
        authenticateListViewTemplate: require('text!apps/authenticate/list/templates/list-view-template.html'),
        NoneUserTemplate: require('text!apps/authenticate/list/templates/none.html')
    };
});