module.exports = function (app, fs, parser, application_root, path) {

    var cheerio = require('cheerio');
    var shortId = require('shortid');
    var check = require('check-types');
    var async = require('async');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();

    var deepCopy = function (prefix) {

    };

    app.get('/api/mini/:folder', function (request, response) {
        //console.log('Minitour Get Called : ' + request.params.folder);

        var selections = [];
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'app.xml');

        if (!(fs.existsSync(pathToFile))) {
            response.send(JSON.stringify([]));
            return;
        }

        fs.readFile(pathToFile,
            function (err, data) {
                if (!err) {
                    var $ = cheerio.load(data, {xmlMode: true});

                    var allSelections = $('selection');
                    allSelections.each(function (i, el) {
                        selections.push({

                            id: $(this).attr('id'),
                            text: $(this).attr('name'),
                            filename: $(this).attr('filename')
                        })
                    });
                    response.send(JSON.stringify(selections));
                } else {
                    console.log("Error Loading File: " + err);
                }

            });

    });

    app.get('/api/mini/:folder/:id', function (request, response) {
            //console.log('Mini Tour with id entered : ' + request.params.id);
            var baseName = path.basename(request.params.folder, '.zip');
            var pathToFile = path.join(application_root, 'server/tours', baseName, 'app.xml');

            if (!(fs.existsSync(pathToFile))) {
                response.send(JSON.stringify({}));
                return;
            }
            fs.readFile(pathToFile,
                function (err, data) {
                    if (!err) {
                        var $ = cheerio.load(data, {xmlMode: true});

                        var selNode = $('selection').filter(function () {
                            return $(this).attr('id') === request.params.id;
                        });

                        var selection = {
                            filename: selNode.attr('filename'),
                            text: selNode.attr('name'),
                            id: selNode.attr('id')
                        };
                        response.send(JSON.stringify(selection));
                    } else {
                        console.log("Error Loading File: " + err);
                    }
                });
        }
    );
    app.put('/api/mini/:folder/:id', function (request, response) {
        //console.log('mini tour put entered ' + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'app.xml');
        var basePath = path.join(application_root, 'server/tours', baseName);
        fs.readFile(pathToFile,
            function (err, data) {
                if (!err) {
                    var $ = cheerio.load(data, {xmlMode: true});

                    var selNode = $('selection').filter(function () {
                        return $(this).attr('id') === request.params.id;
                    });

                    if (selNode.length) {
                        var name = request.body.text,
                            id = request.body.id,
                            filename = request.body.filename;

                        async.parallel({
                            renamedIndexEntries: function (callback) {
                                var pathToIndex = path.join(basePath, filename.split(' ').join('_') + '.html');
                                fs.readFile(path.join(pathToIndex, 'index.html'),
                                    function (err, data) {
                                        if (!err) {
                                            var $ = cheerio.load(data);

                                            var $script = $('script');

                                            var selScript = $script.filter(function () {
                                                return (!$(this).attr('src'));
                                            });
                                            selScript.remove();
                                            $('#pano').append('<script>embedpano({swf:"files/tour.swf", xml:"files/' + name.split(' ').join('_') + '_tour.xml", html5:"auto", target:"pano"});</script>');
                                            atom.writeFile(path.join(pathToIndex, filename.split(' ').join('_') + '.html'), $.html(), function (err) {
                                                callback(err, true);
                                            });
                                        }
                                    });
                            },
                            renamedTourEntries: function (callback) {
                                var pathToFile = path.join(basePath, 'files', filename.split(' ').join('_') + '_tour.xml')
                                fs.readFile(pathToFile,
                                    function (err, data) {
                                        if (!err) {
                                            var $ = cheerio.load(data);
                                            var selNode = $('include').filter(function () {
                                                return $(this).attr('url') === "config.xml";
                                            });
                                            var selNodeInt = $('include').filter(function () {
                                                return $(this).attr('url') === "interface.xml";
                                            });

                                            selNode.attr('url', name.split(' ').join('_') + '_config.xml');
                                            selNodeInt.attr('url', name.split(' ').join('_') + '_interface.xml');
                                            atom.writeFile(path.join(pathToFile, 'files', filename.split(' ').join('_') + '_tour.xml'), $.xml(), function (err) {
                                                callback(err, true);
                                            });

                                        }
                                    });

                            },
                            renamedInterfaceEntries: function (callback) {
                                var pathToFile = path.join(basePath, 'files', filename.split(' ').join('_') + '_interface.xml');
                                fs.readFile(pathToFile,
                                    function (err, data) {
                                        if (!err) {
                                            var $ = cheerio.load(data, {xmlMode: true});
                                            var selNode = $('include').filter(function () {
                                                return $(this).attr('url') === 'swfaddress/swfaddress.xml';
                                            });

                                            selNode.attr('url', path.join('swfaddress', filename.split(' ').join('_') + '_swfaddress.xml'));
                                            atom.writeFile(pathToFile, $.xml(), function (err) {
                                                callback(err, true);
                                            });
                                        }
                                    });
                            }
                        }, function (error, results) {
                            if (results.renamedTourEntries === true && results.renamedInterfaceEntries === true && results.renamedIndexEntries === true) {
                                //console.log('minitour renames succeeded');
                                if (fs.existsSync(path.join(basePath, filename.split(' ').join('_') + '.html'))) {
                                    fs.renameSync(path.join(basePath, filename.split(' ').join('_') + '.html'), path.join(basePath, name.split(' ').join('_') + '.html'));
                                }
                                if (fs.existsSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_tour.xml'))) {
                                    fs.renameSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_tour.xml'), path.join(basePath, 'files', name.split(' ').join('_') + '_tour.xml'));
                                }
                                if (fs.existsSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_config.xml'))) {
                                    fs.renameSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_config.xml'), path.join(basePath, 'files', name.split(' ').join('_') + '_config.xml'));
                                }
                                if (fs.existsSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_interface.xml'))) {
                                    fs.renameSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_interface.xml'), path.join(basePath, 'files', name.split(' ').join('_') + '_interface.xml'));
                                }
                                if (fs.existsSync(path.join(basePath, 'files', 'swfaddress', filename.split(' ').join('_') + '_swfaddress.xml'))) {
                                    fs.renameSync(path.join(basePath, 'files', 'swfaddress', filename.split(' ').join('_') + '_swfaddress.xml'), path.join(basePath, 'files', 'swfaddress', name.split(' ').join('_') + '_swfaddress.xml'));
                                }
                                selNode.attr('filename', name.split(' ').join('_'));
                                selNode.attr('name', name);
                                selNode.attr('id', id);
                                atom.writeFile(pathToFile, $.xml(), function (err) {
                                    if (err) throw err;
                                    //console.log("Mini tour writes succeeded " + request.params.id);
                                    var selection = {
                                        filename: selNode.attr('filename'),
                                        text: selNode.attr('name'),
                                        id: selNode.attr('id')
                                    };
                                    response.send(JSON.stringify(selection));
                                });
                            }
                        });
                    }
                } else {
                    console.log("Error Loading File: " + err);
                }
            });
    });

    app.post('/api/mini/:folder', function (request, response) {
        console.log("Minitour post entered");
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'app.xml');
        var pathToIndex = path.join(application_root, 'server/tours', baseName);

        async.parallel({
                exists: function (callback) {
                    fs.exists(pathToFile,
                        function (exists) {
                            if (exists) {
                                fs.readFile(pathToFile,
                                    function (err, data) {
                                        callback(err, {appXml: data, exists: exists});
                                    });
                            } else {
                                callback(undefined, {exists: exists});
                            }
                        });
                },
                indexCopied: function (callback) {
                    var name = request.body.text,
                        filename = request.body.filename,
                        id = shortId.generate();

                    console.log('filename entered = ' + filename);
                    console.log('filename entered = ' + (filename ? filename + '.html' : 'index.html'));
                    fs.readFile(path.join(pathToIndex, filename ? filename + '.html' : 'index.html'),
                        function (err, data) {
                            if (!err) {
                                var $ = cheerio.load(data);

                                var $script = $('script');

                                var selScript = $script.filter(function () {
                                    return (!$(this).attr('src'));
                                });
                                selScript.remove();
                                $('#pano').append('<script>embedpano({swf:"files/tour.swf", xml:"files/' + name.split(' ').join('_') + '_tour.xml", html5:"auto", target:"pano"});</script>');
                                atom.writeFile(path.join(pathToIndex, name.split(' ').join('_') + '.html'), $.html(), function (err) {
                                    //callback(err, {copied: true, name: name, id: id});
                                    fs.readFile(path.join(pathToIndex, name.split(' ').join('_') + '.html'),
                                        function (err, data) {
                                            if (!err) {
                                                var $ = cheerio.load(data);
                                                var $style = $('style');
                                                $style.remove();
                                                $('head').append('<style>@media only screen and (min-width: 750px) { html { overflow:hidden; } a { display:none; }}@media screen and (max-width: 750px) {#pano {display:none;} }html { height:100%; }body { height:100%; overflow: hidden; margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#FFFFFF; background-color:#000000; }a{ color:#AAAAAA; text-decoration:underline; }a:hover{ color:#FFFFFF; text-decoration:underline; }</style>');
                                                $('body').append('<a href="' + name.split(' ').join('_') + '.html" target="_blank"><img src="play_tour.jpg" alt="Click Here To Play" width="100%" overflow: hidden;></a>');
                                                atom.writeFile(path.join(pathToIndex, name.split(' ').join('_') + '_Desk.html'), $.html(), function (err) {
                                                    callback(err, {copied: true, name: name, id: id});

                                                });

                                            }
                                        });
                                });

                            }
                        });
                    /*  fs.copy(path.join(pathToIndex, 'index.html'), path.join(pathToIndex, name + '.html'), function (err) {
                     callback(err, {copied: true, name: name, id: id});
                     });*/
                },
                tourCopied: function (callback) {
                    var name = request.body.text,
                        filename = request.body.filename,
                        id = shortId.generate();
                    //fs.readFile(path.join(pathToIndex, 'files', 'tour.xml'),
                    fs.readFile(path.join(pathToIndex, 'files', filename ? filename + '_tour.xml' : 'tour.xml'),
                        function (err, data) {
                            if (!err) {
                                var $ = cheerio.load(data);
                                var selNode = $('include').filter(function () {
                                    console.log('xml file referenced = '+filename?filename+"_config.xml":"config.xml");
                                    return $(this).attr('url') === (filename?filename+"_config.xml":"config.xml");
                                });
                                var selNodeInt = $('include').filter(function () {
                                    console.log('xml file referenced = '+filename?filename+"_interface.xml":"interface.xml");
                                    return $(this).attr('url') === (filename?filename+"_interface.xml":"interface.xml");
                                });
                                var selNodeIntMob = $('include').filter(function () {
                                    console.log('xml file referenced = '+filename?filename+"_interface_m.xml":"interface_m.xml");
                                    return $(this).attr('url') === (filename?filename+"_interface_m.xml":"interface_m.xml");
                                });

                                selNode.attr('url', name.split(' ').join('_') + '_config.xml');
                                selNodeInt.attr('url', name.split(' ').join('_') + '_interface.xml');
                                selNodeIntMob.attr('url', name.split(' ').join('_') + '_interface_m.xml');
                                atom.writeFile(path.join(pathToIndex, 'files', name.split(' ').join('_') + '_tour.xml'), $.xml(), function (err) {
                                    callback(err, {tourCopied: true, name: name, id: id});
                                });

                            }
                        });
                },
                configCopied: function (callback) {
                    var name = request.body.text,
                        filename = request.body.filename,
                        id = shortId.generate();

                    fs.copy(path.join(pathToIndex, 'files', filename ? filename + '_config.xml' : 'config.xml'), path.join(pathToIndex, 'files', name.split(' ').join('_') + '_config.xml'), function (err) {
                        callback(err, {configCopied: true, name: name, id: id});
                    });
                },
                interfaceCopied: function (callback) {
                    var name = request.body.text,
                        filename = request.body.filename,//will have value in a minitour is selected
                        id = shortId.generate();
                    fs.readFile(path.join(pathToIndex, 'files', filename ? filename + '_interface.xml' : 'interface.xml'),
                        function (err, data) {
                            if (!err) {
                                var $ = cheerio.load(data, {xmlMode: true});
                                var selNode = $('include').filter(function () {
                                    return $(this).attr('url') === (filename ? 'swfaddress/' + filename + '_swfaddress.xml' : 'swfaddress/swfaddress.xml');
                                });

                                selNode.attr('url', path.join('swfaddress', name.split(' ').join('_') + '_swfaddress.xml'));
                                atom.writeFile(path.join(pathToIndex, 'files', name.split(' ').join('_') + '_interface.xml'), $.xml(), function (err) {
                                    callback(err, {interfaceCopied: true, name: name, id: id});
                                });
                            }
                        });
                },
                interfaceMobCopied: function (callback) {
                    var name = request.body.text,
                        filename = request.body.filename,
                        id = shortId.generate();
                    fs.readFile(path.join(pathToIndex, 'files', filename ? filename + '_interface_m.xml' : 'interface_m.xml'),
                        function (err, data) {
                            if (!err) {
                                var $ = cheerio.load(data, {xmlMode: true});
                                var selNode = $('include').filter(function () {
                                    return $(this).attr('url') === (filename ? 'swfaddress/' + filename + '_swfaddress.xml' : 'swfaddress/swfaddress.xml');
                                });

                                selNode.attr('url', path.join('swfaddress', name.split(' ').join('_') + '_swfaddress.xml'));
                                atom.writeFile(path.join(pathToIndex, 'files', name.split(' ').join('_') + '_interface_m.xml'), $.xml(), function (err) {
                                    callback(err, {interfaceCopied: true, name: name, id: id});
                                });
                            }
                        });
                },
                swfAddressCopied: function (callback) {
                    var name = request.body.text,
                        filename = request.body.filename,
                        id = shortId.generate();
                    fs.copy(path.join(pathToIndex, 'files', 'swfaddress', filename ? filename + '_swfaddress.xml' : 'swfaddress.xml'), path.join(pathToIndex, 'files', 'swfaddress', name.split(' ').join('_') + '_swfaddress.xml'), function (err) {
                        callback(err, {swfAddressCopied: true, name: name, id: id});
                    });
                }
            },
            function (err, results) {
                console.log('app xml section global entered');
                if (!err) {
                    var fileExists = results.exists;
                    var copyDetails = results.indexCopied;
                    var $;
                    if (fileExists.exists) {
                        console.log('file exists called');
                        $ = cheerio.load(fileExists.appXml, {xmlMode: true})
                    } else {
                        console.log('file doesnt exists called');
                        $ = cheerio.load('<?xml version="1.0"?><mini><selections></selections><flyers></flyers></mini>', {xmlMode: true});
                    }

                    console.log('app xml attributes name = ' + copyDetails.name + ' filename =  ' + copyDetails.name.split(' ').join('_'));
                    var newSelNode = $("<selection></selection>");
                    newSelNode.attr('name', copyDetails.name);
                    newSelNode.attr('id', copyDetails.id);
                    newSelNode.attr('filename', copyDetails.name.split(' ').join('_'));
                    $("selections").append(newSelNode);
                    atom.writeFile(pathToFile, $.xml(), function (err) {
                        if (err) throw err;
                        console.log("This mini tour write succeeded" + request.params.text);
                        var selection = {
                            id: newSelNode.attr('id'),
                            text: newSelNode.attr('name'),
                            filename: newSelNode.attr('filename')
                        };
                        response.send(JSON.stringify(selection));
                    });
                } else {
                    console.log('Mini Tour Error + ' + err);
                }
            });
    });

    app.delete('/api/mini/:folder/:id', function (request, response) {
        console.log('Deleting MiniTour Item Entered: ' + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'app.xml');
        var basePath = path.join(application_root, 'server/tours', baseName);


        fs.readFile(pathToFile,
            function (err, data) {
                if (!err) {
                    var $ = cheerio.load(data, {xmlMode: true});

                    var selections = $('selection');
                    var selection = selections.filter(function () {
                        return $(this).attr('id') === request.params.id;
                    });

                    if (selection.length) {
                        var filename = selection.attr('filename');
                        console.log('delete text ' + filename);
                        if (fs.existsSync(path.join(basePath, filename.split(' ').join('_') + '.html'))) {
                            fs.unlinkSync(path.join(basePath, filename.split(' ').join('_') + '.html'));
                        }
                        if (fs.existsSync(path.join(basePath, filename.split(' ').join('_') + '_Desk.html'))) {
                            fs.unlinkSync(path.join(basePath, filename.split(' ').join('_') + '_Desk.html'));
                        }
                        if (fs.existsSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_tour.xml'))) {
                            fs.unlinkSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_tour.xml'));
                        }
                        if (fs.existsSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_config.xml'))) {
                            fs.unlinkSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_config.xml'));
                        }
                        if (fs.existsSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_interface.xml'))) {
                            fs.unlinkSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_interface.xml'));
                        }
                        if (fs.existsSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_interface_m.xml'))) {
                            fs.unlinkSync(path.join(basePath, 'files', filename.split(' ').join('_') + '_interface_m.xml'));
                        }
                        if (fs.existsSync(path.join(basePath, 'files', 'swfaddress', filename.split(' ').join('_') + '_swfaddress.xml'))) {
                            fs.unlinkSync(path.join(basePath, 'files', 'swfaddress', filename.split(' ').join('_') + '_swfaddress.xml'));
                        }
                        selection.remove();

                        atom.writeFile(pathToFile, $.xml(), function (err) {
                            if (err) throw err;
                            console.log("This delete write succeeded" + request.params.id);
                            var select = {
                                filename: selection.attr('filename'),
                                text: selection.attr('name'),
                                id: selection.attr('id')
                            };
                            response.send(JSON.stringify(select));
                        });
                    } else {
                        console.log("No Selection Item Deleted");
                    }
                } else {
                    console.log("Error Loading File: " + err);
                }
            });
    });


};