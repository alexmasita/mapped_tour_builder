/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/1/13
 * Time: 5:58 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/1/13
 * Time: 5:46 PM
 * To change this template use File | Settings | File Templates.
 */

// JavaScript Document

module.exports = function (app, fs, parser, application_root, path) {

    //var json2xml = require('../my_modules/json2xml').json2xml;

    var cheerio = require('cheerio');
    var check = require('check-types');
    var async = require('async');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();

    app.get('/edit/:folder/:filename', function (request, response) {
        var scenes = [];
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToTour = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');

        fs.readFile(pathToTour,
            function (err, data) {
                if (!err){
                    var $ = cheerio.load(data,{xmlMode:true});

                    $('scene').each(function(i,el){
                        scenes.push({
                            name:$(this).attr('name'),
                            title:$(this).attr('title')? $(this).attr('title').replace("[h1]","").replace("[/h1]",""):'',
                            description:$(this).attr('descripcion')? $(this).attr('descripcion').replace("[p]","").replace("[/p]",""):'',
                            folderName:request.params.folder
                        })
                    });
                    //console.log("What is scenes" + JSON.stringify(scenes));
                    response.send(JSON.stringify(scenes));
                } else {
                    console.log("Error Loading File: "+err);
                }

            });

    });

    app.get('/edit/:folder/:filename/:name', function (request, response) {
            var baseName = path.basename(request.params.folder, '.zip');

            var xmlData
            fs.readFile(path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml'),
                function (err, data) {
                    if (!err){
                        var $ = cheerio.load(data,{xmlMode:true});
                        var scene = {
                            name:$('scene').attr('name'),
                            title:$('scene').attr('title')? $('scene').attr('title').replace("[h1]","").replace("[/h1]",""):'',
                            description:$('scene').attr('descripcion')? $('scene').attr('descripcion').replace("[p]","").replace("[/p]",""):''}
                        response.send(scene);
                    } else {
                        console.log("Error Loading File: "+err);
                    }

                });


        }
    );
    app.put('/edit/:folder/:filename/:name', function (request, response) {
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile =path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToAddress = path.join(application_root, 'server/tours', baseName, 'files', 'swfaddress',request.params.filename + '_swfaddress.xml');

        async.parallel({
                tour: function(callback){
                    fs.readFile(pathToFile,
                        function (err, data) {
                            callback(err, data);
                        });

                },
                address: function(callback){
                    fs.readFile(pathToAddress,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            },
            function(err, results) {
                // results is now equals to: {one: 1, two: 2}
                if (!err){
                    var $tour = cheerio.load(results.tour,{xmlMode:true});
                    var $address = cheerio.load(results.address,{xmlMode:true});

                    var sceneNode = $tour('scene').filter(function(){
                        return $tour(this).attr('name') === request.params.name;
                    });

                    if(check.isNumber(request.body.index)){
                        console.log('The model has index of : ' + request.body.index);
                        sceneNode.remove();
                        if(request.body.index < $tour('scene').length){
                            $tour('scene').eq(request.body.index).before(sceneNode);

                        } else {
                            $tour('scene').last().after(sceneNode);
                        }

                    } else {
                        sceneNode.attr('name',request.body.name);
                        sceneNode.attr('title','[h1]'+request.body.title+'[/h1]');
                        sceneNode.attr('descripcion','[p]'+request.body.description+'[/p]');

                        //update the address file
                        var filteredAddressElements = $address('pano').filter(function(){
                            return $address(this).attr('scene') === sceneNode.attr('name');
                        });
                        if (filteredAddressElements.length >= 1){
                            filteredAddressElements.attr('pageurl','/' + request.body.title.split(' ').join('_'));
                            filteredAddressElements.attr('pagetitle',request.body.title);
                        } else {
                            var nodesPresent = $address('pano');
                            var panoNode = $address('<pano/>');
                            panoNode.attr('name','pano'+(nodesPresent.length + 1));
                            panoNode.attr('scene',sceneNode.attr('name'));
                            panoNode.attr('pageurl','/' + request.body.title.split(' ').join('_'));
                            panoNode.attr('pagetitle',request.body.title);
                            panoNode.attr('root','true');
                            //console.log($("data").length + ' ' + $("button").length + "Data and Button Values");
                            $address('plugin').append(panoNode);

                        }
                    }

                    async.parallel({
                            writeTour: function (callback) {
                                atom.writeFile(pathToFile, $tour.xml(), function (err) {
                                    callback(err, request.params.name);
                                });
                            },
                            writeAddress: function (callback) {
                                atom.writeFile(pathToAddress, $address.xml(), function (err) {
                                    callback(err, request.params.name);
                                });
                            }
                        },
                        function (err, results) {
                            // results is now equals to: {writeTour: 1, writeAddress: 2}
                            if (err) throw err;
                            console.log("This write succeeded both addr. and tour ");
                            var scene = {
                                name:sceneNode.attr('name'),
                                title:sceneNode.attr('title')? sceneNode.attr('title').replace("[h1]","").replace("[/h1]",""):'',
                                description:sceneNode.attr('descripcion')? sceneNode.attr('descripcion').replace("[p]","").replace("[/p]",""):''}
                            response.send(scene);
                        });


                } else {
                    console.log("Error Loading File: "+err);
                }
            });

        fs.readFile(pathToFile,
            function (err, data) {
                if (!err){
                    var $ = cheerio.load(data,{xmlMode:true});

                    var sceneNode = $('scene').filter(function(){
                        return $(this).attr('name') === request.params.name;
                    });

                    if(check.isNumber(request.body.index)){
                        //console.log('The model has index of : ' + request.body.index);
                        sceneNode.remove();
                        if(request.body.index < $('scene').length){
                            $('scene').eq(request.body.index).before(sceneNode);

                        } else {
                            $('scene').last().after(sceneNode);
                        }

                    } else {
                        sceneNode.attr('name',request.body.name);
                        sceneNode.attr('title','[h1]'+request.body.title+'[/h1]');
                        sceneNode.attr('descripcion','[p]'+request.body.description+'[/p]');
                    }

                    /*sceneNode.attr('name',request.body.name);
                    sceneNode.attr('title','[h1]'+request.body.title+'[/h1]');
                    sceneNode.attr('descripcion','[p]'+request.body.description+'[/p]');*/

                    atom.writeFile(pathToFile, $.xml(),function(err){
                        if (err) throw err;
                        console.log("This write succeeded" + request.params.name);
                        var scene = {
                            name:sceneNode.attr('name'),
                            title:sceneNode.attr('title')? sceneNode.attr('title').replace("[h1]","").replace("[/h1]",""):'',
                            description:sceneNode.attr('descripcion')? sceneNode.attr('descripcion').replace("[p]","").replace("[/p]",""):''}
                        response.send(scene);
                    });

                } else {
                    console.log("Error Loading File: "+err);
                }

            });
    });

    app.delete('/edit/:folder/:filename/:name', function (request, response) {
            var baseName = path.basename(request.params.folder, '.zip');
            var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');

        fs.readFile(pathToFile,function(err,data){
            var $ = cheerio.load(data,{xmlMode:true});

            var sceneNode = $('scene').filter(function(){
                return $(this).attr('name') === request.params.name;
            });
            sceneNode.remove();
            atom.writeFile(pathToFile, $.xml(),function(err){
                if (err) throw err;
                console.log("This delete scene write succeeded " + request.params.name);
                var scene = {
                    name:sceneNode.attr('name'),
                    title:sceneNode.attr('title')? sceneNode.attr('title').replace("[h1]","").replace("[/h1]",""):'',
                    description:sceneNode.attr('descripcion')? sceneNode.attr('descripcion').replace("[p]","").replace("[/p]",""):''}
                response.send(scene);
            });

        });
    });



    };