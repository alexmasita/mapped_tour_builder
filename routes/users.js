module.exports = function (app, path, fs, application_root, upload, mongoose, UserModel, passport, LocalStrategy, ensureAuthenticated, roles, _) {/*,ObjectId*/
    'use strict';
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });
    passport.deserializeUser(function (id, done) {
        UserModel.findById(id, function (err, user) {
            done(err, user);
        });
    });
    passport.use(new LocalStrategy(
        function (username, password, done) {
            UserModel.findOne({ username: username }, function (err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, { message: 'Incorrect username.' });
                }
                user.comparePassword(password, function (err, isMatch) {
                    if (err) {
                        throw err;
                    }
                    if (!isMatch) {
                        return done(null, false, { message: 'Incorrect password.' });
                    }
                    return done(null, user);
                });
            });
        }
    ));
    /*    function ensureAuthenticated(req, res, next) {
     if (req.isAuthenticated()) { return next(); }
     console.log('authentication failed');
     return res.json(401);
     }
     var roles = new ConnectRoles({
     failureHandler: function (req, res, action) {
     // optional function to customise code that runs when
     // user fails authorisation
     var accept = req.headers.accept || '';
     res.json(403);
     }
     });*/
    app.get('/api/users', roles.can('access admin'), function (request, response) {
        return UserModel
            .find({})
            .exec(function (err, users) {
                if (err) {
                    return console.log(users);
                }
                return response.send(users);
            });
    });
    app.get('/api/users/:id', function (request, response) {
        return UserModel.findById(request.params.id, function (err, user) {
            if (err) {
                return console.log(err);
            }
            return response.send(user);
        });
    });
    app.locals.saveUsersModel = function (request) {
        var user = new UserModel({
            firstname: request.body.firstname,
            lastname: request.body.lastname,
            email: request.body.email,
            icode: request.body.icode,
            username: request.body.username || request.body.email,
            password: request.body.password,
            role: request.body.role
        });
        user.save(function (err) {
            if (err) {
                return console.log(err);
            }
            return console.log('created new user entry');
        });
        return _.omit(user, ['password']);
    };
    app.post('/login', passport.authenticate('local'), function (request, response) {
        console.log('the user logged in successfully');
        return response.json(200, _.omit(request.user, ['password']));
    });
    app.get('/logout', function (req, res) {
        req.logout();
        return res.json(200);
    });
    app.post('/api/users', function (request, response) {
        console.log('the user save was called');
        return response.send(app.locals.saveUsersModel(request));
    });
    app.put('/api/users/:id', function (request, response) {
        console.log('updating tour ' + request.body.propertyName);
        return UserModel.findById(request.params.id, function (err, user) {
            user.firstname = request.body.firstname;
            user.lastname = request.body.lastname;
            user.email = request.body.email;
            user.icode = request.body.icode;
            user.username = request.body.username || request.body.email;
            user.password = request.body.password;
            user.role = request.body.role;
            return user.save(function (err) {
                if (!err) {
                    console.log('user updated');
                } else {
                    console.log(err);
                }
                UserModel
                    .find({_id: user._id})
                    .exec(function (err, user) {
                        if (err) {
                            return console.log(user);
                        }
                        return response.send(user);
                    });
            });
        });
    });
    app.get('/auth', function (request, response) {
        if (request.user) {
            console.log('the user logged in check was successful');
            return response.json(_.omit(request.user, ['password']));
        }
        console.log('the user has not logged in check was a failure');
        return response.json({ error: "Client has no valid login cookies."  });
    });
    app.post('/auth/login', passport.authenticate('local'), function (request, response) {
        console.log('the user logged in successfully');
        return response.json(_.omit(request.user, ['password']));
    });
    app.post('/auth/signup', function (request, response, next) {
        console.log('the user signup was called');
        request.login(app.locals.saveUsersModel(request), function (err) {
            if (err) {
                return next(err);
            }
            return response.json(_.omit(request.user, ['password']));
        });
    });
    app.post('/auth/logout', function (req, res) {
        req.logout();
        return res.json(200);
    });
    app.post('/auth/remove_account', function (request, response) {
        console.log('Deleting user with id = ' + request.user.id);
        if (request.user) {
            return UserModel.findById(request.user.id, function (err, user) {
                if (!user) {
                    return;
                }
                return user.remove(function (err) {
                    if (!err) {
                        console.log('user removed');
                    } else {
                        console.log(err);
                    }
                });
            });
        }

    });
    app.delete('/api/users/:id', function (request, response) {
        console.log('Deleting user with id:' + request.params.id);
        return UserModel.findById(request.params.id, function (err, user) {
            if (!user) {
                return;
            }
            return user.remove(function (err) {
                if (!err) {
                    console.log('user removed');
                } else {
                    console.log(err);
                }
            });
        });
    });
};