module.exports = function (app, fs, parser, application_root, path) {

    var cheerio = require('cheerio');
    var shortId = require('shortid');
    var check = require('check-types');
    var async = require('async');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();

    app.get('/api/navigation/:folder/:filename', function (request, response) {
        console.log("Navigation Getter entered with folder " + request.params.folder);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var collection = [];
        async.parallel({
                tour: function (callback) {
                    fs.readFile(pathToFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                if (!err) {
                    var $ = cheerio.load(results.tour, {xmlMode: true});
                    var getScenes = $('scene');
                    getScenes.each(function (i,elem) {
                        var sceneName = $(this);
                        $(this).find('hotspot').each(function(i,elem){
                            //console.log('navigation linkedscene value = ' + $(this).attr('linkedscene') + ' Full Object = ' + $(this));
                            //console.log('hotspot 1 keep = ' + $(this).attr('keep') + ' linkedscene = ' + $(this).attr('linkedscene'));
                            if ($(this).attr('linkedscene')||$(this).attr('linkedScene')/*!== undefined*/){
                                //console.log('hotspot 2');
                                collection.push({
                                    sceneName:sceneName.attr('name'),
                                    name:$(this).attr('name'),
                                    title:$(this).attr('title')?$(this).attr('title'):sceneName.attr('name'),
                                    ath:$(this).attr('ath'),
                                    atv:$(this).attr('atv'),
                                    linkedscene:$(this).attr('linkedscene'),
                                    rotate:$(this).attr('rotate'),
                                    parentName:$(this).attr('parentName'),
                                    id:$(this).attr('id')
                                });
                                console.log($(this).attr('id'));
                            }
                        });
                    });
                    console.log(collection.length);
                    response.send(collection);
                } else {
                    console.log("Error Loading File: " + err);
                }
            });
    });


    app.put('/api/navigation/:folder/:filename/:id', function (request, response) {
        console.log("navigation put entered: hotspot id " + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToAddressFile = path.join(application_root, 'server/tours', baseName, 'files','swfaddress', request.params.filename + '_swfaddress.xml');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml');
        var getMenu = null;


        async.parallel({
                tourFile: function (callback) {
                    fs.readFile(pathToFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            },
            function (err, results) {
                var $ = cheerio.load(results.tourFile, {xmlMode: true});

                var sceneName = request.body.sceneName;
                /*var scene = $('scene').filter(function(){
                    return $(this).attr('name') === request.body.sceneName;

                });*/
                /*var hotspotNode =  scene.find('hotspot').filter(function () {
                    return $(this).attr('name') === request.params.name;
                });*/
                var hotspotNode =  $('hotspot').filter(function () {
                    return $(this).attr('id') === request.params.id;
                });

                var scene = $('scene').filter(function(){
                    return $(this).attr('name').toLowerCase() === request.body.sceneName.toLowerCase();

                });
                var title = scene.attr('title');

                if (hotspotNode.length) {
                    console.log('navigation update section entered');
                    hotspotNode.attr('sceneName', request.body.sceneName);
                    hotspotNode.attr('title', title?title.replace("[h1]","").replace("[/h1]",""):request.body.sceneName);
                    hotspotNode.attr('name', request.body.name);
                    hotspotNode.attr('ath', request.body.ath);
                    hotspotNode.attr('atv', request.body.atv);
                    hotspotNode.attr('style', 'skin_hotspotstyle');
                    hotspotNode.attr('linkedscene', request.body.linkedscene);
                    hotspotNode.attr('rotate', request.body.rotate);
                    hotspotNode.attr('parentName',request.body.parentName);
                }/* else {
                    console.log('navigation addition section entered');

                    var newHotspotNode = $("<hotspot></hotspot>");
                    newHotspotNode.attr('sceneName', request.body.sceneName);
                    newHotspotNode.attr('title', title?title.replace("[h1]","").replace("[/h1]",""):request.body.sceneName);
                    newHotspotNode.attr('name', request.body.name);
                    newHotspotNode.attr('ath', request.body.ath);
                    newHotspotNode.attr('atv', request.body.atv);
                    newHotspotNode.attr('style', 'skin_hotspotstyle');
                    newHotspotNode.attr('linkedscene', request.body.linkedscene);
                    newHotspotNode.attr('rotate', request.body.rotate);
                    newHotspotNode.attr('parentName',request.body.parentName);
                    //console.log('navigation loop entered ' + title?title:request.body.sceneName);
                    scene.append(newHotspotNode);
                }*/

                async.parallel({
                        writeTour: function (callback) {
                            //console.log("value of atom : " + typeof atom.writeFile);
                            atom.writeFile(pathToFile, $.xml(), function (err) {
                                callback(err, request.body.name);
                            });
                        }
                    },
                    function (err, results) {
                        // results is now equals to: {writeTour: 1, writeConfig: 2}
                        var hotNode = request.body;
                        hotNode.title = hotspotNode.attr('title');
                        //console.log("This navigation put write succeeded " + JSON.stringify(hotNode));
                        response.send(JSON.stringify(hotNode));
                    });
            });

    });

    app.post('/api/navigation/:folder/:filename', function (request, response) {
        console.log("navigation post entered");
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToTourFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');

        async.parallel({
                tourFile:function(callback){
                    fs.readFile(pathToTourFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                var $ = cheerio.load(results.tourFile, {xmlMode: true});
                var sceneName = request.body.sceneName;
                var scene = $('scene').filter(function(){
                    return $(this).attr('name').toLowerCase() === request.body.sceneName.toLowerCase();

                });
                //this collection is only from specified scenename
                console.log('scene name for post = ' + sceneName);
                console.log('scene name count = ' + scene.length);
                var title = scene.attr('title');
                var newHotspotNode = $("<hotspot></hotspot>");
                newHotspotNode.attr('sceneName', request.body.sceneName);
                newHotspotNode.attr('title', title?title.replace("[h1]","").replace("[/h1]",""):request.body.sceneName);
                newHotspotNode.attr('name', request.body.name);
                newHotspotNode.attr('ath', request.body.ath);
                newHotspotNode.attr('atv', request.body.atv);
                newHotspotNode.attr('style', 'skin_hotspotstyle');
                newHotspotNode.attr('linkedscene', request.body.linkedscene);
                newHotspotNode.attr('rotate', request.body.rotate);
                newHotspotNode.attr('parentName',request.body.parentName);
                newHotspotNode.attr('id',shortId.generate());

                scene.append(newHotspotNode);
                //console.log('hotspotnodes entered');

                async.parallel({
                        writeTour: function (callback) {
                            atom.writeFile(pathToTourFile, $.xml(), function (err) {
                                callback(err, request.body);
                            });
                        }
                    },
                    function (err, results) {
                        console.log("Navigate post succeeded ");
                        var navRes = request.body;
                        navRes.id = newHotspotNode.attr('id');
                        console.log(navRes.id);
                        response.send(navRes);
                    });
            });

    });


    app.delete('/api/navigation/:folder/:filename/:id', function (request, response) {
        console.log('Deleting navigation Item Entered: ' + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');

        //var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', 'tour.xml');

        var pathToTourFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');

        async.parallel({
            tourFile:function(callback){
                fs.readFile(pathToTourFile,
                    function (err, data) {
                        callback(err, data);
                    });
            }

        },function(err,results){
            var $ = cheerio.load(results.tourFile, {xmlMode: true});

            /* var sceneName = request.body.sceneName;
            console.log('scene name ' + sceneName);
            var scene = $('scene').filter(function(){
                return $(this).attr('name') === request.body.sceneName;

            });
            var selectedHot =  scene.find('hotspot').filter(function () {
                return $(this).attr('name') === request.body.name;
            });*/
            var selectedHot = $('hotspot').filter(function () {
                return $(this).attr('id') === request.params.id;
            });
            selectedHot.remove();

            async.parallel({
                    writeTour: function (callback) {
                        atom.writeFile(pathToTourFile, $.xml(), function (err) {
                            callback(err, request.params.id);
                        });
                    }

                },
                function (err, results) {
                    // results is now equals to: {writeTour: 1, writeConfig: 2}
                    console.log("This navigation delete write succeeded" + results.writeTour);
                    var hotNode = request.body;
                    response.send(JSON.stringify(hotNode));
                });

        });
    });
};