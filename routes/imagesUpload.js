/**
 * Created by alexmasita on 12/3/13.
 */
module.exports = function (app, path, fs, application_root, upload) {

    var mkdirp = require('mkdirp');
    var util = require('util');
    /* upload.configure({
     imageVersions: {
     thumbnail: {
     width: 70,
     height: 70
     }
     }
     });*/
    app.use('/flyerupload', function (req, res, next) {
        console.log('flyer upload log entered = ' + req.param('folderName'));
        var baseName = req.param('folderName');//'stadia_hotel_hd';//path.basename(req.params.folder, '.zip');
        var pathToFolder = path.join(application_root, 'server/tours', baseName, 'files/flyers');

        var rmDir = function (dirPath) {
            try {
                var files = fs.readdirSync(dirPath);
            }
            catch (e) {
                return;
            }
            if (files.length > 0)
                for (var i = 0; i < files.length; i++) {
                    var filePath = dirPath + '/' + files[i];
                    if (fs.statSync(filePath).isFile()) {
                        fs.unlinkSync(filePath);

                    } else {
                        //rmDir(filePath); //When uncommented is drills down the folder structure for files.
                    }
                }
            //fs.rmdirSync(dirPath);
        };
        rmDir(pathToFolder);

        // imageVersions are taken from upload.configure()
        req.filemanager = upload.fileHandler({
            uploadDir: function () {
                return path.join(application_root, 'server/tours', baseName, 'files/flyers');//application_root + '/public/uploads/' + req.sessionID

            },
            uploadUrl: function () {
                return path.join(baseName, 'files/flyers');
            },
            imageVersions: {
                thumbnail: {
                    width: 70,
                    height: 70
                }
            }
        })(req, res, next);
        //next();
    });

    app.use('/videoupload', function (req, res, next) {
        console.log('video upload log entered = ' + req.param('folderName'));
        var baseName = req.param('folderName');//'stadia_hotel_hd';//path.basename(req.params.folder, '.zip');
        var pathToFolderFilm = path.join(application_root, 'server/tours', baseName, 'files/videos');
        if (!fs.existsSync(pathToFolderFilm)) {
            mkdirp.sync(pathToFolderFilm)
        }
        var rmDir = function (dirPath) {
            try {
                var files = fs.readdirSync(dirPath);
            }
            catch (e) {
                return;
            }
            if (files.length > 0)
                for (var i = 0; i < files.length; i++) {
                    var filePath = dirPath + '/' + files[i];
                    if (fs.statSync(filePath).isFile()) {
                        fs.unlinkSync(filePath);

                    } else {
                        //rmDir(filePath); //When uncommented is drills down the folder structure for files.
                    }
                }
            //fs.rmdirSync(dirPath);
        };
        rmDir(pathToFolderFilm);
        req.filemanager = upload.fileHandler({
            uploadDir: function () {
                return path.join(application_root, 'server/tours', baseName, 'files/videos');//application_root + '/public/uploads/' + req.sessionID
            },
            uploadUrl: function () {
                return path.join(baseName, 'files/videos');
            }
        })(req, res, next);
    });


};