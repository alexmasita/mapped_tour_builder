module.exports = function (app, fs, parser, application_root, path) {

    var cheerio = require('cheerio');
    var shortId = require('shortid');
    var check = require('check-types');
    var async = require('async');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();
    var getSlideActionText = function(hotspotName,sceneName){
        //return ("set(globalScene,get(xml.scene));set(" + hotspotName + "_val,%3);if(" + hotspotName + "_val == null, set(" + hotspotName + "_val, 1));delayedcall(2,  if(globalScene == " + sceneName + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val)););,  if(globalScene == " + sceneName.toLowerCase() + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val));););   );  );");
        return ("set(globalScene,get(xml.scene));set(" + hotspotName + "_val,%3);if(" + hotspotName + "_val == null, set(" + hotspotName + "_val, 1));delayedcall(2,  if(globalScene == " + sceneName + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0);set(hotspot[get(hotID" + hotspotName + ")].zorder, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].zorder, 99);tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val)););,  if(globalScene == " + sceneName.toLowerCase() + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val));););   );  );");
    };

    /*
     app.get('/api/applyimages/:folder/:filename', function (request, response) {
     console.log("Hotspot Getter entered with folder " + request.params.folder);
     var baseName = path.basename(request.params.folder, '.zip');
     var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
     var collection = [];
     async.parallel({
     tour: function (callback) {
     fs.readFile(pathToFile,
     function (err, data) {
     callback(err, data);
     });
     }
     },
     function (err, results) {
     // results is now equals to: {one: 1, two: 2}
     if (!err) {
     var $ = cheerio.load(results.tour, {xmlMode: true});
     var getScenes = $('scene');
     getScenes.each(function (i,elem) {
     var sceneName = $(this);
     $(this).find('hotspot').each(function(i,elem){
     console.log('hotspot linkedscene value = ' + $(this).attr('linkedscene'));
     if(!$(this).attr('linkedscene') && !$(this).attr('linkedScene')){
     collection.push({
     sceneName:sceneName.attr('name'),
     hotspotName:$(this).attr('name'),
     title:$(this).attr('title')?$(this).attr('title'):sceneName.attr('name'),
     url:$(this).attr('url'),
     ath:$(this).attr('ath'),
     atv:$(this).attr('atv'),
     distorted:$(this).attr('distorted'),
     rx:$(this).attr('rx'),
     ry:$(this).attr('ry'),
     rz:$(this).attr('rz'),
     width:$(this).attr('width'),
     height:$(this).attr('height'),
     scale:$(this).attr('scale'),
     keep:$(this).attr('keep'),
     id:$(this).attr('id')
     });
     }

     });
     });
     response.send(collection);
     } else {
     console.log("Error Loading File: " + err);
     }
     });
     });
     */
    app.get('/api/applyimages/:folder/:filename/:parent/:sceneName', function (request, response) {
        console.log("Apply Images Getter entered with folder " + request.params.folder + " " + request.params.parent);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var collection = [];
        async.parallel({
                tour: function (callback) {
                    fs.readFile(pathToFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                if (!err) {
                    var $ = cheerio.load(results.tour, {xmlMode: true});

                    var sceneName = request.body.sceneName;
                    var getScenes = $('scene').filter(function (i, elem) {
                        return $(this).attr('name') === request.params.sceneName;
                    });

                    getScenes.each(function (i, elem) {
                        var sceneName = $(this);
                        $(this).find('hotspot').each(function (i, elem) {
                            //console.log('Flyer images populating' + $(this).attr('parent'));

                            if (!$(this).attr('linkedscene') && !$(this).attr('linkedScene') && $(this).attr('parent') === 'hotspot[' + request.params.parent + ']') {

                                collection.push({
                                    sceneName: $(this).attr('sceneName'),
                                    hotspotName: $(this).attr('name'),
                                    url: path.join(request.params.folder, 'files/flyers/images', path.basename($(this).attr('url'))),
                                    filename: $(this).attr('filename'),
                                    parent: $(this).attr('parent'),
                                    isParent: 'false',
                                    id: $(this).attr('id')
                                });
                            }
                        });
                    });
                    response.send(JSON.stringify(collection));
                } else {
                    console.log("Error Loading File: " + err);
                }
            });
    });

    /*
     app.put('/api/applyimages/:folder/:filename/:id', function (request, response) {
     console.log("hotspot put entered: hotspot id " + request.params.id);
     var baseName = path.basename(request.params.folder, '.zip');
     var pathToAddressFile = path.join(application_root, 'server/tours', baseName, 'files','swfaddress', request.params.filename + '_swfaddress.xml');
     var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
     var pathToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml');
     var getMenu = null;


     async.parallel({
     tourFile: function (callback) {
     fs.readFile(pathToFile,
     function (err, data) {
     callback(err, data);
     });
     }
     },
     function (err, results) {
     var $ = cheerio.load(results.tourFile, {xmlMode: true});
     var hotspotNode = $('hotspot').filter(function () {
     return $(this).attr('id') === request.params.id;
     });

     if (hotspotNode.length) {
     //console.log('hotspotnodes entered');
     hotspotNode.attr('id', request.params.id);
     hotspotNode.attr('sceneName', request.params.sceneName);
     hotspotNode.attr('name', request.params.hotspotName);
     hotspotNode.attr('url', request.params.url);
     hotspotNode.attr('ath', request.params.ath);
     hotspotNode.attr('atv', request.params.atv);
     hotspotNode.attr('distorted', request.params.distorted);
     hotspotNode.attr('rx', request.params.rx);
     hotspotNode.attr('ry', request.params.ry);
     hotspotNode.attr('rz', request.params.rz);
     hotspotNode.attr('width', request.params.width);
     hotspotNode.attr('height', request.params.height);
     hotspotNode.attr('scale', request.params.scale);
     hotspotNode.attr('keep', request.params.keep);
     }

     async.parallel({
     writeTour: function (callback) {
     //console.log("value of atom : " + typeof atom.writeFile);
     atom.writeFile(pathToFile, $.xml(), function (err) {
     callback(err, request.params.id);
     });
     }
     },
     function (err, results) {
     // results is now equals to: {writeTour: 1, writeConfig: 2}
     console.log("This hotspot put write succeeded" + results.writeTour);
     var hotNode = {
     sceneName:hotspotNode.attr('sceneName'),
     hotspotName:hotspotNode.attr('name'),
     url:hotspotNode.attr('url'),
     ath:hotspotNode.attr('ath'),
     atv:hotspotNode.attr('atv'),
     distorted:hotspotNode.attr('distorted'),
     rx:hotspotNode.attr('rx'),
     ry:hotspotNode.attr('ry'),
     rz:hotspotNode.attr('rz'),
     width:hotspotNode.attr('width'),
     height:hotspotNode.attr('height'),
     scale:hotspotNode.attr('scale'),
     keep:hotspotNode.attr('keep'),
     id:hotspotNode.attr('id')
     };  //request.body.menuId
     response.send(hotNode);
     });
     });

     });
     */

    //In this post set the swf address to be the name of the first hotspot scene.
    app.post('/api/applyimages/:folder/:filename', function (request, response) {
        console.log("Apply Images Post Entered");
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToIndex = path.join(application_root, 'server/tours', baseName);
        var pathToTourFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToInterfaceFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_interface.xml');
        var pathToInterfaceMobFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_interface_m.xml');
        async.parallel({
                tourFile: function (callback) {
                    fs.readFile(pathToTourFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                interfaceFile: function (callback) {
                    fs.readFile(pathToInterfaceFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                interfaceMobFile: function (callback) {
                    if (!fs.existsSync(pathToInterfaceMobFile)) {
                        fs.readFile(path.join(pathToIndex, 'files', 'interface_m.xml'),
                            function (err, data) {
                                if (!err) {
                                    var $ = cheerio.load(data, {xmlMode: true});
                                    var selNode = $('include').filter(function () {
                                        return $(this).attr('url') === 'swfaddress/swfaddress.xml';
                                    });

                                    selNode.attr('url', path.join('swfaddress', request.params.filename.split(' ').join('_') + '_swfaddress.xml'));
                                    atom.writeFile(path.join(pathToIndex, 'files', request.params.filename.split(' ').join('_') + '_interface_m.xml'), $.xml(), function (err) {
                                        fs.readFile(pathToInterfaceMobFile,
                                            function (err, data) {
                                                callback(err, data);
                                            });
                                    });
                                }
                            });

                    } else {
                        fs.readFile(pathToInterfaceMobFile,
                            function (err, data) {
                                callback(err, data);
                            });
                    }
                }
            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                var $ = cheerio.load(results.tourFile, {xmlMode: true});
                var $interface = cheerio.load(results.interfaceFile, {xmlMode: true});
                var $interfaceMob = cheerio.load(results.interfaceMobFile, {xmlMode: true});

                var flyerCollection = request.body.flyerCollection;
                var imagesCollection = request.body.imagesCollection;
                var isReplace = request.body.isReplace;
                var isFilm = request.body.isFilm;//a boolean value in the body of a request retains its boolean status. A boolean on a query string becomes a string.


                /*console.log('Is Film Value = ' + isFilm);
                console.log('Flyer Collection = ' + JSON.stringify(flyerCollection));
                console.log('Images Collection = ' + JSON.stringify(imagesCollection));*/
                if (isFilm) {
                    //One ***************Remove Both Old Video and Flyer Child Hotspots.
                    //Two ***************Remove Onstart action
                    //Three *************Remove Interface action
                    //Four ***************Add New Video Hotspot

                    for (var key in flyerCollection) {
                        if (flyerCollection.hasOwnProperty(key)) {
                            var obj = flyerCollection[key];
                            var sceneName = obj['sceneName'];
                            var hotspotName = obj['hotspotName'];
                            var flyerUrl = obj['url']; //flyerUrl to become posterurl
                            var filScene = $('scene').filter(function () {
                                return $(this).attr('name') === sceneName;
                            });

                            //One ***************Remove Both Old Video and Flyer Child Hotspots.
                            filScene.find('hotspot').each(function (i, elem) {
                                if ($(this).attr('parent') === 'hotspot[' + hotspotName + ']') {
                                    $(this).remove();
                                }
                            });

                            //Two ***************Remove Onstart action
                            var onStartVal = filScene.attr('onstart');
                            var startIndex = onStartVal.indexOf('slide' + hotspotName + '_' + sceneName);
                            var newStartValue;
                            if (startIndex === -1) {
                                newStartValue = onStartVal;
                            } else {
                                var endIndex = onStartVal.indexOf(';', startIndex);
                                var stringToReplace = onStartVal.substring(startIndex, endIndex);
                                    newStartValue = onStartVal.replace(stringToReplace + ';', '');
                            }
                            filScene.attr('onstart', newStartValue);

                            //Three *************Remove Interface action
                                var actionNode = $interface('action').filter(function () {
                                    return $interface(this).attr('name') === ('slide' + hotspotName + '_' + sceneName);
                                });
                                actionNode.remove();
                                var actionIntNode = $interfaceMob('action').filter(function () {
                                    return $interfaceMob(this).attr('name') === ('slide' + hotspotName + '_' + sceneName);
                                });
                                actionIntNode.remove();

                            //Four ***************Add New Video Hotspot
                            var videoArray = [];
                            for (var key in imagesCollection) {
                                if (imagesCollection.hasOwnProperty(key)) {
                                    var objImage = imagesCollection[key];
                                    //console.log(JSON.stringify(objImage));
                                    var filename = path.basename(objImage['url']);
                                    videoArray.push('videos/ads/'+filename);
                                }
                            }
                            /*
                            var newHotspotNode = $("<hotspot></hotspot>");
                            newHotspotNode.attr('id', shortId.generate());
                            newHotspotNode.attr('name', hotspotName + '_' + imageCount);
                            newHotspotNode.attr('url', path.join('videos/ads/', path.basename(objImage['url'])));
                            newHotspotNode.attr('style', obj['style']);
                            newHotspotNode.attr('filename', path.basename(objImage['url']));
                            newHotspotNode.attr('parent', 'hotspot[' + hotspotName + ']');
                            newHotspotNode.attr('zorder', imageCount);
                            newHotspotNode.attr('alpha', 0);
                            filScene.append(newHotspotNode);
*/
                            var newHotspotNode = $("<hotspot></hotspot>");
                            newHotspotNode.attr('id', shortId.generate());
                            newHotspotNode.attr('name', hotspotName + '_' + 1);
                            newHotspotNode.attr('url', 'videoplayer.swf');
                            newHotspotNode.attr('alturl', 'videoplayer.js');
                            newHotspotNode.attr('videourl', videoArray.join('|'));
                            newHotspotNode.attr('posterurl', flyerUrl);
                            newHotspotNode.attr('loop', 'true');
                            newHotspotNode.attr('directionalsound', 'true');
                            newHotspotNode.attr('isvideo', 'true');
                            newHotspotNode.attr('style', obj['style']);
                            newHotspotNode.attr('filename', path.basename(objImage['url']));
                            newHotspotNode.attr('parent', 'hotspot[' + hotspotName + ']');
                            newHotspotNode.attr('zorder', 1);
                            newHotspotNode.attr('alpha', 1);
                            filScene.append(newHotspotNode);

                        }
                    }

                } else {
                    for (var key in flyerCollection) {
                        if (flyerCollection.hasOwnProperty(key)) {
                            var obj = flyerCollection[key];
                            var sceneName = obj['sceneName'];
                            var hotspotName = obj['hotspotName'];
                            var filScene = $('scene').filter(function () {
                                return $(this).attr('name') === sceneName;
                            });

                            var noImages = 0;
                            for (var key in imagesCollection) {
                                if (imagesCollection.hasOwnProperty(key)) {
                                    noImages++;
                                    //var objImage = imagesCollection[key];
                                }
                            }
                            if (isReplace) {//***********************************************
                                //one****** Add action if not existing on interface.xml and interface_m.xml
                                //two****** Change or update onstart value on tour.xml
                                //three****** Replace Hotspots on tour.xml
                                //console.log('is Replace entered');
                                //****** Add action if not existing on interface.xml and interface_m.xml
                                if (noImages > 2) {
                                    var actionNode = $interface('action').filter(function () {
                                        return $interface(this).attr('name') === 'slide' + hotspotName + '_' + sceneName;
                                    });
                                    if (actionNode.length === 0) {
                                        var newActionNode = $("<action></action>");
                                        newActionNode.attr('name', 'slide' + hotspotName + '_' + sceneName);
                                        newActionNode.text(getSlideActionText(hotspotName,sceneName));//"set(globalScene,get(xml.scene));set(" + hotspotName + "_val,%3);if(" + hotspotName + "_val == null, set(" + hotspotName + "_val, 1));delayedcall(2,  if(globalScene == " + sceneName + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val)););,  if(globalScene == " + sceneName.toLowerCase() + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val));););   );  );");
                                        $interface('krpano').append(newActionNode);
                                    }
                                    /**/var actionMobNode = $interfaceMob('action').filter(function () {
                                        return $interfaceMob(this).attr('name') === 'slide' + hotspotName + '_' + sceneName;
                                    });
                                    if (actionMobNode.length === 0) {
                                        var newActionNode = $("<action></action>");
                                        newActionNode.attr('name', 'slide' + hotspotName + '_' + sceneName);
                                        /*newActionNode.text("set(" + hotspotName + "_val,%3);if(" + hotspotName + "_val == null, set(" + hotspotName + "_val, 1));delayedcall(2,if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val));););");*/
                                        /*newActionNode.text("set(globalScene,get(xml.scene));set(" + hotspotName + "_val,%3);if(" + hotspotName + "_val == null, set(" + hotspotName + "_val, 1));delayedcall(2, if(globalScene == "+ sceneName+",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val)););););");*/
                                        newActionNode.text(getSlideActionText(hotspotName,sceneName));//"set(globalScene,get(xml.scene));set(" + hotspotName + "_val,%3);if(" + hotspotName + "_val == null, set(" + hotspotName + "_val, 1));delayedcall(2,  if(globalScene == " + sceneName + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val)););,  if(globalScene == " + sceneName.toLowerCase() + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val));););   );  );");
                                        $interfaceMob('krpano').append(newActionNode);
                                    }

                                } else {
                                    var actionNode = $interface('action').filter(function () {
                                        return $interface(this).attr('name') === 'slide' + hotspotName + '_' + sceneName;
                                    });
                                    actionNode.remove();
                                    var actionIntNode = $interfaceMob('action').filter(function () {
                                        return $interfaceMob(this).attr('name') === 'slide' + hotspotName + '_' + sceneName;
                                    });
                                    actionIntNode.remove();
                                }
                                //****** Change or update onstart value on tour.xml
                                var onStartVal = filScene.attr('onstart');
                                var startIndex = onStartVal.indexOf('slide' + hotspotName + '_' + sceneName);
                                var newStartValue;
                                if (startIndex === -1) {
                                    if (noImages > 2) {
                                        newStartValue = onStartVal + 'slide' + hotspotName + '_' + sceneName + '(' + obj['hotspotName'] + ',' + (noImages - 1) + ');';
                                    } else {
                                        newStartValue = onStartVal;
                                    }
                                } else {
                                    var endIndex = onStartVal.indexOf(';', startIndex);
                                    var stringToReplace = onStartVal.substring(startIndex, endIndex);
                                    if (noImages > 2) {
                                        newStartValue = onStartVal.replace(stringToReplace + ';', 'slide' + hotspotName + '_' + sceneName + '(' + obj['hotspotName'] + ',' + (noImages - 1) + ');');
                                    } else {
                                        newStartValue = onStartVal.replace(stringToReplace + ';', '');
                                    }
                                }
                                filScene.attr('onstart', newStartValue);

                                //****** Replace Hotspots on tour.xml
                                filScene.find('hotspot').each(function (i, elem) {
                                    if ($(this).attr('parent') === 'hotspot[' + hotspotName + ']') {
                                        $(this).remove();
                                    }
                                });
                                var imageCount = 1;
                                for (var key in imagesCollection) {
                                    if (imagesCollection.hasOwnProperty(key)) {
                                        var objImage = imagesCollection[key];
                                        //console.log(JSON.stringify(objImage));
                                        if (noImages === 1) {
                                            var hotspotNode = $('hotspot').filter(function () {
                                                return $(this).attr('id') === obj['id'];
                                            });

                                            if (hotspotNode.length) {
                                                hotspotNode.attr('id', obj['id']);
                                                hotspotNode.attr('name', obj['hotspotName']);
                                                hotspotNode.attr('url', path.join('flyers/images/', path.basename(objImage['url'])));
                                                hotspotNode.attr('style', obj['style']);
                                            }
                                        } else {
                                            var hotspotNode = $('hotspot').filter(function () {
                                                return $(this).attr('id') === obj['id'];
                                            });
                                            if (objImage['isParent'] === 'true') {
                                                if (hotspotNode.length) {
                                                    hotspotNode.attr('id', obj['id']);
                                                    hotspotNode.attr('name', obj['hotspotName']);
                                                    hotspotNode.attr('url', path.join('flyers/images/', path.basename(objImage['url'])));
                                                    hotspotNode.attr('style', obj['style']);
                                                }
                                            } else {
                                                var newHotspotNode = $("<hotspot></hotspot>");
                                                newHotspotNode.attr('id', shortId.generate());
                                                //newHotspotNode.attr('sceneName',sceneName);
                                                newHotspotNode.attr('name', hotspotName + '_' + imageCount);
                                                newHotspotNode.attr('url', path.join('flyers/images/', path.basename(objImage['url'])));
                                                if(objImage['webAddress']){
                                                    newHotspotNode.attr('onclick', 'openurl('+objImage['webAddress']+')');
                                                    newHotspotNode.attr('handcursor', 'true');
                                                }
                                                newHotspotNode.attr('style', obj['style']);
                                                newHotspotNode.attr('filename', path.basename(objImage['url']));
                                                newHotspotNode.attr('parent', 'hotspot[' + hotspotNode.attr('name') + ']');//consider using hotspotname variable
                                                newHotspotNode.attr('zorder', imageCount);
                                                if (imageCount > 1) {
                                                    newHotspotNode.attr('alpha', 0);
                                                }
                                                imageCount++;
                                                filScene.append(newHotspotNode);
                                            }
                                        }
                                    }
                                }
                            } else {
                                //****** Check number of images already added (after removing videos) and add noImages for newNoImages
                                filScene.find('hotspot').each(function (i, elem) {
                                    if (($(this).attr('parent') === 'hotspot[' + hotspotName + ']') && ($(this).attr('isvideo')==='true')) {
                                        $(this).remove();
                                    }
                                });

                                var noHotspots = filScene.find('hotspot').filter(function (i, elem) {
                                    return ($(this).attr('parent') === 'hotspot[' + hotspotName + ']');
                                });
                                var newNoImages = noImages + noHotspots.length;
                                //****** Add action if not existing on interface.xml and interface_m.xml
                                if (newNoImages > 1) {
                                    var actionNode = $interface('action').filter(function () {
                                        return $interface(this).attr('name') === 'slide' + hotspotName + '_' + sceneName;
                                    });
                                    if (actionNode.length === 0) {
                                        var newActionNode = $("<action></action>");
                                        newActionNode.attr('name', 'slide' + hotspotName + '_' + sceneName);
                                        newActionNode.text(getSlideActionText(hotspotName,sceneName));//"set(globalScene,get(xml.scene));set(" + hotspotName + "_val,%3);if(" + hotspotName + "_val == null, set(" + hotspotName + "_val, 1));delayedcall(2,  if(globalScene == " + sceneName + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val)););,  if(globalScene == " + sceneName.toLowerCase() + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val));););   );  );");
                                        $interface('krpano').append(newActionNode);
                                    }
                                    /**/var actionMobNode = $interfaceMob('action').filter(function () {
                                        return $interfaceMob(this).attr('name') === 'slide' + hotspotName + '_' + sceneName;
                                    });
                                    if (actionMobNode.length === 0) {
                                        var newActionNode = $("<action></action>");
                                        newActionNode.attr('name', 'slide' + hotspotName + '_' + sceneName);
                                        newActionNode.text(getSlideActionText(hotspotName,sceneName));//"set(globalScene,get(xml.scene));set(" + hotspotName + "_val,%3);if(" + hotspotName + "_val == null, set(" + hotspotName + "_val, 1));delayedcall(2,  if(globalScene == " + sceneName + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val)););,  if(globalScene == " + sceneName.toLowerCase() + ",if(get(" + hotspotName + "_val) != 0,txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));set(hotspot[get(hotID" + hotspotName + ")].alpha, 0));inc(" + hotspotName + "_val);if(" + hotspotName + "_val GT %2, set(" + hotspotName + "_val, 1););txtadd(hotID" + hotspotName + ", %1,'_',get(" + hotspotName + "_val));tween(hotspot[get(hotID" + hotspotName + ")].alpha, 1, 0.5, ,slide" + hotspotName + '_' + sceneName + "(%1,%2,get(" + hotspotName + "_val));););   );  );");
                                        $interfaceMob('krpano').append(newActionNode);
                                    }
                                } else {
                                    var actionNode = $interface('action').filter(function () {
                                        return $interface(this).attr('name') === 'slide' + hotspotName + '_' + sceneName;
                                    });
                                    actionNode.remove();
                                    var actionIntNode = $interfaceMob('action').filter(function () {
                                        return $interfaceMob(this).attr('name') === 'slide' + hotspotName + '_' + sceneName;
                                    });
                                    actionIntNode.remove();
                                }
                                //****** Change or update onstart value on tour.xml
                                var onStartVal = filScene.attr('onstart');
                                var startIndex = onStartVal.indexOf('slide' + hotspotName + '_' + sceneName);
                                var newStartValue;
                                if (startIndex === -1) {
                                    if (newNoImages > 1) {
                                        newStartValue = onStartVal + 'slide' + hotspotName + '_' + sceneName + '(' + obj['hotspotName'] + ',' + (newNoImages) + ');';
                                    } else {
                                        newStartValue = onStartVal;
                                    }
                                } else {
                                    var endIndex = onStartVal.indexOf(';', startIndex);
                                    var stringToReplace = onStartVal.substring(startIndex, endIndex);
                                    if (newNoImages > 1) {
                                        /*var sceneNode = $('scene').filter(function(i,elem){
                                         return $(this).attr('name') === sceneName;
                                         });*/
                                        newStartValue = onStartVal.replace(stringToReplace + ';', 'slide' + hotspotName + '_' + sceneName + '(' + obj['hotspotName'] + ',' + (newNoImages) + ');');
                                        /*if (onStartVal.indexOf('slide'+hotspotName+'('+obj['hotspotName']+','+(noImages-1)+');') === -1){
                                         sceneNode.attr('onstart',onStartVal+'slide'+hotspotName+'('+obj['hotspotName']+','+(noImages-1)+');');
                                         }*/
                                    } else {
                                        newStartValue = onStartVal.replace(stringToReplace + ';', '');
                                    }
                                }
                                filScene.attr('onstart', newStartValue);
                                //****** No Replacement of Hotspots but only video hotspots on tour.xml


                                var imageCount = 1 + noHotspots.length;
                                for (var key in imagesCollection) {
                                    if (imagesCollection.hasOwnProperty(key)) {
                                        var objImage = imagesCollection[key];
                                        //console.log(JSON.stringify(objImage));
                                        var newHotspotNode = $("<hotspot></hotspot>");
                                        newHotspotNode.attr('id', shortId.generate());
                                        //newHotspotNode.attr('sceneName',sceneName);
                                        newHotspotNode.attr('name', hotspotName + '_' + imageCount);
                                        newHotspotNode.attr('url', path.join('flyers/images/', path.basename(objImage['url'])));
                                        //console.log('value of web address = '+objImage['webAddress']);
                                        if(objImage['webAddress']){
                                            newHotspotNode.attr('onclick', 'openurl('+objImage['webAddress']+')');
                                            newHotspotNode.attr('handcursor', 'true');
                                        }
                                        newHotspotNode.attr('style', obj['style']);
                                        newHotspotNode.attr('filename', path.basename(objImage['url']));
                                        newHotspotNode.attr('parent', 'hotspot[' + hotspotName + ']');
                                        newHotspotNode.attr('zorder', imageCount);
                                        if (newNoImages > 1) {
                                            newHotspotNode.attr('alpha', 0);
                                        }
                                        imageCount++;
                                        filScene.append(newHotspotNode);
                                    }
                                }
                            }
                        }
                    }

                }

                async.parallel({
                        writeTour: function (callback) {
                            atom.writeFile(pathToTourFile, $.xml(), function (err) {
                                callback(err, request.body);
                            });
                        },
                        writeInterface: function (callback) {
                            atom.writeFile(pathToInterfaceFile, $interface.xml(), function (err) {
                                callback(err, request.body);
                            });
                        }/**/,
                        writeInterfaceMob: function (callback) {
                            atom.writeFile(pathToInterfaceMobFile, $interfaceMob.xml(), function (err) {
                                callback(err, request.body);
                            });
                        }
                    },
                    function (err, results) {
                        // results is now equals to: {writeTour: 1, writeConfig: 2}
                        console.log("Flyer bulk update succeeded ");
                        response.send(request.body);
                    });
            });
    });

    app.delete('/api/applyimages/:folder/:filename/:parent/:sceneName/:id', function (request, response) {
        console.log('Deleting apply images Entered: ' + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToIndex = path.join(application_root, 'server/tours', baseName);
        var pathToTourFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToInterfaceFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_interface.xml');
        var pathToInterfaceMobFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_interface_m.xml');

        async.parallel({
            tourFile: function (callback) {
                fs.readFile(pathToTourFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            interfaceFile: function (callback) {
                fs.readFile(pathToInterfaceFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            interfaceMobFile: function (callback) {
                if (!fs.existsSync(pathToInterfaceMobFile)) {
                    fs.readFile(path.join(pathToIndex, 'files', 'interface_m.xml'),
                        function (err, data) {
                            if (!err) {
                                var $ = cheerio.load(data, {xmlMode: true});
                                var selNode = $('include').filter(function () {
                                    return $(this).attr('url') === 'swfaddress/swfaddress.xml';
                                });

                                selNode.attr('url', path.join('swfaddress', request.params.filename.split(' ').join('_') + '_swfaddress.xml'));
                                atom.writeFile(path.join(pathToIndex, 'files', request.params.filename.split(' ').join('_') + '_interface_m.xml'), $.xml(), function (err) {
                                    fs.readFile(pathToInterfaceMobFile,
                                        function (err, data) {
                                            callback(err, data);
                                        });
                                });
                            }
                        });
                } else {
                    fs.readFile(pathToInterfaceMobFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            }
        }, function (err, results) {
            var $ = cheerio.load(results.tourFile, {xmlMode: true});
            var $interface = cheerio.load(results.interfaceFile, {xmlMode: true});
            var $interfaceMob = cheerio.load(results.interfaceMobFile, {xmlMode: true});

            //one****** remove Hotspot on tour.xml
            //two****** Change or update onstart value on tour.xml
            //three****** remove action if existing on interface.xml and interface_m.xml

            //****** remove Hotspot on tour.xml

            var selectedHot = $('hotspot').filter(function () {
                return $(this).attr('id') === request.params.id;
            });

            var parentAttr = selectedHot.attr('parent');

            var sceneNode = $('scene').filter(function (i, elem) {
                return $(this).attr('name') === request.params.sceneName;
            });
            var childHots = sceneNode.find('hotspot').filter(function (i, elem) {
                return $(this).attr('parent') === parentAttr;
            });
            selectedHot.remove();

            var newChildHots = sceneNode.find('hotspot').filter(function () {
                return $(this).attr('parent') === parentAttr;
            });

            var newCount = 1;
            newChildHots.each(function (i, elem) {
                $(this).attr('name', $(this).attr('parent').replace('hotspot[', '').replace(']', '') + '_' + newCount);
                console.log('hot name = ' + $(this).attr('name'));
                if (newCount === 1) {
                    $(this).attr('alpha', 1);
                }
                newCount++;
            });

            //****** Change or update onstart value on tour.xml
            var selScene = $('scene').filter(function () {
                return $(this).attr('name') === request.params.sceneName;
            });
            var onStartVal = selScene.attr('onstart');
            var startIndex = onStartVal.indexOf('slide' + parentAttr.replace('hotspot[', '').replace(']', '') + '_' + request.params.sceneName);
            var newStartValue;
            if (startIndex === -1) {
                newStartValue = onStartVal;
            } else {
                var endIndex = onStartVal.indexOf(';', startIndex);
                var stringToReplace = onStartVal.substring(startIndex, endIndex);
                if (childHots.length > 2) {
                    newStartValue = onStartVal.replace(stringToReplace + ';', 'slide' + parentAttr.replace('hotspot[', '').replace(']', '') + '_' + request.params.sceneName + '(' + selectedHot.attr('parent').replace('hotspot[', '').replace(']', '') + ',' + (childHots.length - 1) + ');');
                } else {
                    newStartValue = onStartVal.replace(stringToReplace + ';', '');
                }
            }
            selScene.attr('onstart', newStartValue);

            //****** remove action if existing on interface.xml and interface_m.xml
            var currentHots = sceneNode.find('hotspot').filter(function () {
                return $(this).attr('parent') === parentAttr;
            });
            if (currentHots.length < 2) {
                var actionNode = $interface('action').filter(function () {
                    return $interface(this).attr('name') === 'slide' + parentAttr.replace('hotspot[', '').replace(']', '') + '_' + request.params.sceneName;
                });
                actionNode.remove();
                var actionIntNode = $interfaceMob('action').filter(function () {
                    return $interfaceMob(this).attr('name') === 'slide' + parentAttr.replace('hotspot[', '').replace(']', '') + '_' + request.params.sceneName;
                });
                actionIntNode.remove();
            }
            async.parallel({
                    writeTour: function (callback) {
                        atom.writeFile(pathToTourFile, $.xml(), function (err) {
                            callback(err, request.params.id);
                        });
                    },
                    writeInterface: function (callback) {
                        atom.writeFile(pathToInterfaceFile, $interface.xml(), function (err) {
                            callback(err, request.body);
                        });
                    }/**/,
                    writeInterfaceMob: function (callback) {
                        atom.writeFile(pathToInterfaceMobFile, $interfaceMob.xml(), function (err) {
                            callback(err, request.body);
                        });
                    }
                },
                function (err, results) {
                    // results is now equals to: {writeTour: 1, writeConfig: 2}
                    console.log("This hotspot delete write succeeded  " + request.params.id);
                    var hotNode = {
                        sceneName: selectedHot.attr('sceneName'),
                        hotspotName: selectedHot.attr('name'),
                        url: selectedHot.attr('url'),
                        ath: selectedHot.attr('ath'),
                        atv: selectedHot.attr('atv'),
                        distorted: selectedHot.attr('distorted'),
                        rx: selectedHot.attr('rx'),
                        ry: selectedHot.attr('ry'),
                        rz: selectedHot.attr('rz'),
                        width: selectedHot.attr('width'),
                        height: selectedHot.attr('height'),
                        scale: selectedHot.attr('scale'),
                        keep: selectedHot.attr('keep'),
                        id: selectedHot.attr('id')
                    };
                    response.send(JSON.stringify(hotNode));
                });

        });
    });
};