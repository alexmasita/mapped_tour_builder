module.exports = function (app, express, path, application_root, passport, roles) {

    app.use(express.cookieParser());
    app.use(express.bodyParser());
    app.use(express.session({ secret: 'keyboard cat' }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(roles.middleware());
    app.use(express.methodOverride());
    app.use(express.csrf());
    app.use(function (req, res, next) {
        res.locals.csrfToken = req.session._csrf;
        next();
    });
    app.use(app.router);
    app.set('view engine', 'html');
    app.set('views',  application_root + '/site');
    app.engine('html', require('hbs').__express);
    app.use(express.static(path.join(application_root, 'site')));
    app.use(express.static(path.join(application_root, 'server', 'tours')));

    app.use(express.errorHandler({dumpExceptions: true, showStack: true}));
};