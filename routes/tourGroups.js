module.exports = function (app, path, fs, application_root, upload, mongoose, TourGroupModel) {
    'use strict';
    var db,
        TourGroup,
        saveTourGroupModel = function (request) {
            var tour = new TourGroupModel({
                groupName: request.body.groupName,
                groupDescription: request.body.groupDescription
            });
            tour.save(function (err) {
                if (err) {
                    return console.log(err);
                }
                return console.log('created new entry');
            });
            return tour;
        };


    app.get('/api/tourGroups', function (request, response) {
        return TourGroupModel.find(function (err, tourGroups) {
            if (err) {
                return console.log(tourGroups);
            }
            return response.send(tourGroups);
        });
    });

    app.post('/api/tourGroups', function (request, response) {
        console.log('the save tourGroup was called');
        return response.send(saveTourGroupModel(request));
    });

    app.get('/api/tourGroups/:id', function (request, response) {
        return TourGroupModel.findById(request.params.id, function (err, tour) {
            if (!err) {
                return console.log(err);
            }
            return response.send(tour);
        });
    });

    app.put('/api/tourGroups/:id', function (request, response) {
        console.log('updating tour ' + request.body.groupName);
        return TourGroupModel.findById(request.params.id, function (err, tour) {
            tour.groupName = request.body.groupName;
            tour.groupDescription = request.body.groupDescription;
            //tour.filename = request.body.filename;
            //tour.order = request.body.order;
            return tour.save(function (err) {
                if (!err) {
                    console.log('tour updated');
                } else {
                    console.log(err);
                }
                return response.send(tour);
            });
        });
    });

    app.delete('/api/tourGroups/:id', function (request, response) {
        console.log('Deleting tour with id:' + request.params.id);
        return TourGroupModel.findById(request.params.id, function (err, tour) {
            if (!tour) {
                return;
            }
            return tour.remove(function (err) {
                if (!err) {
                    console.log('Tour removed');
                } else {
                    console.log(err);
                }
            });
        });
    });


};