/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/1/13
 * Time: 5:46 PM
 * To change this template use File | Settings | File Templates.
 */

// JavaScript Document

module.exports = function (app, path, fs, application_root, upload, mongoose, TourModel) {/*,ObjectId*/
    'use strict';
    app.get('/api/tours', function (request, response) {
        var myObj = {};
        if (request.param('groupId')) {
            myObj._group = request.param('groupId');
        }
        return TourModel
            .find(myObj)
            .populate('_group')
            .exec(function (err, tours) {
                if (err) {
                    return console.log(tours);
                }
                return response.send(tours);
            });
    });


    app.get('/api/tours/group/:id', function (request, response) {
        //console.log('filter group entered ' + JSON.stringify(new ObjectId(request.params.id)));
        return TourModel
            .find({_group: (request.params.id)})//{_group: new ObjectId(request.params.id)}
            .populate('_group')
            .exec(function (err, tours) {
                if (err) {
                    return console.log(err);
                }
                return response.send(tours);
            });
    });



    app.locals.saveTourModel = function (request) {

        var tour = new TourModel({
            propertyName: request.body.propertyName,
            description: request.body.description,
            tourLink: request.body.tourLink,
            filename: path.basename(request.body.filename, '.zip'),
            order: request.body.order,
            latitude: request.body.latitude,
            longitude: request.body.longitude,
            _group: request.body._group
        });
        tour.save(function (err) {
            if (err) {
                return console.log(err);
            }
            return console.log('created new entry');
        });
        return tour;
    };


    app.post('/api/tours', function (request, response) {
        //console.log(request.files.count());
        console.log('the save was called');
        if (fs.existsSync(path.join(application_root, 'server/tours', request.body.filename))) {
            fs.unlinkSync(path.join(application_root, 'server/tours', request.body.filename));
        }

        var fileExt = path.extname(request.body.filename),
            AdmZip,
            zip;
        if (fileExt === '.zip') {
            console.log('tours post called for upload = ' + request.body.filename);

            AdmZip = require('adm-zip');
            zip = new AdmZip(path.join(application_root, 'server/files', request.body.filename));
            zip.extractAllTo(path.join(application_root, 'server/tours'), /*overwrite*/true);
            return response.send(app.locals.saveTourModel(request));
        }
        upload.fileManager().move(request.body.filename, '../tours', function (err, result) {
            if (err) {
                return response.send(err);
            }
            console.log('app locals save called for upload');
            return response.send(app.locals.saveTourModel(request));
        });
    });
    app.get('/api/tours/:id', function (request, response) {
        return TourModel.findById(request.params.id, function (err, tour) {
            if (err) {
                return console.log(err);
            }
            return response.send(tour);
        });
    });

    app.put('/api/tours/:id', function (request, response) {
        console.log('updating tour ' + request.body.propertyName);
        return TourModel.findById(request.params.id, function (err, tour) {
            tour.propertyName = request.body.propertyName;
            tour.description = request.body.description;
            tour.tourLink = request.body.tourLink;
            tour.latitude = request.body.latitude;
            tour.longitude = request.body.longitude;
            //request.body._group._id = mongoose.Schema.Types.ObjectId(request.body._group._id);
            //tour._group = request.body._group._id;
            tour._group = request.body._group._id;
            //console.log('Put value of body _group = ' + JSON.stringify(request.body._group));
            //console.log('Put value of body _group = ' + JSON.stringify(request.body._group._id));
            //tour.filename = request.body.filename;
            //tour.order = request.body.order;
            return tour.save(function (err) {
                if (!err) {
                    console.log('tour updated');
                } else {
                    console.log(err);
                }
                TourModel
                    .find({_id: tour._id})
                    .populate('_group')
                    .exec(function (err, tour) {
                        if (err) {
                            return console.log(tour);
                        }
                        return response.send(tour);
                    });
            });
        });
    });
    app.delete('/api/tours/:id', function (request, response) {
        console.log('Deleting tour with id:' + request.params.id);
        return TourModel.findById(request.params.id, function (err, tour) {
            if (!tour) {
                return;
            }
            return tour.remove(function (err) {
                if (!err) {
                    console.log('Tour removed');
                } else {
                    console.log(err);
                }
            });
        });
    });

};