

module.exports = function (app, fs, parser, application_root, path) {

    var cheerio = require('cheerio');
    var shortId = require('shortid');
    var check = require('check-types');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();

    app.get('/api/stops/:folder/:filename/:name', function (request, response) {
 //       console.log("api stops entered folder-name : " + request.params.folder + " name : " + request.params.name);
        var stops = [];
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToConfig = (path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml'));
        fs.readFile(pathToConfig,
            function (err, data) {
                if (!err){
                    var $ = cheerio.load(data,{xmlMode:true});

                    var selAuto = $('autoview').filter(function(){
                        return $(this).attr('name') === request.params.name;
                    });
                    //console.log("length is : "+selAuto.children('stop').length);

                    selAuto.children('stop').each(function(i,el){
                        stops.push({

                            name:$(this).attr('name'),
                            h:$(this).attr('h'),
                            v:$(this).attr('v'),
                            fov:$(this).attr('fov'),
                            tween:$(this).attr('tween'),
                            tweentime:$(this).attr('tweentime'),
                            id:$(this).attr('id')
                        })
                    });
                    console.log("stops object : "+JSON.stringify(stops));
                    response.send(JSON.stringify(stops));
                } else {
                    console.log("Error Loading File: "+err);
                }

            });

    });

  /*  app.get('/api/stops/:folder/:id', function (request, response) {
            var baseName = path.basename(request.params.folder, '.zip');
            var xmlData
            fs.readFile(path.join(application_root, 'server/tours', baseName, 'files', 'config.xml'),
                function (err, data) {
                    if (!err){
                        var $ = cheerio.load(data,{xmlMode:true});

                        var menuNode = $('button').filter(function(){
                            return $(this).attr('id') === request.params.id;
                        });

                        var menu = {
                            name:menuNode.attr('name').slice(4),
                            text:menuNode.attr('text'),
                            type:menuNode.attr('type'),
                            load:menuNode.attr('load'),
                            folderName:request.params.folder,
                            id:menuNode.attr('id')
                        }
                        response.send(menu);
                    } else {
                        console.log("Error Loading File: "+err);
                    }
                });
        }
    );*/


    app.post('/api/stops/:folder/:filename/:name', function (request, response) {
        console.log("Autoview stops entered" + request.params.folder + ' and ' + request.params.name);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile =path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml')
        fs.readFile(pathToFile,
            function (err, data) {
                if (!err){
                    var $ = cheerio.load(data,{xmlMode:true});

                    var stopForNode = $('autoview').filter(function(){
                        return $(this).attr('name')=== request.params.name;
                    });
                    var NodeStops = stopForNode.children('stop').length;
                    //console.log("no of stops" + NodeStops)
                    var newStopNode = $("<stop></stop>");
                    newStopNode.attr('name','stop'+(NodeStops+1));
                    newStopNode.attr('h',request.body.h);
                    newStopNode.attr('v',request.body.v);
                    newStopNode.attr('fov',request.body.fov);
                    newStopNode.attr('tween',request.body.tween);
                    newStopNode.attr('tweentime',request.body.tweentime);
                    newStopNode.attr('id',shortId.generate());

                    stopForNode.append(newStopNode);

                    atom.writeFile(pathToFile, $.xml(),function(err){
                        if (err) throw err;
                        console.log("This auto view write stop succeeded" + request.params.name);
                        var stopNode = {
                            name:newStopNode.attr('name'),
                            h:newStopNode.attr('h'),
                            v:newStopNode.attr('v'),
                            fov:newStopNode.attr('fov'),
                            tween:newStopNode.attr('tween'),
                            tweentime:newStopNode.attr('tweentime'),
                            id:newStopNode.attr('id')
                    };
                        //console.log("node post object string " + JSON.stringify(stopNode));
                        response.send(JSON.stringify(stopNode));
                    });

                } else {
                    console.log("Error Loading File: "+err);
                }

            });

    });

    app.delete('/api/stops/:folder/:filename/:name/:id', function (request, response) {
        console.log('Deleting Auto View Item Entered:name ' + request.params.name + ' id: ' + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile =path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml')
        fs.readFile(pathToFile,
            function (err, data) {
                if (!err){
                    var $ = cheerio.load(data,{xmlMode:true});

                    var selAutoView = $('autoview').filter(function(){
                        return $(this).attr('name')=== request.params.name;
                    });
                    var autoViewStopNode = selAutoView.children('stop').filter(function(){
                        return ($(this).attr('id') === request.params.id)|| $(this).attr('name') === request.params.id;
                    });

                    if (autoViewStopNode.length){
                        autoViewStopNode.remove();

                        selAutoView.children('stop').each(function(index){
                            //console.log();
                            $(this).attr('name','stop'+(index+1))
                        });

                        atom.writeFile(pathToFile, $.xml(),function(err){
                            if (err) throw err;
                            console.log("This write succeeded" + request.params.name);
                            var stopNode = {
                                name:autoViewStopNode.attr('name'),
                                h:autoViewStopNode.attr('h'),
                                v:autoViewStopNode.attr('v'),
                                fov:autoViewStopNode.attr('fov'),
                                tween:autoViewStopNode.attr('tween'),
                                tweentime:autoViewStopNode.attr('tweentime'),
                                id:autoViewStopNode.attr('id')
                            }
                            response.send(stopNode);
                        });
                    } else {
                        console.log("No Stop Item Deleted");
                    }
                } else {
                    console.log("Error Loading File: "+err);
                }
            });
    });


};