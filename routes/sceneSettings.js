/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/1/13
 * Time: 5:58 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/1/13
 * Time: 5:46 PM
 * To change this template use File | Settings | File Templates.
 */

// JavaScript Document

module.exports = function (app, fs, parser, application_root, path) {

    //var json2xml = require('../my_modules/json2xml').json2xml;

    var cheerio = require('cheerio');
    var check = require('check-types');
    var async = require('async');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();


    app.get('/api/sceneSettings/:folder/:filename/:name', function (request, response) {
            console.log('scene settings get entered with ' + request.params.folder + ' and ' + request.params.name);
            var baseName = path.basename(request.params.folder, '.zip');

            fs.readFile(path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml'),
                function (err, data) {
                    if (!err){
                        var $ = cheerio.load(data,{xmlNode:true});

                        var selScene = $('scene').filter(function(){
                            return $(this).attr('name')=== request.params.name;
                        });
                        var selView = selScene.first().find('view').first();

                        var settings = {
                            name:selScene.attr('name'),
                            lat:selScene.attr('lat'),
                            lng:selScene.attr('lng'),
                            hlookat:selView.attr('hlookat'),
                            vlookat:selView.attr('vlookat'),
                            fov:selView.attr('fov'),
                            maxpixelzoom:selView.attr('maxpixelzoom'),
                            fovmin:selView.attr('fovmin'),
                            fovmax:selView.attr('fovmax'),
                            hlookatmin:selView.attr('hlookatmin'),
                            hlookatmax:selView.attr('hlookatmax'),
                            vlookatmin:selView.attr('vlookatmin'),
                            vlookatmax:selView.attr('vlookatmax'),
                            limitview:selView.attr('limitview')

                        }
                        response.send(JSON.stringify(settings));
                    } else {
                        console.log("Error Loading File: "+err);
                    }

                });


        }
    );
    app.put('/api/sceneSettings/:folder/:filename/:name', function (request, response) {
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile =path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        //var pathToAddress = path.join(application_root, 'server/tours', baseName, 'files', 'swfaddress','swfaddress.xml');

        fs.readFile(path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml'),
            function (err, data) {
                if (!err){
                    var $tour = cheerio.load(data,{xmlNode:true});
                    //var $address = cheerio.load(results.address,{xmlNode:true});

                    var sceneNode = $tour('scene').filter(function(){
                        return $tour(this).attr('name') === request.params.name;
                    });

                    var viewNode = sceneNode.first().find('view').first();


                    sceneNode.attr('name',request.body.name);

                    if (request.body.allScenes){
                        console.log('all scenes entered');
                        $tour('scene').each(function(i,el){
                            $tour(this).attr('lat',request.body.lat);
                            $tour(this).attr('lng',request.body.lng);
                        });
                    } else {
                        sceneNode.attr('lat',request.body.lat);
                        sceneNode.attr('lng',request.body.lng);
                    }

                    viewNode.attr('hlookat',request.body.hlookat);
                    viewNode.attr('vlookat',request.body.vlookat);
                    viewNode.attr('fov',request.body.fov);
                    viewNode.attr('maxpixelzoom',request.body.maxpixelzoom);
                    viewNode.attr('fovmin',request.body.fovmin);
                    viewNode.attr('fovmax',request.body.fovmax);
                    if (!request.body.hlookatmin || !request.body.hlookatmax||!request.body.vlookatmin|| !request.body.vlookatmax) {
                        viewNode.removeAttr('hlookatmin');
                        viewNode.removeAttr('hlookatmax');
                        viewNode.removeAttr('vlookatmin');
                        viewNode.removeAttr('vlookatmax');
                        viewNode.attr('limitview','auto');
                    }else{
                        viewNode.attr('hlookatmin',request.body.hlookatmin);
                        viewNode.attr('hlookatmax',request.body.hlookatmax);
                        viewNode.attr('vlookatmin',request.body.vlookatmin);
                        viewNode.attr('vlookatmax',request.body.vlookatmax);
                        viewNode.attr('limitview','range');
                    }
                    atom.writeFile(pathToFile, $tour.xml(), function (err) {
                        if (err) throw err;
                        console.log("This write succeeded");
                        var settings = {
                            name:sceneNode.attr('name'),
                            lat:sceneNode.attr('lat'),
                            lng:viewNode.attr('lng'),
                            hlookat:viewNode.attr('hlookat'),
                            vlookat:viewNode.attr('vlookat'),
                            fov:viewNode.attr('fov'),
                            maxpixelzoom:viewNode.attr('maxpixelzoom'),
                            fovmin:viewNode.attr('fovmin'),
                            fovmax:viewNode.attr('fovmax'),
                            hlookatmin:viewNode.attr('hlookatmin'),
                            hlookatmax:viewNode.attr('hlookatmax'),
                            vlookatmin:viewNode.attr('vlookatmin'),
                            vlookatmax:viewNode.attr('vlookatmax'),
                            limitview:viewNode.attr('limitview')

                        }
                        response.send(JSON.stringify(settings));
                    });


                } else {
                    console.log("Error Loading File: "+err);
                }

            });


    });


};