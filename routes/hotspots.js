module.exports = function (app, fs, parser, application_root, path) {

    var cheerio = require('cheerio');
    var shortId = require('shortid');
    var check = require('check-types');
    var async = require('async');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();

    app.get('/api/hotspot/:folder/:filename', function (request, response) {
        //console.log("Hotspot Getter entered with folder " + request.params.folder);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToInterfaceFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_interface.xml');

        var collection = [];
        async.parallel({
                tour: function (callback) {
                    fs.readFile(pathToFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                interface: function (callback) {
                    fs.readFile(pathToInterfaceFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }

            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                if (!err) {
                    var $ = cheerio.load(results.tour, {xmlMode: true});
                    var $interface = cheerio.load(results.interface, {xmlMode: true});
                    var getScenes = $('scene');
                    getScenes.each(function (i,elem) {
                        var sceneName = $(this);
                        $(this).find('hotspot').each(function(i,elem){
                            //console.log('hotspot linkedscene value = ' + $(this).attr('linkedscene'));
                            if(!$(this).attr('linkedscene') && !$(this).attr('linkedScene') && !$(this).attr('parent')){
                                var hotspotName = $(this).attr('name');
                                //console.log('hotspot name = ' + hotspotName);
                                var getStyleNode = $interface('style').filter(function(){
                                    return $interface(this).attr('name')===hotspotName+'_style'+'_'+sceneName.attr('name');
                                });
                                collection.push({
                                    sceneName:sceneName.attr('name'),
                                    hotspotName:$(this).attr('name'),
                                    title:getStyleNode.attr('title')?getStyleNode.attr('title'):sceneName.attr('name'),
                                    url:$(this).attr('url'),
                                    style:$(this).attr('style'),
                                    parent:$(this).attr('parent'),
                                    zorder:$(this).attr('zorder'),
                                    ath:getStyleNode.attr('ath'),
                                    atv:getStyleNode.attr('atv'),
                                    distorted:getStyleNode.attr('distorted'),
                                    rx:getStyleNode.attr('rx'),
                                    ry:getStyleNode.attr('ry'),
                                    rz:getStyleNode.attr('rz'),
                                    width:getStyleNode.attr('width'),
                                    height:getStyleNode.attr('height'),
                                    scale:getStyleNode.attr('scale'),
                                    keep:getStyleNode.attr('keep'),
                                    id:$(this).attr('id')
                                });
                            }

                        });
                    });
                    response.send(collection);
                } else {
                    console.log("Error Loading File: " + err);
                }
            });
    });


    app.put('/api/hotspot/:folder/:filename/:id', function (request, response) {
        //console.log("hotspot put entered: hotspot id " + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToAddressFile = path.join(application_root, 'server/tours', baseName, 'files','swfaddress', request.params.filename + '_swfaddress.xml');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml');
        var getMenu = null;


        async.parallel({
                tourFile: function (callback) {
                    fs.readFile(pathToFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            },
            function (err, results) {
                var $ = cheerio.load(results.tourFile, {xmlMode: true});
                var hotspotNode = $('hotspot').filter(function () {
                    return $(this).attr('id') === request.params.id;
                });

                if (hotspotNode.length) {
                    //console.log('hotspotnodes entered');
                    hotspotNode.attr('id', request.params.id);
                    hotspotNode.attr('sceneName', request.params.sceneName);
                    hotspotNode.attr('name', request.params.hotspotName);
                    hotspotNode.attr('url', request.params.url);
                    hotspotNode.attr('ath', request.params.ath);
                    hotspotNode.attr('atv', request.params.atv);
                    hotspotNode.attr('distorted', request.params.distorted);
                    hotspotNode.attr('rx', request.params.rx);
                    hotspotNode.attr('ry', request.params.ry);
                    hotspotNode.attr('rz', request.params.rz);
                    hotspotNode.attr('width', request.params.width);
                    hotspotNode.attr('height', request.params.height);
                    hotspotNode.attr('scale', request.params.scale);
                    hotspotNode.attr('keep', request.params.keep);
                }

                async.parallel({
                        writeTour: function (callback) {
                            //console.log("value of atom : " + typeof atom.writeFile);
                            atom.writeFile(pathToFile, $.xml(), function (err) {
                                callback(err, request.params.id);
                            });
                        }
                    },
                    function (err, results) {
                        // results is now equals to: {writeTour: 1, writeConfig: 2}
                        //console.log("This hotspot put write succeeded" + results.writeTour);
                        var hotNode = {
                            sceneName:hotspotNode.attr('sceneName'),
                            hotspotName:hotspotNode.attr('name'),
                            url:hotspotNode.attr('url'),
                            ath:hotspotNode.attr('ath'),
                            atv:hotspotNode.attr('atv'),
                            distorted:hotspotNode.attr('distorted'),
                            rx:hotspotNode.attr('rx'),
                            ry:hotspotNode.attr('ry'),
                            rz:hotspotNode.attr('rz'),
                            width:hotspotNode.attr('width'),
                            height:hotspotNode.attr('height'),
                            scale:hotspotNode.attr('scale'),
                            keep:hotspotNode.attr('keep'),
                            id:hotspotNode.attr('id')
                        };  //request.body.menuId
                        response.send(hotNode);
                    });
            });

    });

    //In this post set the swf address to be the name of the first hotspot scene.
    app.post('/api/hotspot/:folder/:filename', function (request, response) {
        console.log("hotspots post update entered");
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToIndex = path.join(application_root, 'server/tours', baseName);
        var pathToTourFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToInterfaceFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_interface.xml');
        var pathToInterfaceMobFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_interface_m.xml');

        async.parallel({
                tourFile:function(callback){
                    fs.readFile(pathToTourFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                interfaceFile:function(callback){
                    fs.readFile(pathToInterfaceFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                interfaceMobFile:function(callback){
                    if (!fs.existsSync(pathToInterfaceMobFile)) {
                        console.log();
                        fs.readFile(path.join(pathToIndex, 'files', 'interface_m.xml'),
                            function (err, data) {
                                if (!err) {
                                    var $ = cheerio.load(data,{xmlMode:true});
                                    var selNode = $('include').filter(function () {
                                        return $(this).attr('url') === 'swfaddress/swfaddress.xml';
                                    });
                                    selNode.attr('url', path.join('swfaddress', request.params.filename.split(' ').join('_') + '_swfaddress.xml'));
                                    atom.writeFile(path.join(pathToIndex, 'files', request.params.filename.split(' ').join('_') + '_interface_m.xml'), $.xml(), function (err) {
                                        fs.readFile(pathToInterfaceMobFile,
                                            function (err, data) {
                                                callback(err, data);
                                            });
                                    });
                                }
                            });

                    }else{
                        fs.readFile(pathToInterfaceMobFile,
                            function (err, data) {
                                callback(err, data);
                            });
                    }
                }
            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                var $ = cheerio.load(results.tourFile, {xmlMode: true});
                var $interface = cheerio.load(results.interfaceFile, {xmlMode: true});
                var $interfaceMob = cheerio.load(results.interfaceMobFile, {xmlMode: true});

                var sceneName = request.body.sceneName;
                var scene = $('scene').filter(function(){
                    return $(this).attr('name') === request.body.sceneName;

                });
                //this collection is only from specified scenename
                var collection = request.body.collection;

                scene.find('hotspot').each(function(i,elem){
                    if(!$(this).attr('linkedscene') && !$(this).attr('linkedScene') && !$(this).attr('id')){
                        $(this).remove();
                    }
                });

                var title = scene.attr('title');

                for (var i = 0, len = collection.length; i < len; i++) {
                    var hots = collection[i];
                    //console.log('has linkedscene = ' + !!hots.linkedscene + ' And linkedScene = ' + !!hots.linkedScene);
                    if(!hots.linkedscene && !hots.linkedScene && !hots.id){
                        var newHotspotNode = $("<hotspot></hotspot>");
                        newHotspotNode.attr('id', shortId.generate());
                        newHotspotNode.attr('name',hots.hotspotName);
                        newHotspotNode.attr('url',path.join('graphics',hots.url));
                        newHotspotNode.attr('style',hots.hotspotName+'_style' + '_'+request.body.sceneName);
                        newHotspotNode.attr('zorder',0);
                        //console.log('hotspotnodes loop entered ' + title?title:hots.sceneName);

                        var sceneNode = $('scene').filter(function(i,elem){
                            return $(this).attr('name') === hots.sceneName;
                        });
                        //console.log('Scene Name = '+ sceneName + ' Node '+sceneNode);
                        sceneNode.append(newHotspotNode);

                        //setting style for interface.xml
                        var styleNode = $interface('style').filter(function(){
                            return $interface(this).attr('name')=== hots.hotspotName+'_style' + '_'+request.body.sceneName;
                        });
                        if(styleNode.length===0){
                            var newStyle = $interface("<style></style>");
                            newStyle.attr('name',hots.hotspotName+'_style' + '_'+request.body.sceneName);
                            newStyle.attr('sceneName',hots.sceneName);
                            newStyle.attr('title',title?title.replace("[h1]","").replace("[/h1]",""):hots.sceneName);
                            newStyle.attr('ath',hots.ath);
                            newStyle.attr('atv',hots.atv);
                            newStyle.attr('distorted',hots.distorted);
                            newStyle.attr('rx',hots.rx);
                            newStyle.attr('ry',hots.ry);
                            newStyle.attr('rz',hots.rz);
                            newStyle.attr('width',hots.width);
                            newStyle.attr('height',hots.height);
                            newStyle.attr('scale',hots.scale);
                            newStyle.attr('keep',hots.keep);
                            //console.log('hotspotnodes loop entered ' + title?title:hots.sceneName);

                            $interface('krpano').append(newStyle);
                        } else {
                            styleNode.attr('name',hots.hotspotName+'_style' + '_'+request.body.sceneName);
                            styleNode.attr('sceneName',hots.sceneName);
                            styleNode.attr('title',title?title.replace("[h1]","").replace("[/h1]",""):hots.sceneName);
                            styleNode.attr('ath',hots.ath);
                            styleNode.attr('atv',hots.atv);
                            styleNode.attr('distorted',hots.distorted);
                            styleNode.attr('rx',hots.rx);
                            styleNode.attr('ry',hots.ry);
                            styleNode.attr('rz',hots.rz);
                            styleNode.attr('width',hots.width);
                            styleNode.attr('height',hots.height);
                            styleNode.attr('scale',hots.scale);
                            styleNode.attr('keep',hots.keep);
                        }

                        //setting style for interface_m.xml
                        var styleMobNode = $interfaceMob('style').filter(function(){
                            return $interfaceMob(this).attr('name')=== hots.hotspotName+'_style' + '_'+request.body.sceneName;
                        });
                        if(styleMobNode.length===0){
                            var newMobStyle = $interfaceMob("<style></style>");
                            newMobStyle.attr('name',hots.hotspotName+'_style' + '_'+request.body.sceneName);
                            newMobStyle.attr('sceneName',hots.sceneName);
                            newMobStyle.attr('title',title?title.replace("[h1]","").replace("[/h1]",""):hots.sceneName);
                            newMobStyle.attr('ath',hots.ath);
                            newMobStyle.attr('atv',hots.atv);
                            newMobStyle.attr('distorted',hots.distorted);
                            newMobStyle.attr('rx',hots.rx);
                            newMobStyle.attr('ry',hots.ry);
                            newMobStyle.attr('rz',hots.rz);
                            newMobStyle.attr('width',hots.width);
                            newMobStyle.attr('height',hots.height);
                            newMobStyle.attr('scale',hots.scale);
                            newMobStyle.attr('keep',hots.keep);
                            //console.log('hotspotnodes loop entered ' + title?title:hots.sceneName);

                            $interfaceMob('krpano').append(newMobStyle);
                        } else {
                            styleMobNode.attr('name',hots.hotspotName+'_style' + '_'+request.body.sceneName);
                            styleMobNode.attr('sceneName',hots.sceneName);
                            styleMobNode.attr('title',title?title.replace("[h1]","").replace("[/h1]",""):hots.sceneName);
                            styleMobNode.attr('ath',hots.ath);
                            styleMobNode.attr('atv',hots.atv);
                            styleMobNode.attr('distorted',hots.distorted);
                            styleMobNode.attr('rx',hots.rx);
                            styleMobNode.attr('ry',hots.ry);
                            styleMobNode.attr('rz',hots.rz);
                            styleMobNode.attr('width',hots.width);
                            styleMobNode.attr('height',hots.height);
                            styleMobNode.attr('scale',hots.scale);
                            styleMobNode.attr('keep',hots.keep);
                        }
                    }
                }

                //console.log('hotspotnodes entered');

                async.parallel({
                        writeTour: function (callback) {
                            atom.writeFile(pathToTourFile, $.xml(), function (err) {
                                callback(err, request.body);
                            });
                        },
                        writeInterface: function (callback) {
                            atom.writeFile(pathToInterfaceFile, $interface.xml(), function (err) {
                                callback(err, request.body);
                            });
                        },
                        writeInterfaceMob: function (callback) {
                            atom.writeFile(pathToInterfaceMobFile, $interfaceMob.xml(), function (err) {
                                callback(err, request.body);
                            });
                        }
                    },
                    function (err, results) {
                        // results is now equals to: {writeTour: 1, writeConfig: 2}
                        //console.log("Hotspot bulk 2 update succeeded ");
                        /*var hotNode = {
                            sceneName:newHotspotNode.attr('sceneName'),
                            hotspotName:newHotspotNode.attr('name'),
                            url:newHotspotNode.attr('url'),
                            ath:newHotspotNode.attr('ath'),
                            atv:newHotspotNode.attr('atv'),
                            distorted:newHotspotNode.attr('distorted'),
                            rx:newHotspotNode.attr('rx'),
                            ry:newHotspotNode.attr('ry'),
                            rz:newHotspotNode.attr('rz'),
                            width:newHotspotNode.attr('width'),
                            height:newHotspotNode.attr('height'),
                            scale:newHotspotNode.attr('scale'),
                            keep:newHotspotNode.attr('keep'),
                            id:newHotspotNode.attr('id')
                        }; */

                        response.send(request.body);
                    });
            });
    });


    app.delete('/api/hotspot/:folder/:filename/:id', function (request, response) {
        //console.log('Deleting Menu Item Entered: ' + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');

        //var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', 'tour.xml');
        var pathToIndex = path.join(application_root, 'server/tours', baseName);
        var pathToTourFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToTourInterface = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_interface.xml');
        var pathToInterfaceMobFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_interface_m.xml');

        async.parallel({
            tourFile:function(callback){
                fs.readFile(pathToTourFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            interfaceFile:function(callback){
                fs.readFile(pathToTourInterface,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            interfaceMobFile:function(callback){
                if (!fs.existsSync(pathToInterfaceMobFile)) {
                    fs.readFile(path.join(pathToIndex, 'files', 'interface_m.xml'),
                        function (err, data) {
                            if (!err) {
                                var $ = cheerio.load(data,{xmlMode:true});
                                var selNode = $('include').filter(function () {
                                    return $(this).attr('url') === 'swfaddress/swfaddress.xml';
                                });

                                selNode.attr('url', path.join('swfaddress', request.params.filename.split(' ').join('_') + '_swfaddress.xml'));
                                atom.writeFile(path.join(pathToIndex, 'files', request.params.filename.split(' ').join('_') + '_interface_m.xml'), $.xml(), function (err) {
                                    fs.readFile(pathToInterfaceMobFile,
                                        function (err, data) {
                                            callback(err, data);
                                        });
                                });
                            }
                        });

                }else{
                    fs.readFile(pathToInterfaceMobFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            }


        },function(err,results){
            var $ = cheerio.load(results.tourFile, {xmlMode: true});
            var $interface = cheerio.load(results.interfaceFile, {xmlMode: true});
            var $interfaceMob = cheerio.load(results.interfaceMobFile, {xmlMode: true});


            //One ************* Delete flyer sub images
            //Two ************* Delete flyers
            //Three *********** Remove onstart action
            //Four ************ Remove style from interface files
            //Five ************ Remove action from interface files

            //One ************* Delete flyer sub images

            var selectedHot = $('hotspot').filter(function (i,elem) {
                return $(this).attr('id') === request.params.id;
            });
            var styleName = selectedHot.attr('style');
            var styleNode = $interface('style').filter(function(){
                return $interface(this).attr('name')=== styleName;
            });
            var sceneName = styleNode.attr('sceneName');
            var sceneNode = $('scene').filter(function(i,elem){
                return $(this).attr('name')===sceneName;
            });
            var parentAttr = 'hotspot['+selectedHot.attr('name')+']';
            var selectChildrenHot = sceneNode.find('hotspot').filter(function(i,elem){
                return $(this).attr('parent')===parentAttr;
            });
            selectChildrenHot.each(function(i,elem){
                $(this).remove();
            });

            //Two ************* Delete flyers
            selectedHot.remove();

            //Three *********** Remove onstart action

            //var styleName = selectedHot.attr('style');

            /*var styleNode = $interface('style').filter(function(){
                return $interface(this).attr('name') === styleName;
            });*///*******just removed
            /*var styleMobNode = $interfaceMob('style').filter(function(){
                return $interfaceMob(this).attr('name') === styleName;
            });*/
            if (styleNode.length){
                /*var sceneName = styleNode.attr('sceneName');
                var sceneNode = $('scene').filter(function(){
                    return $(this).attr('name') === sceneName;
                });*/ //*****Just removed

                var originalOnStartValue = sceneNode.attr('onstart');

                var startIndex = originalOnStartValue.indexOf('slide' + selectedHot.attr('name') + '_' + sceneName);
                var newStartValue;

                if (startIndex === -1) {
                    newStartValue = originalOnStartValue;
                } else {
                    var endIndex = originalOnStartValue.indexOf(';', startIndex);
                    var stringToReplace = originalOnStartValue.substring(startIndex, endIndex);
                    newStartValue = originalOnStartValue.replace(stringToReplace + ';', '');
                }
                sceneNode.attr('onstart',newStartValue);
            }

            //Four ************ Remove style from interface files

            /*var styleName = selectedHot.attr('style');
            var styleNodes = $interface('style').filter(function(){
                return $interface(this).attr('name')===styleName;
            });*///*******Just removed
            styleNode.each(function(i,elem){
                $interface(this).remove();
            });
            var styleMobNode = $interfaceMob('style').filter(function(){
                return $interfaceMob(this).attr('name')===styleName;
            });
            styleMobNode.each(function(i,elem){
                $interfaceMob(this).remove();
            });

            //Five ************ Remove action from interface files

            var actionNode = $interface('action').filter(function(){
                return $interface(this).attr('name') === ('slide' + selectedHot.attr('name') + '_' + sceneName);
            });
            actionNode.each(function(i,elem){
                $interface(this).remove();
            });
            var actionMobNode = $interfaceMob('action').filter(function(){
                return $interfaceMob(this).attr('name') === ('slide' + selectedHot.attr('name') + '_' + sceneName);
            });
            actionMobNode.each(function(i,elem){
                $interfaceMob(this).remove();
            });

            async.parallel({
                    writeTour: function (callback) {
                        atom.writeFile(pathToTourFile, $.xml(), function (err) {
                            callback(err, request.params.id);
                        });
                    },
                    writeInterface: function (callback) {
                        atom.writeFile(pathToTourInterface, $interface.xml(), function (err) {
                            callback(err, request.params.id);
                        });
                    },
                    writeInterfaceMob: function (callback) {
                        atom.writeFile(pathToInterfaceMobFile, $interfaceMob.xml(), function (err) {
                            callback(err, request.body);
                        });
                    }

                },
                function (err, results) {
                    // results is now equals to: {writeTour: 1, writeConfig: 2}
                    //console.log("This hotspot delete write succeeded  " + request.params.id);
                    var hotNode = {
                        sceneName:selectedHot.attr('sceneName'),
                        hotspotName:selectedHot.attr('name'),
                        url:selectedHot.attr('url'),
                        ath:selectedHot.attr('ath'),
                        atv:selectedHot.attr('atv'),
                        distorted:selectedHot.attr('distorted'),
                        rx:selectedHot.attr('rx'),
                        ry:selectedHot.attr('ry'),
                        rz:selectedHot.attr('rz'),
                        width:selectedHot.attr('width'),
                        height:selectedHot.attr('height'),
                        scale:selectedHot.attr('scale'),
                        keep:selectedHot.attr('keep'),
                        id:selectedHot.attr('id')
                    };
                    response.send(JSON.stringify(hotNode));
                });

        });
    });
};