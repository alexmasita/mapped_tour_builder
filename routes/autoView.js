module.exports = function (app, fs, parser, application_root, path) {

    var cheerio = require('cheerio');
    var check = require('check-types');
    var async = require('async');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();
    var getAllUpdatedXml = function($Address,$Config,$Tour){
        //********Reset first node of swf address
        var firstAutoViewName = $Config('autoview').first().attr('name');

        $Address('pano').each(function(i,elem){
            $Address(this).remove();
        });

        var getScene = $Tour('scene').filter(function(){
            return $Tour(this).attr('name') === firstAutoViewName;
        });

        if (getScene.length){
            var panoNode = $Address('<pano/>');
            panoNode.attr('name','pano1');
            panoNode.attr('scene',getScene.attr('name'));
            panoNode.attr('pageurl','/' + getScene.attr('title')?'/' + getScene.attr('title').split(' ').join('_').replace("[h1]","").replace("[/h1]",""):'/');
            panoNode.attr('pagetitle',getScene.attr('title')?getScene.attr('title').replace("[h1]","").replace("[/h1]",""):'/');
            panoNode.attr('root','true');
            $Address('plugin').prepend(panoNode);

            var index = 1;
            $Tour('scene').each(function(i,elem){
                if ($Tour(this).attr('name')!== firstAutoViewName){
                    var panoNode = $Address('<pano/>');
                    index = index + 1;
                    panoNode.attr('name','pano'+index);
                    panoNode.attr('scene',$Tour(this).attr('name'));
                    panoNode.attr('pageurl','/' + $Tour(this).attr('title')?'/' + $Tour(this).attr('title').split(' ').join('_').replace("[h1]","").replace("[/h1]",""):'/');
                    panoNode.attr('pagetitle',$Tour(this).attr('title')?$Tour(this).attr('title').replace("[h1]","").replace("[/h1]",""):'/');
                    panoNode.attr('root','true');
                    $Address('plugin').append(panoNode);
                }
            });
        }
        return {$Address:$Address,$Config:$Config,$Tour:$Tour};
    };

    app.get('/api/autoview/:folder/:filename', function (request, response) {
        console.log("Auto View Getter entered with folder " + request.params.folder);
        var autoViewResponse = [];
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml');


        async.parallel({
                tour: function (callback) {
                    fs.readFile(pathToFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                config: function (callback) {
                    fs.readFile(pathToConfigFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                if (!err) {
                    var $tour = cheerio.load(results.tour, {xmlMode: true});
                    var $config = cheerio.load(results.config, {xmlMode: true});

                    var getScenes = $config('autoview');
                    getScenes.each(function () {

                        var that = $config(this);
                        var selScene = $tour('scene').filter(function () {
                            return $tour(this).attr('name') === that.attr('name');
                        });
                        //console.log(selScene.length);
                        var myTitle = '';
                        if (selScene.length) {
                            myTitle =selScene && selScene.first().attr('title')? selScene.first().attr('title').replace("[h1]","").replace("[/h1]",""):'';
                        } else {
                            myTitle = 'Delete This'
                        }

                        autoViewResponse.push({
                            name: $config(this).attr('name'),
                            title: myTitle
                        });
                    });
                    //console.log("Title function entered " + JSON.stringify(autoViewResponse) );
                    response.send(JSON.stringify(autoViewResponse));
                } else {
                    console.log("Error Loading File: " + err);
                }
            });
    });


    app.put('/api/autoview/:folder/:filename/:id', function (request, response) {
        console.log("autoview put entered: autoview id " + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToAddressFile = path.join(application_root, 'server/tours', baseName, 'files','swfaddress', request.params.filename + '_swfaddress.xml');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml');
        var getMenu = null;


        async.parallel({
                addressFile: function (callback) {
                    fs.readFile(pathToAddressFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                tourFile: function (callback) {
                    fs.readFile(pathToFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                configFile: function (callback) {
                    fs.readFile(pathToConfigFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                var $Address = cheerio.load(results.addressFile, {xmlMode: true});
                var $Tour = cheerio.load(results.tourFile, {xmlMode: true});
                var $Config = cheerio.load(results.configFile, {xmlMode: true});

                var autoViewNodes = $Config('autoview').filter(function () {
                    return $Config(this).attr('name') === request.params.id;
                });

                var newAutoNode = null;
                if (!autoViewNodes.length) {
                    //console.log('autoviewnodes entered');
                    newAutoNode = $Config("<autoview></autoview>");
                    newAutoNode.attr('name', request.params.id);
                    newAutoNode.attr('title', function () {
                        var selScene = $Tour('scene').filter(function () {
                            return $Tour(this).attr('name') === request.params.id;
                        });
                        return selScene.attr('title');
                    });
                    $Config('krpano').append(newAutoNode);
                } else {
                    //console.log('autoview view failed to enter:');
                    if(check.isNumber(request.body.index)){
                        //console.log('The model has index of : ' + request.body.index);
                        if (!autoViewNodes.attr('title')){
                            autoViewNodes.attr('title',function(){
                                var selScene = $Tour('scene').filter(function () {
                                    return $Tour(this).attr('name') === request.params.id;
                                });
                                return selScene.attr('title');
                            });
                        }

                        newAutoNode = autoViewNodes;
                        autoViewNodes.remove();
                        if(request.body.index < $Config('autoview').length){
                            $Config('autoview').eq(request.body.index).before(autoViewNodes);

                        } else {
                            $Config('autoview').last().after(autoViewNodes);
                        }

                       /* $('button').each(function(index){
                            //console.log();
                            $(this).attr('name','item'+index)
                        });
*/
                    }
                }

                var AllXml = getAllUpdatedXml($Address,$Config,$Tour);
                console.log('All Xml for put passed');

                async.parallel({
                        writeTour: function (callback) {


                            //console.log("value of atom : " + typeof atom.writeFile);
                            atom.writeFile(pathToFile, AllXml.$Tour.xml(), function (err) {
                                callback(err, request.params.id);
                            });
                        },
                        writeConfig: function (callback) {
                            atom.writeFile(pathToConfigFile, AllXml.$Config.xml(), function (err) {
                                callback(err, request.params.id);
                            });
                        },
                        writeAddress:function(callback){
                            atom.writeFile(pathToAddressFile, AllXml.$Address.xml(), function (err) {
                                callback(err, request.params.id);
                            });
                        }
                    },
                    function (err, results) {
                        // results is now equals to: {writeTour: 1, writeConfig: 2}
                        console.log("This write succeeded for Tour and Test " + results.writeTour);
                        var autoScene = {
                            name: newAutoNode ? newAutoNode.attr('name') : '',
                            title: newAutoNode && newAutoNode.attr('title') ? newAutoNode.attr('title').replace("[h1]", "").replace("[/h1]", "") : ''
                        };  //request.body.menuId
                        response.send(autoScene);
                    });
            });

    });

    //In this post set the swf address to be the name of the first autoview scene.

    app.post('/api/autoview/:folder/:filename', function (request, response) {
        console.log("autoview bulk post entered");
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToAddressFile = path.join(application_root, 'server/tours', baseName, 'files','swfaddress', request.params.filename + '_swfaddress.xml');
        var pathToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml');
        var pathToTourFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');


        async.parallel({
                addressFile: function (callback) {
                    fs.readFile(pathToAddressFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                configFile: function (callback) {
                    fs.readFile(pathToConfigFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                tourFile:function(callback){
                    fs.readFile(pathToTourFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                var $Address = cheerio.load(results.addressFile, {xmlMode: true});
                var $Config = cheerio.load(results.configFile, {xmlMode: true});
                var $Tour = cheerio.load(results.tourFile, {xmlMode: true});
                //console.log(JSON.stringify(request.body.length));
                for (var i = 0, len = request.body.length; i < len; i++) {
                    var scene = request.body[i];
                    var scenesInConfig = $Config('autoview').filter(function () {
                        return $Config(this).attr('name') === scene.name;
                    });
                   // console.log("No of existing scenes : " + scenesInConfig.length);
                    if (!scenesInConfig.length) {
                        var newAutoNode = $Config("<autoview></autoview>");
                        newAutoNode.attr('name', scene.name);
                        newAutoNode.attr('title', scene.title);
                        $Config('krpano').append(newAutoNode);
                        //console.log("node entered");
                    }
                }
                //********Reset first node of swf address

                /*var firstAutoViewName = $Config('autoview').first().attr('name');

                var getAddressNodes = $Address('pano').filter(function(){
                    return $Address(this).attr('scene')===firstAutoViewName;
                });
                console.log('No of address nodes' + getAddressNodes.length + ' panos length ' + $Address('pano').length);
                if (getAddressNodes.length){
                    getAddressNodes.remove();
                }

                var getScene = $Tour('scene').filter(function(){
                    return $Tour(this).attr('name') === firstAutoViewName;
                });

                var panoNode = $Address('<pano/>');
                panoNode.attr('name','pano1');
                panoNode.attr('scene',getScene.attr('name'));
                panoNode.attr('pageurl','/' + getScene.attr('title')?'/' + getScene.attr('title').split(' ').join('_').replace("[h1]","").replace("[/h1]",""):'/');
                panoNode.attr('pagetitle',getScene.attr('title')?getScene.attr('title').replace("[h1]","").replace("[/h1]",""):'/');
                panoNode.attr('root','true');
                $Address('plugin').prepend(panoNode);

                $Address('pano').each(function(index){
                    //console.log();
                    $Address(this).attr('name','pano'+(index+1));
                });*/

                //********Reset first node of swf address

                var AllXml = getAllUpdatedXml($Address,$Config,$Tour);
                console.log('All Xml for post passed');


                async.parallel({
                        writeTour: function (callback) {
                            atom.writeFile(pathToTourFile, AllXml.$Tour.xml(), function (err) {
                                callback(err, request.body);
                            });
                        },
                        writeConfig: function (callback) {
                            atom.writeFile(pathToConfigFile, AllXml.$Config.xml(), function (err) {
                                callback(err, request.body);
                            });
                        },
                        writeAddress:function(callback){
                            atom.writeFile(pathToAddressFile, AllXml.$Address.xml(), function (err) {
                                callback(err, request.body);
                            });
                        }
                    },
                    function (err, results) {
                        // results is now equals to: {writeTour: 1, writeConfig: 2}
                        console.log("Bulk Scene address update succeeded ");
                         //request.body.menuId
                        response.send(request.body);
                    });
            });

    });


    app.delete('/api/autoview/:folder/:filename/:id', function (request, response) {
        console.log('Deleting Menu Item Entered: ' + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');

        //var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', 'tour.xml');

        var pathToAddressFile = path.join(application_root, 'server/tours', baseName, 'files','swfaddress', request.params.filename + '_swfaddress.xml');
        var pathToTourFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml');

        async.parallel({
            addressFile: function (callback) {
                fs.readFile(pathToAddressFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            configFile: function (callback) {
                fs.readFile(pathToConfigFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            tourFile:function(callback){
                fs.readFile(pathToTourFile,
                    function (err, data) {
                        callback(err, data);
                    });
            }

        },function(err,results){
            var $Address = cheerio.load(results.addressFile, {xmlMode: true});
            var $Config = cheerio.load(results.configFile, {xmlMode: true});
            var $Tour = cheerio.load(results.tourFile, {xmlMode: true});

            //var $ = cheerio.load(data, {xmlMode: true});
            var selectedAuto = $Config('autoview').filter(function () {
                return $Config(this).attr('name') === request.params.id;
            });
            selectedAuto.remove();

            var AllXml = getAllUpdatedXml($Address,$Config,$Tour);
            console.log('All Xml for delete passed');

            async.parallel({
                    writeTour: function (callback) {
                        atom.writeFile(pathToTourFile, AllXml.$Tour.xml(), function (err) {
                            callback(err, request.params.id);
                        });
                    },
                    writeConfig: function (callback) {
                        atom.writeFile(pathToConfigFile, AllXml.$Config.xml(), function (err) {
                            callback(err, request.params.id);
                        });
                    },
                    writeAddress:function(callback){
                        atom.writeFile(pathToAddressFile, AllXml.$Address.xml(), function (err) {
                            callback(err, request.params.id);
                        });
                    }
                },
                function (err, results) {
                    // results is now equals to: {writeTour: 1, writeConfig: 2}
                    console.log("This autoview delete write succeeded  " + request.params.id);
                    var autoView = {
                        name: selectedAuto.attr('name'),
                        title: selectedAuto.attr('title')
                    };
                    response.send(JSON.stringify(autoView));
                });

        });


        /*fs.readFile(pathToConfigFile,
            function (err, data) {
                if (!err) {
                    var $ = cheerio.load(data, {xmlMode: true});
                    var selectedAuto = $('autoview').filter(function () {
                        return $(this).attr('name') === request.params.id;
                    });
                    selectedAuto.remove();

                    atom.writeFile(pathToConfigFile, $.xml(), function (err) {
                        if (err) throw err;
                        console.log("This write succeeded  " + request.params.id);
                        var autoView = {
                            name: selectedAuto.attr('name').slice(4),
                            title: selectedAuto.attr('title')
                        };
                        response.send(autoView);
                    });
                } else {
                    Console.log("error : " + err);
                }
            });*/
    });
};