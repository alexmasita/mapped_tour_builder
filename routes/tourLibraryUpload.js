/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/1/13
 * Time: 5:46 PM
 * To change this template use File | Settings | File Templates.
 */

// JavaScript Document

module.exports = function (app, path, fs, application_root, upload) {

    /*upload.configure({
     uploadDir: path.join(application_root, 'server/files'),
     uploadUrl: '/files'
     });*/

    app.use('/upload', function (req, res, next) {
        console.log('upload log entered');
        var rmDir = function (dirPath) {
            try {
                var files = fs.readdirSync(dirPath);
            }
            catch (e) {
                return;
            }
            if (files.length > 0)
                for (var i = 0; i < files.length; i++) {
                    var filePath = dirPath + '/' + files[i];
                    if (fs.statSync(filePath).isFile()) {
                        fs.unlinkSync(filePath);
                        console.log('if entered');
                    } else {
                        rmDir(filePath);
                        console.log('else entered');
                    }
                }
            fs.rmdirSync(dirPath);
        };

        rmDir(path.join(application_root, 'server/files'));
        upload.fileHandler({
            uploadDir: function () {
                return path.join(application_root, 'server/files');
            },
            uploadUrl: function () {
                return '/files'
            }
        })(req, res, next);
    });

};