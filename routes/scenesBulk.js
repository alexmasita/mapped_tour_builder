module.exports = function (app,fs,parser,application_root,path) {


    var cheerio = require('cheerio');
    var check = require('check-types');
    var async = require('async');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();
    var getAllUpdatedXml = function($Address,$Config,$Tour){
        //********Reset first node of swf address
        var firstAutoViewName = $Config('autoview').first().attr('name');

        $Address('pano').each(function(i,elem){
            $Address(this).remove();
        });

        var getScene = $Tour('scene').filter(function(){
            return $Tour(this).attr('name') === firstAutoViewName;
        });

        if (getScene.length){
            var panoNode = $Address('<pano/>');
            panoNode.attr('name','pano1');
            panoNode.attr('scene',getScene.attr('name'));
            panoNode.attr('pageurl','/' + getScene.attr('title')?'/' + getScene.attr('title').split(' ').join('_').replace("[h1]","").replace("[/h1]",""):'/');
            panoNode.attr('pagetitle',getScene.attr('title')?getScene.attr('title').replace("[h1]","").replace("[/h1]",""):'/');
            panoNode.attr('root','true');
            $Address('plugin').prepend(panoNode);

            var index = 1;
            $Tour('scene').each(function(i,elem){
                if ($Tour(this).attr('name')!== firstAutoViewName){
                    var panoNode = $Address('<pano/>');
                    index = index + 1;
                    panoNode.attr('name','pano'+index);
                    panoNode.attr('scene',$Tour(this).attr('name'));
                    panoNode.attr('pageurl','/' + $Tour(this).attr('title')?'/' + $Tour(this).attr('title').split(' ').join('_').replace("[h1]","").replace("[/h1]",""):'/');
                    panoNode.attr('pagetitle',$Tour(this).attr('title')?$Tour(this).attr('title').replace("[h1]","").replace("[/h1]",""):'/');
                    panoNode.attr('root','true');
                    $Address('plugin').append(panoNode);
                }
            });
        }
        return {$Address:$Address,$Config:$Config,$Tour:$Tour};
    };

    app.put('/bulkscenes/:folder/:filename', function (request, response) {
        console.log('menu put entered');
        var baseName = path.basename(request.params.folder, '.zip');
        console.log('if undefined Mini failed ' + request.params.filename);
        var pathMiniToFile =path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');//This is the mini you are modifying
        var pathMiniToInterfaceFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_interface.xml');
        var pathMiniToInterfaceMobFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_interface_m.xml');
        var pathMiniToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml');
        var pathMiniToAddressFile = path.join(application_root, 'server/tours', baseName, 'files','swfaddress', request.params.filename + '_swfaddress.xml');

        console.log('if undefined Main failed ' + request.body.mainfilename);
        var pathMainToFile =path.join(application_root, 'server/tours', baseName, 'files', request.body.mainfilename + '_tour.xml');//This is the Main you are prototyping
        var pathToInterfaceFile = path.join(application_root, 'server/tours', baseName, 'files', request.body.mainfilename + '_interface.xml');
        var pathToInterfaceMobFile = path.join(application_root, 'server/tours', baseName, 'files', request.body.mainfilename + '_interface_m.xml');
        var pathToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.body.mainfilename + '_config.xml');
        var pathToIndex = path.join(application_root, 'server/tours', baseName);


        async.parallel({
            tourMain:function(callback){
                fs.readFile(pathMainToFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            interfaceFile: function (callback) {
                fs.readFile(pathToInterfaceFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            configFile: function (callback) {
                fs.readFile(pathToConfigFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            interfaceMobFile: function (callback) {
                if (!fs.existsSync(pathToInterfaceMobFile)) {
                    fs.readFile(path.join(pathToIndex, 'files', 'interface_m.xml'),
                        function (err, data) {
                            if (!err) {
                                var $ = cheerio.load(data, {xmlMode: true});
                                var selNode = $('include').filter(function () {
                                    return $(this).attr('url') === 'swfaddress/swfaddress.xml';
                                });

                                selNode.attr('url', path.join('swfaddress', request.body.mainfilename.split(' ').join('_') + '_swfaddress.xml'));
                                atom.writeFile(path.join(pathToIndex, 'files', request.body.mainfilename.split(' ').join('_') + '_interface_m.xml'), $.xml(), function (err) {
                                    fs.readFile(pathToInterfaceMobFile,
                                        function (err, data) {
                                            callback(err, data);
                                        });
                                });
                            }
                        });
                } else {
                    fs.readFile(pathToInterfaceMobFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            },
            tourMini:function(callback){
                fs.readFile(pathMiniToFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            interfaceMiniFile: function (callback) {
                fs.readFile(pathMiniToInterfaceFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            configMiniFile: function (callback) {
                fs.readFile(pathMiniToConfigFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            addressMiniFile: function (callback) {
                fs.readFile(pathMiniToAddressFile,
                    function (err, data) {
                        callback(err, data);
                    });
            },
            interfaceMobMiniFile: function (callback) {
                if (!fs.existsSync(pathMiniToInterfaceMobFile)) {
                    fs.readFile(path.join(pathToIndex, 'files', 'interface_m.xml'),
                        function (err, data) {
                            if (!err) {
                                var $ = cheerio.load(data, {xmlMode: true});
                                var selNode = $('include').filter(function () {
                                    return $(this).attr('url') === 'swfaddress/swfaddress.xml';
                                });

                                selNode.attr('url', path.join('swfaddress', request.params.filename.split(' ').join('_') + '_swfaddress.xml'));
                                atom.writeFile(path.join(pathToIndex, 'files', request.params.filename.split(' ').join('_') + '_interface_m.xml'), $.xml(), function (err) {
                                    fs.readFile(pathToInterfaceMobFile,
                                        function (err, data) {
                                            callback(err, data);
                                        });
                                });
                            }
                        });
                } else {
                    fs.readFile(pathMiniToInterfaceMobFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            }
        },function(err,results){
            var $Main =cheerio.load(results.tourMain,{xmlMode:true});
            var $Mini =cheerio.load(results.tourMini,{xmlMode:true});
            var $interface = cheerio.load(results.interfaceFile, {xmlMode: true});
            var $config = cheerio.load(results.configFile, {xmlMode: true});
            var $interfaceMob = cheerio.load(results.interfaceMobFile, {xmlMode: true});
            var $interfaceMini = cheerio.load(results.interfaceMiniFile, {xmlMode: true});
            var $configMini = cheerio.load(results.configMiniFile, {xmlMode: true});
            var $addressMini = cheerio.load(results.addressMiniFile, {xmlMode: true});
            var $interfaceMobMini = cheerio.load(results.interfaceMobMiniFile, {xmlMode: true});


            //one******** copy scene nodes over****************************
            //two******** copy all style nodes over ***********************
            //three****** copy all action nodes over **********************
            //four******* copy autoview node over if it exists ************
            //five******* create and reorder pano nodes in swfaddress *****


            //one******** copy scene nodes over**********
            var MainScene = $Main('scene').filter(function(i,el){
                return $Main(this).attr('name') === request.body.name;
            });
            var MiniScene = $Mini('scene').filter(function(i,el){
                return $Mini(this).attr('name') === request.body.name;
            });
            if (MiniScene.length){
                MiniScene.remove();
            }
            var MiniKrpano = $Mini('krpano').first();
            MiniKrpano.append(MainScene);
            //two******** copy all style nodes over from interface files ****
            MainScene.find('hotspot').each(function(i,elem){
                if(!$Main(this).attr('linkedscene') && !$Main(this).attr('linkedScene') && !$Main(this).attr('parent')){

                    var styleName = $Main(this).attr('style');
                    var styleNode = $interface('style').filter(function(i,elem){
                        return $interface(this).attr('name')=== styleName;
                    });

                    var miniStyles = $interfaceMini('style').filter(function(i,elem){
                        return $interfaceMini(this).attr('name')=== styleName;
                    });
                    var miniMobStyles = $interfaceMobMini('style').filter(function(i,elem){
                        return $interfaceMobMini(this).attr('name')=== styleName;
                    });
                    if (!miniStyles.length){
                        $interfaceMini('krpano').append(styleNode);
                    }
                    if (!miniMobStyles.length){
                        $interfaceMobMini('krpano').append(styleNode);
                    }

                }
            });
            //three****** copy all action nodes over ****
            MainScene.find('hotspot').each(function(i,elem){
                if(!$Main(this).attr('linkedscene') && !$Main(this).attr('linkedScene') && !$Main(this).attr('parent')){

                    var hotName = $Main(this).attr('name');
                    var sceneName = request.body.name;
                    var actionName = 'slide'+hotName + '_' + sceneName;
                    console.log('action name = '+actionName);
                    var actionNode = $interface('action').filter(function(i,elem){
                        return $interface(this).attr('name')=== actionName;
                    });

                    var miniActionNode = $interfaceMini('action').filter(function(i,elem){
                        return $interfaceMini(this).attr('name')=== actionName;
                    });
                    var miniMobActionNode = $interfaceMobMini('style').filter(function(i,elem){
                        return $interfaceMobMini(this).attr('name')=== actionName;
                    });
                    if (!miniActionNode.length){
                        $interfaceMini('krpano').append(actionNode);
                    }
                    if (!miniMobActionNode.length){
                        $interfaceMobMini('krpano').append(actionNode);
                    }
                }
            });
            //four******* copy autoview node over if it exists ******

            var autoViewNodes = $config('autoview').filter(function(i,elem){
                return $config(this).attr('name')===request.body.name;
            });
            if (autoViewNodes.length){
                autoViewNodes.each(function(i,elem){
                    var autoViewMiniNodes = $configMini('autoview').filter(function(i,elem){
                        return $configMini(this).attr('name') === request.body.name;
                    });
                    if (!autoViewMiniNodes.length){
                        $configMini('krpano').append($config(this));
                    }
                });
            }

            //five******* create and reorder pano nodes in mini swfaddress *****
            var AllXml = getAllUpdatedXml($addressMini,$configMini,$Mini);

            async.parallel({
                    writeMiniTour: function (callback) {
                        atom.writeFile(pathMiniToFile, AllXml.$Tour.xml(), function (err) {
                            callback(err, request.body);
                        });
                    },
                    writeMiniInterface: function (callback) {
                        atom.writeFile(pathMiniToInterfaceFile, $interfaceMini.xml(), function (err) {
                            callback(err, request.body);
                        });
                    },
                    writeMiniConfig: function (callback) {
                        atom.writeFile(pathMiniToConfigFile, AllXml.$Config.xml(), function (err) {
                            callback(err, request.body);
                        });
                    },
                    writeMiniAddress: function (callback) {
                        atom.writeFile(pathMiniToAddressFile, AllXml.$Address.xml(), function (err) {
                            callback(err, request.body);
                        });
                    },
                    writeMiniInterfaceMob: function (callback) {
                        atom.writeFile(pathMiniToInterfaceMobFile, $interfaceMobMini.xml(), function (err) {
                            callback(err, request.body);
                        });
                    }
                },
                function (err, results) {
                    if (err) throw err;
                    console.log("This scene transfer put write succeeded " + request.params.filename);
                    var Main_Scene = {
                        name:MainScene.attr('name'),
                        title:MainScene.attr('title'),
                        description:MainScene.attr('description'),
                        folderName:request.params.folder
                    };
                    response.send(JSON.stringify(Main_Scene));
                });


           /* atom.writeFile(pathMiniToFile, $Mini.xml(),function(err){
                if (err) throw err;
                console.log("This scene transfer put write succeeded " + request.params.filename);
                var Main_Scene = {
                    name:MainScene.attr('name'),
                    title:MainScene.attr('title'),
                    description:MainScene.attr('description'),
                    folderName:request.params.folder
                };
                response.send(JSON.stringify(Main_Scene));
            });*/

        });
    });

    app.delete('/bulkscenes/:folder/:filename', function (request, response) {
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');

        console.log('Bulk scene delete entered' + JSON.stringify(request.body) + ' Length ' +request.params.filename);
        fs.readFile(pathToFile, function (err, data) {
            var $ = cheerio.load(data, {xmlMode: true});

            var sceneNode = $('scene').each(function (i,elem) {
                    return $(this).remove();
                });

            atom.writeFile(pathToFile, $.xml(), function (err) {
                if (err) throw err;
                console.log("This delete bulk scene write succeeded " + request.params.name);
                var scene = {
                    name: sceneNode.attr('name'),
                    title: sceneNode.attr('title') ? sceneNode.attr('title').replace("[h1]", "").replace("[/h1]", "") : '',
                    description: sceneNode.attr('descripcion') ? sceneNode.attr('descripcion').replace("[p]", "").replace("[/p]", "") : ''}
                response.send(scene);
            });

        });

    });

};