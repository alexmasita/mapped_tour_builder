/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/1/13
 * Time: 5:46 PM
 * To change this template use File | Settings | File Templates.
 */

// JavaScript Document

module.exports = function (app, fs, parser, application_root, path) {

    //var json2xml = require('../my_modules/json2xml').json2xml;

    var cheerio = require('cheerio');
    var shortId = require('shortid');
    var check = require('check-types');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();

    app.get('/api/menus/:folder/:filename', function (request, response) {
        console.log('menus with just folder entered : ' + request.params.folder);

        var menus = [];
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile =path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml');

        fs.readFile(pathToFile,
            function (err, data) {
                if (!err){
                    var $ = cheerio.load(data,{xmlMode:true});

                    var allMenus = $('button');
                    var menuNode = allMenus.filter(function(){
                        return !($(this).attr('id'));
                    });

                    console.log('nodes to delete ' + menuNode.length);

                    menuNode.each(function(index){
                        //console.log();
                        $(this).remove();
                    });
                    atom.writeFile(pathToFile, $.xml(),function(err){
                        if (err) throw err;
                    });

                    $('button').each(function(i,el){
                        menus.push({

                            name:$(this).attr('name').slice(4),
                            text:$(this).attr('text'),
                            type:$(this).attr('type'),
                            load:$(this).attr('load'),
                            folderName:request.params.folder,
                            id:$(this).attr('id')
                        })
                    });
                    //console.log("What is menus" + JSON.stringify(menus));
                    response.send(JSON.stringify(menus));
                } else {
                    console.log("Error Loading File: "+err);
                }

            });

    });

    app.get('/api/menus/:folder/:filename/:id', function (request, response) {
            console.log('menus with id entered : ' + request.params.id);
            var baseName = path.basename(request.params.folder, '.zip');
            var xmlData
            fs.readFile(path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml'),
                function (err, data) {
                    if (!err){
                        var $ = cheerio.load(data,{xmlMode:true});

                        var menuNode = $('button').filter(function(){
                            return $(this).attr('id') === request.params.id;
                        });

                        var menu = {
                            name:menuNode.attr('name').slice(4),
                            text:menuNode.attr('text'),
                            type:menuNode.attr('type'),
                            load:menuNode.attr('load'),
                            folderName:request.params.folder,
                            id:menuNode.attr('id')
                        };
                        console.log('alert menu item response ' + JSON.stringify(menu));
                        response.send(JSON.stringify(menu));
                    } else {
                        console.log("Error Loading File: "+err);
                    }
                });
        }
    );
    app.put('/api/menus/:folder/:filename/:id', function (request, response) {
        console.log('menu put entered');
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile =path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml')
        fs.readFile(pathToFile,
            function (err, data) {
                if (!err){
                    var $ = cheerio.load(data,{xmlMode:true});

                    var menuNode = $('button').filter(function(){
                        return $(this).attr('id') === request.params.id;
                    });

                    if (menuNode.length){
                        if(check.isNumber(request.body.index)){
                            //console.log('The menu model has index of : ' + request.body.index);
                            menuNode.remove();
                            if(request.body.index < $('button').length){
                                $('button').eq(request.body.index).before(menuNode);

                            } else {
                                $('button').last().after(menuNode);
                            }


                            $('button').each(function(index){
                                //console.log();
                                $(this).attr('name','item'+index)
                            });

                        } else {
                            //console.log('The model has no index');
                            menuNode.attr('name','item'+request.body.name);
                            menuNode.attr('text',request.body.text);
                            menuNode.attr('type',request.body.type);
                            menuNode.attr('load',request.body.load);
                            menuNode.attr('id',request.body.id);
                        }

                        atom.writeFile(pathToFile, $.xml(),function(err){
                            if (err) throw err;
                            console.log("This menu put write succeeded " + request.params.filename);
                            var menu = {
                                name:menuNode.attr('name').slice(4),
                                text:menuNode.attr('text'),
                                type:menuNode.attr('type'),
                                load:menuNode.attr('load'),
                                folderName:request.params.folder,
                                id:menuNode.attr('id')
                            };
                            response.send(JSON.stringify(menu));
                        });
                    }/*else{
                        var newMenuNode = $("<button></button>");
                        newMenuNode.attr('name','item'+request.body.name);
                        newMenuNode.attr('text',request.body.text);
                        newMenuNode.attr('type',request.body.type);
                        newMenuNode.attr('load',request.body.load);
                        newMenuNode.attr('id',request.body.id);
                        $("button").last().after(newMenuNode)

                        fs.writeFile(pathToFile, $.xml(),function(err){
                            if (err) throw err;
                            console.log("This write succeeded" + request.params.name);
                            var menu = {
                                name:newMenuNode.attr('name').slice(4),
                                text:newMenuNode.attr('text'),
                                type:newMenuNode.attr('type'),
                                load:newMenuNode.attr('load'),
                                folderName:request.params.folder,
                                id:newMenuNode.attr('id')
                            }
                            response.send(menu);
                        });
                    }*/
                } else {
                    console.log("Error Loading File: "+err);
                }
            });
    });

    app.post('/api/menus/:folder/:filename', function (request, response) {
        console.log("Menus post entered");
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile =path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml')
        fs.readFile(pathToFile,
            function (err, data) {
                if (!err){
                    var $ = cheerio.load(data,{xmlMode:true});

                    var menuNodeLength = $('button').length;
                    var newMenuNode = $("<button></button>");
                    newMenuNode.attr('name','item'+menuNodeLength/* request.body.name*/);
                    newMenuNode.attr('text',request.body.text);
                    newMenuNode.attr('type',request.body.type);
                    newMenuNode.attr('load',request.body.load);
                    newMenuNode.attr('id',shortId.generate());
                    console.log($("data").length + ' ' + $("button").length + "Data and Button Values");
                    if ($("button").length){
                        console.log("Entered button");
                        $("button").last().after(newMenuNode);
                    } else if($("data").length){
                        console.log("Entered data");
                        $("data").last().after(newMenuNode);
                    }


                    atom.writeFile(pathToFile, $.xml(),function(err){
                        if (err) throw err;
                        console.log("This write succeeded" + request.params.name);
                        var menu = {
                            name:newMenuNode.attr('name').slice(4),
                            text:newMenuNode.attr('text'),
                            type:newMenuNode.attr('type'),
                            load:newMenuNode.attr('load'),
                            folderName:request.params.folder,
                            id:newMenuNode.attr('id')
                        }
                        response.send(menu);
                    });

                } else {
                    console.log("Error Loading File: "+err);
                }

            });


       /* var book = new BookModel({
            title: request.body.title,
            author: request.body.author,
            releaseDate: request.body.releaseDate,
            keywords:request.body.keywords
        });
        book.save(function (err) {
            if (!err) {
                return console.log('created');
            } else {
                return console.log(err);
            }
        });
        return response.send(book);*/
    });

    app.delete('/api/menus/:folder/:filename/:id', function (request, response) {
        console.log('Deleting Menu Item Entered: ' + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile =path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml')
        fs.readFile(pathToFile,
            function (err, data) {
                if (!err){
                    var $ = cheerio.load(data,{xmlMode:true});

                    var allMenus = $('button');
                    var menuNode = allMenus.filter(function(){
                        return $(this).attr('id') === request.params.id;
                    });

                    if (menuNode.length){
                        menuNode.remove();

                        $('button').each(function(index){
                            //console.log();
                            $(this).attr('name','item'+index)
                        });

                        atom.writeFile(pathToFile, $.xml(),function(err){
                            if (err) throw err;
                            console.log("This delete write succeeded" + request.params.id);
                            var menu = {
                                name:menuNode.attr('name').slice(4),
                                text:menuNode.attr('text'),
                                type:menuNode.attr('type'),
                                load:menuNode.attr('load'),
                                folderName:request.params.folder,
                                id:menuNode.attr('id')
                            }
                            response.send(menu);
                        });
                    } else {
                        console.log("No Menu Item Deleted");
                    }

                } else {
                    console.log("Error Loading File: "+err);
                }

            });
    });


};