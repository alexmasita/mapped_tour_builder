/**
 * Created by alexmasita on 11/17/13.
 */

module.exports = function (app, path, fs, application_root, upload) {
    var cheerio = require('cheerio');
    var shortId = require('shortid');
    var check = require('check-types');
    var async = require('async');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();
    var mv = require('mv');

    app.get('/api/images/:folder', function (request, response) {
        console.log('Images Get Called : ' + request.params.folder);
        //console.log('Is Film value = ' +request.param('isFilm'));
        var isFilm = request.param('isFilm');
        var flyers = [];
        var videos = [];
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'app.xml');

        if (!(fs.existsSync(pathToFile))) {
            response.send(JSON.stringify([]));
            return;
        }
        fs.readFile(pathToFile,
            function (err, data) {
                if (!err) {
                    var $ = cheerio.load(data, {xmlMode: true});
                    if (isFilm==='true'){
                        var allVideo = $('video');
                        allVideo.each(function (i, el) {
                            videos.push({
                                id: $(this).attr('id'),
                                url: $(this).attr('url'),
                                filename: $(this).attr('filename'),
                                isParent:$(this).attr('isParent')
                            })
                        });
                        response.send(JSON.stringify(videos));

                    }else{
                        var allFlyers = $('flyer');
                        allFlyers.each(function (i, el) {
                            console.log('value of webaddress = ' + $(this).attr('webAddress'));
                            flyers.push({
                                id: $(this).attr('id'),
                                url: $(this).attr('url'),
                                filename: $(this).attr('filename'),
                                webAddress: $(this).attr('webAddress'),
                                isParent:$(this).attr('isParent')
                            })
                        });
                        response.send(JSON.stringify(flyers));
                    }

                } else {
                    console.log("Error Loading File: " + err);
                }
            });
    });

    app.post('/api/images/:folder', function (request, response) {
        //console.log(request.files.count());
        console.log('The image post was called ' + request.body);
        var isFilm = request.body.isFilm;
        //console.log('Images post isFilm Value = ' + isFilm);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFolder = path.join(application_root, 'server/tours', baseName,isFilm?'files/videos':'files/flyers');

        mv(path.join(pathToFolder,request.body.filename), path.join(pathToFolder,isFilm?'ads':'images',request.body.filename), {mkdirp: true}, function(err) {
            // done. it first created all the necessary directories, and then
            // tried fs.rename, then falls back to using ncp to copy the dir
            // to dest and then rimraf to remove the source dir
            var pathToFile = path.join(application_root, 'server/tours', baseName, 'app.xml');
            fs.readFile(pathToFile,
                function (err, data) {
                    if (!err) {
                        var $ = cheerio.load(data, {xmlMode: true});

                        if (isFilm){
                            var videoCount = $('video').filter(function(i,el){
                                return $(this).attr('filename') === request.body.filename;
                            });
                            //console.log('video count ' + videoCount.length);
                            if (videoCount.length){
                                return;
                            }

                            var newVideoNode = $("<video></video>");
                            newVideoNode.attr('url',path.join(baseName,'files/videos/ads',request.body.filename));
                            newVideoNode.attr('filename', request.body.filename);
                            newVideoNode.attr('isParent', request.body.isParent);
                            newVideoNode.attr('id', shortId.generate());
                            //console.log($("video").length + " video length");
                            var $videos;
                            if ($('videos').length){
                                $videos = $('videos');
                            } else {
                                $videos = $("<videos></videos>");
                                $('mini').append($videos);
                            }
                            $videos.append(newVideoNode);

                            atom.writeFile(pathToFile, $.xml(), function (err) {
                                if (err) throw err;
                                console.log("This video write succeeded");
                                var video = request.body;
                                video.id = newVideoNode.attr('id');
                                response.send(video);

                            });
                        } else {
                            var flyerCount = $('flyer').filter(function(i,el){
                                return $(this).attr('filename') === request.body.filename;
                            });
                            console.log('flyer count ' + flyerCount.length);
                            if (flyerCount.length){
                                return;
                            }

                            console.log('web address value = '+request.body.webAddress);
                            var newFlyerNode = $("<flyer></flyer>");
                            newFlyerNode.attr('url',path.join(baseName,'files/flyers/images',request.body.filename));
                            newFlyerNode.attr('filename', request.body.filename);
                            newFlyerNode.attr('isParent', request.body.isParent);
                            newFlyerNode.attr('webAddress', request.body.webAddress);
                            newFlyerNode.attr('id', shortId.generate());
                            console.log($("flyer").length + " Flyers length");
                            var $flyers;
                            if ($('flyers').length){
                                $flyers = $('flyers');
                            } else {
                                $flyers = $("<flyers></flyers>");
                                $('mini').append($flyers);
                            }
                            $flyers.append(newFlyerNode);

                            atom.writeFile(pathToFile, $.xml(), function (err) {
                                if (err) throw err;
                                console.log("This flyer write succeeded");
                                var image = request.body;
                                image.id = newFlyerNode.attr('id');
                                response.send(image);
                            });
                        }
                    } else {
                        console.log("Error Loading File: " + err);
                    }

                });

        });

       /* upload.fileManager().move(request.body.filename, 'images', function (err, result) {
            if (!err) {
                var baseName = path.basename(request.params.folder, '.zip');
                if (fs.existsSync(path.join(application_root, 'server/tours', baseName, 'files/flyers', request.body.filename))) {
                    fs.unlinkSync(path.join(application_root, 'server/tours', baseName, 'files/flyers', request.body.filename));
                }

                var pathToFile = path.join(application_root, 'server/tours', baseName, 'app.xml');
                fs.readFile(pathToFile,
                    function (err, data) {
                        if (!err) {
                            var $ = cheerio.load(data, {xmlMode: true});


                            var newFlyerNode = $("<flyer></flyer>");
                            newFlyerNode.attr('url', request.body.url);
                            newFlyerNode.attr('filename', request.body.filename);
                            newFlyerNode.attr('id', shortId.generate());
                            console.log($("flyer").length + " Flyers length");
                            var $flyers;
                            if ($('flyers').length){
                                $flyers = $('flyers');
                            } else {
                                $flyers = $("<flyers></flyers>");
                                $('mini').append($flyers);
                            }
                            $flyers.append(newFlyerNode);



                            atom.writeFile(pathToFile, $.xml(), function (err) {
                                if (err) throw err;
                                console.log("This write succeeded" + request.params.name);
                                var flyer = request.body;
                                flyer.id = newFlyerNode.attr('id');
                                response.send(flyer);
                            });

                        } else {
                            console.log("Error Loading File: " + err);
                        }

                    });
            } else {
                return response.send(err);
            }
        });*/

    });

    app.delete('/api/images/:folder/:id', function (request, response) {
        console.log('Deleting image Item Entered: ' + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');

        //var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', 'tour.xml');

        var pathToFile = path.join(application_root, 'server/tours', baseName, 'app.xml');

        async.parallel({
            appFile:function(callback){
                fs.readFile(pathToFile,
                    function (err, data) {
                        callback(err, data);
                    });
            }

        },function(err,results){
            var $ = cheerio.load(results.appFile, {xmlMode: true});

            //var $ = cheerio.load(data, {xmlMode: true});
            var selectedFlyer = $('flyer').filter(function () {
                return $(this).attr('id') === request.params.id;
            });
            var selectedVideo = $('video').filter(function () {
                return $(this).attr('id') === request.params.id;
            });

            if (selectedFlyer.length){
                if (fs.existsSync(path.join(application_root, 'server/tours', baseName, 'files/flyers/images', selectedFlyer.attr('filename')))) {
                    fs.unlinkSync(path.join(application_root, 'server/tours', baseName, 'files/flyers/images', selectedFlyer.attr('filename')));
                }
                selectedFlyer.remove();
            }
            if (selectedVideo.length){
                if (fs.existsSync(path.join(application_root, 'server/tours', baseName, 'files/videos/ads', selectedVideo.attr('filename')))) {
                    fs.unlinkSync(path.join(application_root, 'server/tours', baseName, 'files/videos/ads', selectedVideo.attr('filename')));
                }
                selectedVideo.remove();
            }
            async.parallel({
                    writeTour: function (callback) {
                        atom.writeFile(pathToFile, $.xml(), function (err) {
                            callback(err, request.params.id);
                        });
                    }

                },
                function (err, results) {
                    // results is now equals to: {writeTour: 1, writeConfig: 2}
                    console.log("This image delete write succeeded  " + request.params.id);
                    var hotflyer = request.body;
                    response.send(JSON.stringify(hotflyer));
                });

        });

    });

};