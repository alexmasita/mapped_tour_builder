/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/1/13
 * Time: 5:58 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Created with JetBrains WebStorm.
 * User: alexmasita
 * Date: 9/1/13
 * Time: 5:46 PM
 * To change this template use File | Settings | File Templates.
 */

// JavaScript Document

module.exports = function (app, fs, parser, application_root, path) {

    //var json2xml = require('../my_modules/json2xml').json2xml;

    var cheerio = require('cheerio');
    var check = require('check-types');
    var async = require('async');
    var atomic = require('../my_modules/index.js');
    var atom = new atomic.Context();
    var shortId = require('shortid');

    app.get('/api/menuScenes/:folder/:filename/:menuId', function (request, response) {
        console.log("Menu Scenes Getter entered with folder " + request.params.folder + ' and MenuId ' + request.params.menuId);
        var scenesResponse = [];
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml');


        async.parallel({
                tour: function (callback) {

                    fs.readFile(pathToFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                config: function (callback) {
                    fs.readFile(pathToConfigFile,
                        function (err, data) {
                            callback(err, data);
                        });

                }
            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                if (!err) {
                    var $tour = cheerio.load(results.tour, {xmlNode: true});
                    var $config = cheerio.load(results.config, {xmlNode: true});
                    var getMenu = null;

                    var menuWithLoad =$config('button').filter(function () {
                        return $config(this).attr('load') === request.params.menuId;
                    });

                    if (menuWithLoad.length){
                        console.log('menu id is load '+menuWithLoad.length);
                        menuWithLoad.attr('id',shortId.generate());
                        atom.writeFile(pathToConfigFile, $config.xml(), function (err) {
                            if(!err){
                                var scenesInMenu = $tour('scene').filter(function () {
                                    //if menu with load create menu with id


                                    getMenu = $config('button').filter(function () {
                                        return $config(this).attr('id') === menuWithLoad.attr('id'); //||$config(this).attr('load') === request.params.menuId
                                    });
                                    if (getMenu.attr('load')){
                                        return ($tour(this).attr('category') === getMenu.attr('load')) || ($tour(this).attr('name') === getMenu.attr('load'));
                                    } else {
                                        return false;
                                    }
                                })

                                scenesInMenu.each(function (i, el) {
                                    scenesResponse.push({
                                        sceneName: $tour(this).attr('name'),
                                        title:$tour(this).attr('title')? $tour(this).attr('title').replace("[h1]","").replace("[/h1]",""):'',
                                        load: getMenu.attr('load'),
                                        menuId: getMenu.attr('id'),
                                        folderName: request.params.folder
                                    })
                                });
                                //console.log("What is scenes" + JSON.stringify(scenesResponse));
                                response.send(JSON.stringify(scenesResponse));
                            }
                        });
                    } else {
                        var scenesInMenu = $tour('scene').filter(function () {
                            //if menu with load create menu with id


                            getMenu = $config('button').filter(function () {
                                return $config(this).attr('id') === request.params.menuId||$config(this).attr('load') === request.params.menuId;
                            });
                            if (getMenu.attr('load')){
                                return ($tour(this).attr('category') === getMenu.attr('load')) || ($tour(this).attr('name') === getMenu.attr('load'));
                            } else {
                                return false;
                            }
                        })

                        scenesInMenu.each(function (i, el) {
                            scenesResponse.push({
                                sceneName: $tour(this).attr('name'),
                                title:$tour(this).attr('title')? $tour(this).attr('title').replace("[h1]","").replace("[/h1]",""):'',
                                load: getMenu.attr('load'),
                                menuId: getMenu.attr('id'),
                                folderName: request.params.folder
                            })
                        });
                        //console.log("What is scenes" + JSON.stringify(scenesResponse));
                        response.send(JSON.stringify(scenesResponse));
                    }


                } else {
                    console.log("Error Loading File: " + err);
                }
            });
    });

    app.put('/api/menuScenes/:folder/:filename/:menuId/:id', function (request, response) {
        console.log("menu scenes put entered: menu Id " + request.params.menuId + "id " + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml');
        var getMenu = null;

        async.parallel({
                tourFile: function (callback) {
                    fs.readFile(pathToFile,
                        function (err, data) {
                            callback(err, data);
                        });
                },
                configFile: function (callback) {
                    fs.readFile(pathToConfigFile,
                        function (err, data) {
                            callback(err, data);
                        });
                }
            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                var $Tour = cheerio.load(results.tourFile, {xmlNode: true});
                var $Config = cheerio.load(results.configFile, {xmlNode: true});

                var scenesInMenu = $Tour('scene').filter(function () {
                    getMenu = $Config('button').filter(function () {
                        return $Config(this).attr('id') === request.params.menuId;
                    });
                    if (getMenu.attr('load')){
                        return ($Tour(this).attr('category') === getMenu.attr('load')) || ($Tour(this).attr('name') === getMenu.attr('load'));
                    } else {
                        return false;
                    }
                });

                var updateScene = null;
                var updateMenu = null;
                if (scenesInMenu.length > 0) {
                    if(scenesInMenu.length===1){
                         scenesInMenu.attr('category', request.body.menuId + 'Group');
                    }
                    updateScene = $Tour('scene').filter(function () {
                        return ($Tour(this).attr('name') === request.params.id);
                    });
                    updateMenu = $Config('button').filter(function () {
                        return ($Config(this).attr('id') === request.body.menuId);
                    });

                    updateScene.attr('category', request.body.menuId + 'Group');
                    updateMenu.attr('load', request.body.menuId + 'Group');
                    updateMenu.attr('type', 'category');
                } else {
                    updateScene = $Tour('scene').filter(function () {
                        return ($Tour(this).attr('name') === request.params.id);
                    });
                    updateMenu = $Config('button').filter(function () {
                        return ($Config(this).attr('id') === request.params.menuId) //request.body.menuId);
                    });
                    updateScene.attr('category', '');
                    updateMenu.attr('load', request.params.id);
                    updateMenu.attr('type', 'button');
                }

                async.parallel({
                        writeTour: function (callback) {
                            atom.writeFile(pathToFile, $Tour.xml(), function (err) {
                                callback(err, request.params.id);
                            });
                        },
                        writeConfig: function (callback) {
                            atom.writeFile(pathToConfigFile, $Config.xml(), function (err) {
                                callback(err, request.params.id);
                            });
                        }
                    },
                    function (err, results) {
                        // results is now equals to: {writeTour: 1, writeConfig: 2}
                        console.log("This write succeeded for Tour and Config " + results.writeTour);
                        var menuScene = {
                            sceneName: updateScene.attr('name'),
                            title:updateScene.attr('title')? updateScene.attr('title').replace("[h1]","").replace("[/h1]",""):'',
                            load: updateMenu.attr('load'),
                            menuId: request.params.menuId}   //request.body.menuId
                        response.send(menuScene);
                    });
            });

        /*fs.readFile(pathToFile,
         function (err, data) {
         if (!err){
         var $ = cheerio.load(data,{xmlNode:true});

         var scenesInMenu = $('scene').filter(function(){
         return ($(this).attr('category') === request.params.load) || ($(this).attr('name') === request.params.load);
         });

         var updateScene = null;
         if (scenesInMenu.length > 0){
         updateScene = $('scene').filter(function(){
         return ($(this).attr('name') === request.params.id);
         });
         updateScene.attr('category',request.body.menuId + 'Group');
         } else {
         updateScene = $('scene').filter(function(){
         return ($(this).attr('name') === request.params.id);
         });
         updateScene.attr('category','');
         }

         fs.writeFile(pathToFile, $.xml(),function(err){
         if (err) throw err;
         console.log("This write succeeded for " + request.params.id);
         var menuScene = {
         sceneName:updateScene.attr('name'),
         load:updateScene.attr('category'),
         menuId:request.body.menuId}
         response.send(menuScene);
         });

         } else {
         console.log("Error Loading File: "+err);
         }

         });

         fs.readFile(pathToConfigFile,
         function (err, data) {
         if (!err){
         var $ = cheerio.load(data,{xmlNode:true});

         var scenesInMenu = $('scene').filter(function(){
         return ($(this).attr('category') === request.params.load) || ($(this).attr('name') === request.params.load);
         });

         if (scenesInMenu.length > 0){
         var updateScene = $('scene').filter(function(){
         return ($(this).attr('name') === request.params.id);
         });
         updateScene.attr('category',request.body.menuId + 'Group');
         } else {
         var updateScene = $('scene').filter(function(){
         return ($(this).attr('name') === request.params.id);
         });
         updateScene.attr('category','');
         }

         fs.writeFile(pathToFile, $.xml(),function(err){
         if (err) throw err;
         console.log("This write succeeded" + request.params.name);
         var scene = {
         name:sceneNode.attr('name'),
         title:sceneNode.attr('title').replace("[h1]","").replace("[/h1]",""),
         description:sceneNode.attr('descripcion').replace("[p]","").replace("[/p]","")}
         response.send(scene);
         });

         } else {
         console.log("Error Loading File: "+err);
         }

         });*/
    });

    /* app.post('/api/menuScenes/:folder/:load', function (request, response) {
     console.log("Menu Scenes post entered");
     var baseName = path.basename(request.params.folder, '.zip');
     var pathToFile =path.join(application_root, 'server/tours', baseName, 'files', 'config.xml');
     var pathToTourFile = path.join(application_root, 'server/tours', baseName, 'files', 'tour.xml');
     fs.readFile(pathToFile,
     function (err, data) {
     if (!err){
     var $ = cheerio.load(data,{xmlNode:true});


     var newMenuNode = $("<button></button>");
     newMenuNode.attr('name','item'+request.body.name);
     newMenuNode.attr('text',request.body.text);
     newMenuNode.attr('type',request.body.type);
     newMenuNode.attr('load',request.body.load);
     newMenuNode.attr('id',shortId.generate());
     console.log($("data").length + ' ' + $("button").length + "Data and Button Values");
     if ($("button").length){
     console.log("Entered button");
     $("button").last().after(newMenuNode);
     } else if($("data").length){
     console.log("Entered data");
     $("data").last().after(newMenuNode);
     }


     fs.writeFile(pathToFile, $.xml(),function(err){
     if (err) throw err;
     console.log("This write succeeded" + request.params.name);
     var menu = {
     name:newMenuNode.attr('name').slice(4),
     text:newMenuNode.attr('text'),
     type:newMenuNode.attr('type'),
     load:newMenuNode.attr('load'),
     folderName:request.params.folder,
     id:newMenuNode.attr('id')
     }
     response.send(menu);
     });

     } else {
     console.log("Error Loading File: "+err);
     }

     });

     });*/

    app.delete('/api/menuScenes/:folder/:filename/:menuId/:id', function (request, response) {
        console.log('Deleting Menu Item Entered: ' + request.params.id);
        var baseName = path.basename(request.params.folder, '.zip');
        var pathToFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_tour.xml');
        var pathToConfigFile = path.join(application_root, 'server/tours', baseName, 'files', request.params.filename + '_config.xml')

        // an example using an object instead of an array
        async.parallel({
                tour: function (callback) {

                    fs.readFile(pathToFile,
                        function (err, data) {
                            callback(err, data);

                        });

                },
                config: function (callback) {
                    fs.readFile(pathToConfigFile,
                        function (err, data) {
                            callback(err, data);

                        });

                }
            },
            function (err, results) {
                // results is now equals to: {one: 1, two: 2}
                if (!err) {
                    var $config = cheerio.load(results.config, {xmlNode: true});
                    var $tour = cheerio.load(results.tour, {xmlNode: true});
                    var getMenuToEdit = null;
                    var getSceneToEdit = null;
                    var noMenuScenes = null;

                    //Check to see how many scenes are in loadGroup.
                    noMenuScenes = $tour('scene').filter(function () {
                        getMenuToEdit = $config('button').filter(function () {
                            return $config(this).attr('id') === request.params.menuId;
                        });
                        if (getMenuToEdit.attr('load')){
                            return ($tour(this).attr('category') === getMenuToEdit.attr('load')) || $tour(this).attr('name') === getMenuToEdit.attr('load');

                        } else{
                            return false;
                        }
                    });
                    console.log('no of menu scenes before delete : ' +  noMenuScenes.length);
                    if (noMenuScenes.length > 1) {
                        getSceneToEdit = $tour('scene').filter(function () {
                            return $tour(this).attr('name') === request.params.id;
                        });
                        console.log('no of menu scenes after filter : ' + getSceneToEdit.length);
                        getSceneToEdit.attr('category', '');
                        //Check scene count here, if equal to one, set config buttons element load attr to name of scene
                        noMenuScenes = $tour('scene').filter(function () {
                            getMenuToEdit = $config('button').filter(function () {
                                return $config(this).attr('id') === request.params.menuId;
                            });
                            if(getMenuToEdit.attr('load')){
                                return ($tour(this).attr('category') === getMenuToEdit.attr('load')); //|| $tour(this).attr('name') === getMenuToEdit.attr('load')
                            }else{
                                return false;
                            }
                        });
                        console.log('new no of menu scenes after filter/update : ' + noMenuScenes.length);
                        if(noMenuScenes.length === 1){
                            getMenuToEdit.attr('load',noMenuScenes.attr('name'));
                            getMenuToEdit.attr('type','button');
                            noMenuScenes.attr('category','');
                        }

                    } else {
                        getSceneToEdit = $tour('scene').filter(function () {
                            return $tour(this).attr('name') === request.params.id;
                        });
                        getSceneToEdit.attr('category', '');
                        getMenuToEdit = $config('button').filter(function () {
                            return $config(this).attr('id') === request.params.menuId;
                        });
                        getMenuToEdit.attr('load', '');
                        getMenuToEdit.attr('type','');
                    }

                    async.parallel({
                            writeTour: function (callback) {
                                atom.writeFile(pathToFile, $tour.xml(), function (err) {
                                    callback(err, request.params.id);
                                });
                            },
                            writeConfig: function (callback) {
                                atom.writeFile(pathToConfigFile, $config.xml(), function (err) {
                                    callback(err, request.params.id);
                                });
                            }
                        },
                        function (err, results) {

                            console.log("This write delete succeeded for Tour and Config " + results.writeTour);

                        });


                } else {
                    console.log('Menu Scenes Delete error : ' + err)
                }
            });


    });

};