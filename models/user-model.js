/**
 * Created by alexmasita on 3/13/14.
 */
module.exports = function (app, path, fs, application_root, upload, mongoose) {
    'use strict';
    var Schema = mongoose.Schema,
        bcrypt = require('bcrypt'),
        SALT_WORK_FACTOR = 10,
        UserSchema = new Schema({
            firstname: String,
            lastname: String,
            email: String,
            icode: String,
            username: { type: String, required: true, index: { unique: true } },
            password: { type: String, required: true },
            role: String
        });

    UserSchema.pre('save', function (next) {
        var user = this;

// only hash the password if it has been modified (or is new)
        if (!user.isModified('password')) { return next(); }

// generate a salt
        bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
            if (err) { return next(err); }

            // hash the password using our new salt
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) { return next(err); }
                // override the cleartext password with the hashed one
                user.password = hash;
                next();
            });
        });
    });

    UserSchema.methods.comparePassword = function (candidatePassword, cb) {
        bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
            if (err) { return cb(err); }
            cb(null, isMatch);
        });
    };
    return mongoose.model('User', UserSchema);
};