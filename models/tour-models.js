module.exports = function (app, path, fs, application_root, upload, mongoose) {
    'use strict';
    var Tour,
        //TourModel,
        //TourGroupModel,
        TourGroup,
        models;

    TourGroup = new mongoose.Schema({
        /* _id: mongoose.Schema.Types.ObjectId,*/
        groupName: String,
        groupDescription: String
    });
    //TourGroupModel = mongoose.model('TourGroup', TourGroup);
    Tour = new mongoose.Schema({
        _group: { type: mongoose.Schema.Types.ObjectId, ref: 'TourGroup' },//ref is the name of the schema
        propertyName: String,
        description: String,
        tourLink: String,
        filename: String,
        order: Number,
        latitude: Number,
        longitude: Number
    });
    //TourModel = mongoose.model('Tour', Tour);
    models = {
        TourGroupModel: mongoose.model('TourGroup', TourGroup),
        TourModel: mongoose.model('Tour', Tour)
    };/*, ObjectId: mongoose.Schema.Types.ObjectId*/
    return models;
};